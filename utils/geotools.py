# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 17:12:50 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
from datetime import datetime
from math import fabs
from math import ceil
from math import floor
import os
import re

# python package imports
try:
    from osgeo import gdal
    from osgeo import ogr
    from osgeo import osr
except:
    import gdal
    import ogr
    import osr

import netCDF4
import numpy as np
import numpy.ma as ma
from scipy import stats

# yulia imports
from .utilities import Utils
from .config import ConfigDict
from .crns_weight_schroen2017 import W_r_star


class Geotools(object):
    """Collection of tools for reading, writing and converting geodata."""
    def __init__(self,
                 log_object):
    
        self.log = log_object
        self.utils = Utils(log_object)
        
    def get_geotrans(self,
                     geotrans_method,
                     **args):
        """
        Return reclass according to method.

        Args:
            geotrans_method (string): string naming the method.
            **args (dict): keyword arguments.

        Returns:
            None.

        """
        # get name of method accoridng to keyword arg
        _method_name = "_get_geotrans_{}".format(geotrans_method)
        # define error string to be in case the method not implemented
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
        getattr(self,_method_name,lambda **x: self.log.log(
            _err_string))(**args)
    
    def _get_geotrans_gdal(self,
                           dataset):
        """
        Read geotrans tuple from gdal dataset object.

        Args:
            dataset (object): gdal dataset object.

        Returns:
            None.

        """
        # geotrans tuple
        self.geotrans = dataset.GetGeoTransform()
        # grid resolution
        self.res = self.geotrans[1]
        # width and height of the grid
        self.width = dataset.RasterXSize
        self.height = dataset.RasterYSize
        # coordinates of grid corners
        self.x_min = self.geotrans[0]
        self.x_max = self.x_min + self.width*self.res
        self.y_max = self.geotrans[3]
        self.y_min = self.y_max - self.height*self.res
    
    def _get_geotrans_from_coordinates(self,
                                       xlist,
                                       ylist):
        """
        Create geotrans tuple from coordinates.

        Args:
            x_coords (array-like, 1D): list of x-coordinates.
            y_coords (array-like, 1D): list of y-coordinates.

        Returns:
            None.

        """
        # coordinates of grid corners
        self.x_min = np.nanmin(xlist)
        self.x_max = np.nanmax(xlist)
        self.y_min = np.nanmin(ylist)
        self.y_max = np.nanmax(ylist)
        # grid resolution
        self.res   = abs(xlist[0] - xlist[1])
        # width and height of the grid
        self.width = len(xlist)
        self.height = len(ylist)
        # geotrans tuple
        self.geotrans = (self.x_min, self.res, 0, self.y_max, 0, -self.res)

    def _get_hru_ncells(self, 
                        hrugrid, 
                        hrulist):
        """analyses HRU grid
    
        Args:
            hrugrid (array-like, 1D): HRU ID grid (flattened ID-array)
            hrulist (array-like, 1D): List of HRU IDs, 1D-Array created
                                      with np.unique(HRU-Grid)
    
        Returns:
            1D-array: Number of Cells per HRU
            1D-array: IDs of HRUs
        """
    
        dimlist = hrulist.shape[0]
        dimgrid = hrugrid.shape[0]
        hru_ncells = np.zeros(dimlist, dtype=np.float32, order='F')
        hru_list_ids = np.zeros(dimgrid, dtype=np.int32, order='F')
    
        for idlist in range(dimlist):
            for idgrid in range(dimgrid):
                hruid = hrulist[idlist]
                if hrugrid[idgrid] == hruid:
                    hru_ncells[idlist] += 1
                    hru_list_ids[idgrid] = idlist
        return hru_ncells, hru_list_ids
    
    def get_ncells(self,
                   zone_id_grid,
                   zone_id_list):
        """
        

        Args:
            zone_id_grid (array-like, 2D): Grid with Zone IDs.
            zone_id_list (array-like, 1D): List with Zone IDs.

        Returns:
            ncells (array-like, 1D): Array with number of cells per zone.
            list_ids (array-like, 1D): List IDs for converting 1D to 2D.

        """

        try:
            from supratool.utils.cGeotools import get_hru_ncells
            ncells, list_ids = get_hru_ncells(zone_id_grid.flatten(),
                                              zone_id_list)
        except ImportError:
            err_text = "Warning:\tLoading slower Python version of get_hru_ncells."
            self.log.log(err_text)
            ncells, list_ids = self._get_hru_ncells(zone_id_grid.flatten(),
                                                    zone_id_list)
        
        return ncells, list_ids

    def export_grid(self,
                    filename,
                    grid,
                    geotrans = None,
                    driver='GTiff',
                    dtype=None):
        """
        Export grid to geotif-file.

        Args:
            filename (string): target filename.
            grid (array-like, 2D): grid to export.
            geotrans (tuple, optional): GDAL geotrans tuple. Defaults to None.
            driver (string, optional): GDAL driver name. Defaults to 'GTiff'.
            dtype (object, optional): GDAL data type. Defaults to None.

        Returns:
            None.

        """
        from osgeo import gdal, gdalconst
        if geotrans is None and hasattr(self, "geotrans"):
            geotrans = self.geotrans
        if dtype is None:
            dtype = gdalconst.GDT_Float32
        ds = gdal.GetDriverByName(driver).Create(filename, 
                                                 grid.shape[1], 
                                                 grid.shape[0], 
                                                 1, 
                                                 dtype)
        
        if geotrans is not None:
            ds.SetGeoTransform(geotrans)
        ds.GetRasterBand(1).WriteArray(grid)
        del ds

    def reclass(self, 
                **args):
        """
        Handle reclass according to **args.

        Args:
            **args (dict): keyword arguments.

        Returns:
            data (array-like): reclassified raster.

        """
        # check if args dict contains neccessary keys
        self.utils.check_args(args, ["reclass_method"])
        
        # get name of method accoridng to keyword arg
        _method_name = "_reclass_{}".format(args["reclass_method"])
        # define error string to be in case the method not implemented
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
        return getattr(self,_method_name,lambda **x: self.log.log(
            _err_string))(**args)
    
    def _reclass_none(self,
                      **args):
        """Do not perform any reclass"""
        return args["orig_data"]
    
    def _reclass_swe_to_sca(self,
                            orig_data,
                            **args):
        """
        Reclassify raster from SWE to binary SCA (snow covered area).

        Args:
            **args (dict): keyword arguments.

        Returns:
            out (array-like): reclassified raster.

        """
        # assing raster to out
        out = orig_data.copy()
        # get swe threshold from args, or use default value
        thr = args.get("swe_threshold", 5)
        # reclass
        out[out < thr] = 0
        out[out >= thr] = 1
        return out
    
    def _reclass_swe_to_scf(self,
                            orig_data,
                            **args):
        """
        Reclassify raster from SWE to SCF (snow cover fraction).

        Args:
            **args (dict): keyword arguments.

        Returns:
            out (array-like): reclassified raster.

        """
        # assing raster to out
        out = orig_data.copy()
        # get swe threshold from args, or use default value
        thr = args.get("swe_shallowpack_threshold", 20.0)
        # reclass
        out = out/thr
        out[out > 1] = 1
        return out

    def _reclass_dict(self,
                      orig_data,
                      reclass_dict):
        """
        General reclassification.

        Args:
            orig_data (array-like): original raster.
            reclass_dict (dict): reclassification {old_val : new_val}

        Returns:
            out (array-like): reclassified raster.

        """
        # assing raster to out
        out = orig_data.copy()
        # reclass
        for key in reclass_dict:
            out[out == key] = reclass_dict[key]
        return out

    def reproject(self,
                  **args):
        """
        Reproject coordinates usign either gdal or pyproj, 
        reproj_engine (gdal or pyproj) is set according to **args.

        Args:
            **args (dict): keyword arguments.

        Returns:
            data (array-like): data to be returned.

        """
        
        # check if args dict contains neccessary keys
        self.utils.check_args(args, ["xlist", 
                                      "ylist", 
                                      "epsg_in", 
                                      "epsg_out"])
        
        # check if there is need for reprojection
        if args["epsg_in"] == args["epsg_out"]:
            # bypass calculation but create 2D xgrid and ygrid
            x_out, y_out = np.meshgrid(args["xlist"],args["ylist"])
            y_out = np.rot90(y_out, 2)
        else:                    
            # get name of method accoridng to keyword arg
            _method_name = "_reproject_{}".format(args.get("reproj_engine",
                                                           "gdal"))
            # define error string to be in case the method not implemented
            _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
                _method_name)
            x_out, y_out =  getattr(self,
                                    _method_name,
                                    lambda **x: self.log.log(
                                        _err_string))(**args)
        return x_out, y_out

    def _reproject_pyproj(self,
                        xlist,
                        ylist,
                        epsg_in,
                        epsg_out,
                        **args):
        """
        Reproject coordinates using GDAL.

        Args:
            xlist (array-like, 1D): list of x-coordinates.
            ylist (array-like, 1D): list of y-coordinates.
            epsg_in (int): EPSG-code (source).
            epsg_out (int): EPSG-code (target).
            **args (dict): keyword arguments.

        Returns:
            x_out (array-like, 2D): Reprojected x-coordinates.
            y_out (array-like, 2D): Reprojected y-coordinates.

        """
        from pyproj import Proj, Transformer

        width = len(xlist)
        height = len(ylist)
        # generate empty arrays
        y_out = np.zeros((height, width), dtype=np.float32)
        x_out = np.zeros((height, width), dtype=np.float32)
    
        # create projection from epsg
        p1 = Proj(init='epsg:'+str(epsg_in))
        p2 = Proj(init='epsg:'+str(epsg_out))
        # (note: other options currently do not work)
        transformer = Transformer.from_proj(p1, p2)

        # transform coordinates
        for x in range(width):
            for y in range(height):
                # reprojecting our array
                x0 = xlist[x]
                y0 = ylist[y]
                #x1, y1 = transform(p1, p2, x0, y0)
                x1, y1 = transformer.transform(x0, y0)
                x_out[y, x] = x1
                y_out[y, x] = y1
        return x_out, y_out
            
    def _reproject_gdal(self,
                        xlist,
                        ylist,
                        epsg_in,
                        epsg_out,
                        **args):
        """
        Reproject coordinates using GDAL.

        Args:
            xlist (array-like, 1D): list of x-coordinates.
            ylist (array-like, 1D): list of y-coordinates.
            epsg_in (int): EPSG-code (source).
            epsg_out (int): EPSG-code (target).
            **args (dict): keyword arguments.

        Returns:
            x_out (array-like, 2D): Reprojected x-coordinates.
            y_out (array-like, 2D): Reprojected y-coordinates.

        """
        try:
            from osgeo import osr
        except:
            import osr
    
        width = len(xlist)
        height = len(ylist)
        # generate empty arrays
        y_out = np.zeros((height, width), dtype=np.float32)
        x_out = np.zeros((height, width), dtype=np.float32)
        # coord ref input data
        in_ref = osr.SpatialReference()
        e = in_ref.ImportFromEPSG(epsg_in)
        if e != 0:
            _err_string = "ERROR:\tImportFromEPSG failed for epsg_in. "
            _err_string += "Errorcode: {}".format(e)
            self.log.log(_err_string)
        # coord ref output data
        out_ref = osr.SpatialReference()
        e = out_ref.ImportFromEPSG(epsg_out)
        if e != 0:
            self.log.log(
                "ERROR:\tImportFromEPSG failed for epsg_out. Errorcode: " +
                str(e))
        # calculate coord Transformation
        coordTrans = osr.CoordinateTransformation(in_ref, out_ref)
        # transform coordinates
        for x in range(width):
            for y in range(height):
                # reprojecting our array
                x0 = xlist[x]
                y0 = ylist[y]
                x1, y1, z = coordTrans.TransformPoint(float(x0), float(
                    y0))  # ensure the variable is equivalent to c_double (float64)
                x_out[y, x] = x1
                y_out[y, x] = y1
        return x_out, y_out

    def get_row_col_point(self, 
                          point_x, 
                          point_y):
        """
        Return rows and colums of a point. Run self.get_geotrans() before.
        All coordinates are assumed to be in the same coordinate system.

        Args:
            point_x (float): X-coord of point.
            point_y (float): Y-coord of point.

        Returns:
            row (int): Row of point.
            col (int): Column of point.

        """
        row = int(floor((self.y_max - point_y) / self.res))
        col = int(floor((point_x - self.x_min) / self.res))
        # check if point is in domain. warn otherwise.
        if row < 0 or col < 0:
            _err_text = "Warning:\t: Point outside domain "
            _err_text += "(x:{:.2f},y:{:.2f}).".format(point_x, point_y)
            self.log.log(_err_text)
        return (row, col)

    def get_griddata_of_point(self,
                              point_x,
                              point_y):
        """
        Return grid data of a point. Needs self.data. 
        Run self.get_geotrans() before.

        Args:
            point_x (float): X-coord of point.
            point_y (float): Y-coord of point.

        Returns:
            point_data (misc): Data of point.

        """        
        return self.data[self._get_row_col_point(point_x, point_y)]

    def zonal(self,
              zonal_method,
              **args):
        """
        Zonal aggregation of data.

        Args:
            **args (TYPE): DESCRIPTION.

        Returns:
            None.

        """
        # check if args dict contains neccessary keys
        self.utils.check_args(args, ["zone_id_grid", 
                                      "zone_id_list", 
                                      "orig_data"])
        
        # get name of method accoridng to keyword arg
        _method_name = "_zonal_{}".format(zonal_method)
        # define error string to be in case the method not implemented
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
        return getattr(self,_method_name,lambda **x: self.log.log(
            _err_string))(**args)
    
    def _zonal_mean(self,
                    zone_id_grid, 
                    zone_id_list, 
                    orig_data, 
                    zonelistids=None, 
                    ncells=None):
        """Aggregate zones using mean. 
        zonelistids and ncells for compatibility with Cython version."""
        meanlist = np.zeros(zone_id_list.shape)
        for i, zone_id in enumerate(zone_id_list):
            mask = (zone_id_grid == zone_id)
            meanlist[i] = np.mean(orig_data[mask])
        return meanlist
    
    def _zonal_mean_cGeotools(self,
                              zone_id_grid, 
                              zone_id_list, 
                              orig_data, 
                              zonelistids=None, 
                              ncells=None):
        """Aggregate zones using mean (Cython version). 
        zonelistids and ncells for compatibility with Cython version."""
        try:
            from supratool.utils.cGeotools import zonalmean
            meanlist = zonalmean(zone_id_grid.flatten(),
                                 zone_id_list,
                                 orig_data.flatten(),
                                 zonelistids,
                                 ncells)
        except ImportError:
            err_text = "Warning:\tLoading slower Python version of zonalmean."
            self.log.log(err_text)
            meanlist = self._zonal_mean(zone_id_grid,
                                        zone_id_list,
                                        orig_data,
                                        zonelistids,
                                        ncells)
        
        return meanlist
    
    def _zonal_mean_orig_shape(self,
                               zone_id_grid, 
                               zone_id_list, 
                               orig_data, 
                               zonelistids=None, 
                               ncells=None):
        """Aggregate zones using mean returning array in original shape. 
        zonelistids and ncells for compatibility with Cython version."""
        mean_grid = np.zeros(zone_id_grid.shape)
        for i, zone_id in enumerate(zone_id_list):
            mask = (zone_id_grid == zone_id)
            mean_grid[mask] = np.mean(orig_data[mask])
        return mean_grid
    
    def _zonal_median(self,
                      zone_id_grid, 
                      zone_id_list, 
                      orig_data, 
                      zonelistids=None, 
                      ncells=None):
        """Aggregate zones using median. 
        zonelistids and ncells for compatibility with Cython version."""
        medianlist = np.zeros(zone_id_list.shape)
        for i, zone_id in enumerate(zone_id_list):
            mask = (zone_id_grid == zone_id)
            medianlist[i] = np.median(orig_data[mask])
        return medianlist
    
    def _zonal_median_orig_shape(self,
                                 zone_id_grid, 
                                 zone_id_list, 
                                 orig_data, 
                                 zonelistids=None, 
                                 ncells=None):
        """Aggregate zones using median returning array in original shape. 
        zonelistids and ncells for compatibility with Cython version."""
        mean_grid = np.zeros(zone_id_grid.shape)
        for i, zone_id in enumerate(zone_id_list):
            mask = (zone_id_grid == zone_id)
            mean_grid[mask] = np.median(orig_data[mask])
        return mean_grid
    
    def _zonal_mostfreq(self,
                        zone_id_grid, 
                        zone_id_list, 
                        orig_data, 
                        zonelistids=None, 
                        ncells=None):
        """Aggregate zones using most frequent value. 
        zonelistids and ncells for compatibility with Cython version."""
        from scipy import stats
        mfreqlist = np.zeros(zone_id_list.shape)
        for i, zone_id in enumerate(zone_id_list):
            mask = (zone_id_grid == zone_id)
            mfreqlist[i] = stats.mode(orig_data[mask], axis=None)[0][0]
        return mfreqlist
    
    def _zonal_mostfreq_orig_shape(self,
                                   zone_id_grid, 
                                   zone_id_list, 
                                   orig_data, 
                                   zonelistids=None, 
                                   ncells=None):
        """Aggregate zones using mostfreq returning array in original shape. 
        zonelistids and ncells for compatibility with Cython version."""
        mean_grid = np.zeros(zone_id_grid.shape)
        for i, zone_id in enumerate(zone_id_list):
            mask = (zone_id_grid == zone_id)
            mean_grid[mask] = stats.mode(orig_data[mask], axis=None)[0][0]
        return mean_grid
       
    def calc_crns_weights(self,
                          dist_grid,
                          h,
                          sm,
                          P,
                          H_Veg=0):
        """
        Calculate CRNS weights after Schroen et al., 2017.

        Args:
            dist_grid (array-like): Grid with distances to point.
            h (float): absolute air humidity [g/m^3].
            sm (float): soil moisture [fraction].
            P (float): air pressure [hPa].
            H_Veg (float, optional): Height of vegetation [m]. Defaults to 0.

        Returns:
            weight (TYPE): DESCRIPTION.

        """
        
        weight = np.zeros_like(dist_grid)
        for y in range(dist_grid.shape[0]):
            for x in range(dist_grid.shape[1]):
                d = dist_grid[y,x]
                weight[y,x] = W_r_star(d,
                                       h,
                                       sm,
                                       P,
                                       H_Veg)
        return weight
    
    def calc_dist_grid(x_grid,
                       y_grid,
                       x_point,
                       y_point,
                       z_grid = 0,
                       z_point = 0):
        """
        Calculate distance grid.

        Args:
            x_grid (array-like): X-coordinates grid.
            y_grid (array-like): Y-coordinates grid.
            x_point (float): X-coordinates point.
            y_point (float): Y-coordinates point.
            z_grid (array-like, optional): Elevation grid. Defaults to 0.
            z_point (float, optional): Elevation point. Defaults to 0.

        Returns:
            TYPE: Grid with distances to point.
        """
        dx = x_grid - x_point
        dy = y_grid - y_point
        dz = z_grid - z_point
        return np.sqrt((np.square(dx)+np.square(dy)+np.square(dz)))
    
    def calc_footprint(self, origins, dist_grid, max_dist=600):
        """
        Find footprint of simulated neutron origins

        Args:
            origins (array-like): Array with neutron origins.
            dist_grid (array-like): Array with distances.
            max_dist (float, optional): Maximum search distance. Defaults to 600.

        Returns:
            d (float): Distance from where 86.5 % of neutrons origninate.

        """
        origins_pdf = (origins / np.nansum(origins[dist_grid <= max_dist]))
       
        for d in np.arange(0,max_dist,0.1):
            rel_cts = np.nansum(origins_pdf[dist_grid <= d])
            if rel_cts >= 0.865:
                break
        return d
    
    def calc_pressure(self,
                      z):
        """
        Calc mean air pressure based on elevation.

        Args:
            z (float): Elevation of point [m a.s.l.].
        Returns:
            p (float): Air pressure.
        """
        # Formula after FAO Guidlines fir Crop Evapotranspiration, A3, Eq. 7.
        p = 101.3 * ((293.-0.0065*z)/(293.))**5.26
        return p
    
    def crns_weighted_mean(self,
                           val_grid,
                           weight):
        """
        Calculate weighted mean as seen by CRNS

        Args:
            val_grid (array-like): Grid with values.
            weight (array-like): Grid with CRNS weights.

        Returns:
            weighted_value (float): CRNS equivalent value.

        """
        val_grid_w = val_grid * weight
        return np.sum(val_grid_w) / np.sum(weight)


class Gridfile(Geotools):
    """Gridfile object."""
    def __init__(self,
                 log_object):
    
        self.log = log_object
        
    def _calc_scf_factor_scf_dist(self,
                                  scf,
                                  dist,
                                  m=0.412,
                                  t0=4.558,
                                  t1=0.020,
                                  b=1):
        """
        Calculate relative neutron flux rate for patchy snow cover

        Args:
            scf (float or array-like): Snow cover fraction (scf) [-].
            dist (float or array-like): Distance from sensor (dist) [m].
            
            m (float, optional): Parameter, general factor. Defaults to 0.412.
            t0 (float, optional): Parameter, factor for snow cover fraction. Defaults to 4.558.
            t1 (float, optional): Parameter, factor for distance. Defaults to 0.020.
            b (float, optional): Parameter, y-intercept. Defaults to 1.

        Returns:
            rnf (TYPE): DESCRIPTION.

        """
        
        rnf = m * (np.exp(-t0*scf) * np.exp(-t1*dist)) + b
        return rnf

    def _calc_scf_factor_scf_dist_swe(self,
                                      scf,
                                      dist,
                                      swe,
                                      m=0.122,
                                      t0=3.430,
                                      t1=0.020,
                                      t2=2.927,
                                      b=1):
        """
        Calculate relative neutron flux rate for patchy snow cover

        Args:
            scf (float or array-like): Snow cover fraction (scf) [-].
            dist (float or array-like): Distance from sensor (dist) [m].
            swe (float or array-like): Snow water eqivalent (swe) [mm].
            
            m (float, optional): Parameter, general factor. Defaults to 0.122.
            t0 (float, optional): Parameter, factor for snow cover fraction. Defaults to 3.430.
            t1 (float, optional): Parameter, factor for distance. Defaults to 0.020.
            t2 (float, optional): Parameter, factor for snow water equivalent. Defaults to 2.927.
            b (float, optional): Parameter, y-intercept. Defaults to 1.

        Returns:
            rnf (TYPE): DESCRIPTION.

        """
        
        rnf = m * (np.exp(-t0*scf) * np.exp(-t1*dist) * (np.log(swe)/np.log(t2))) + b
        return rnf

    def _calc_scf_factor_scf_dist_swe_sm(self,
                                         scf,
                                         dist,
                                         swe,
                                         sm,
                                         m=0.407,
                                         t0=3.706,
                                         t1=0.019,
                                         t2=7.128,
                                         t3=0.038,
                                         b=1):
        """
        Calculate relative neutron flux rate for patchy snow cover

        Args:
            scf (float or array-like): Snow cover fraction (scf) [-].
            dist (float or array-like): Distance from sensor (dist) [m].
            swe (float or array-like): Snow water eqivalent (swe) [mm].
            sm (float or array-like): Soil moisture (sm) [%].
            
            m (float, optional): Parameter, general factor. Defaults to 0.407.
            t0 (float, optional): Parameter, factor for snow cover fraction. Defaults to 3.706.
            t1 (float, optional): Parameter, factor for distance. Defaults to 0.019.
            t2 (float, optional): Parameter, factor for snow water equivalent. Defaults to 7.128.
            t3 (float, optional): Parameter, factor for soil moisture. Defaults to 0.038.
            b (float, optional): Parameter, y-intercept. Defaults to 1.

        Returns:
            rnf (float): Relative neutron flux rate.

        """
        
        rnf = m * (np.exp(-t0*scf) * np.exp(-t1*dist) * (np.log(swe)/np.log(t2)) * np.exp(-t3*sm)) + b
        return rnf
            
    def _get_aoi(self, domain):
        """get data points in area of interest"""

        min_x = domain.x_min
        max_x = domain.x_max
        min_y = domain.y_min
        max_y = domain.y_max
        
        self.subset = ConfigDict()
        
        # self.subset.add("row_max", int(ceil((self.y_max - min_y) / self.res)))
        # self.subset.add("row_min", int(floor((self.y_max - max_y) / self.res)))
        # self.subset.add("col_min", int(floor((min_x - self.x_min) / self.res)))
        # self.subset.add("col_max", int(ceil((max_x - self.x_min) / self.res)))
        
        self.subset.add("row_max", int(round((self.y_max - min_y) / self.res)))
        self.subset.add("row_min", int(round((self.y_max - max_y) / self.res)))
        self.subset.add("col_min", int(round((min_x - self.x_min) / self.res)))
        self.subset.add("col_max", int(round((max_x - self.x_min) / self.res)))
                
        # check for possible neg values at the lower ends
        for k in ["row_min","col_min"]:
            if self.subset[k] < 0:
                self.log.log("Error:\tGrid domain too small. Extend grid.")
            
        # check if data bounds fit into clip bounds
        if self.height < self.subset["row_max"]:
            self.log.log("Error:\tGrid height too low. Extend grid.")
        
        if self.width < self.subset["col_max"]:
            self.log.log("Error:\tGrid width too low. Extend grid.")
    
    def get_centroids(self):
        """
        Calculate coordinates from corners.

        Returns:
            None.

        """
        self.xlist = np.arange(self.x_min+self.res/2.,self.x_max,self.res)
        self.ylist = np.arange(self.y_min+self.res/2.,self.y_max,self.res)
        self.xgrid,self.ygrid = np.meshgrid(self.xlist,self.ylist)
        self.ygrid = np.rot90(self.ygrid, 2)
    
    def calc_distance_zones(self,
                            station,
                            numsectors=16,
                            distzonewidth=10):
        """
        Calculate distance zones and sectors

        Args:
            station (object): Object defining detector location.
            numsectors (int, optional): Nr of Sectors. Defaults to 16.
            distzonewidth (int, optional): Width of distance zones. Defaults to 10.

        Returns:
            None.

        """
        
        # prepare empty matrices
        self.dist = np.zeros(self.data.shape)
        self.dx = np.zeros(self.data.shape)
        self.dy = np.zeros(self.data.shape)
        self.dz = np.zeros(self.data.shape)
        # prepare meshgrids for x and y
        X = self.xgrid
        Y = self.ygrid
        Z = self.data
        # calculate differences for every direction (x,y,z)
        dx = X-station.x
        dy = Y-station.y
        dz = Z-station.z
        # calculate distance
        self.dist = np.sqrt((np.square(dx)+np.square(dy)+np.square(dz)))
        self.dist_2D = np.sqrt((np.square(dx)+np.square(dy)))
        self.distzones_3D = np.ceil(self.dist / distzonewidth)
        self.distzone_3D_list = np.unique(self.distzones_3D)
        self.distzones = np.ceil(self.dist_2D / distzonewidth)
        self.distzone_list = np.unique(self.distzones)
        # calculate polar coordinates (in degrees)
        self.polar_coords = np.zeros_like(self.data)
        self.polar_coords[self.dist_2D != 0] = np.degrees(
            (np.arcsin(dy[self.dist_2D != 0]/self.dist_2D[
                self.dist_2D != 0]))).astype(np.int32)
        # turn coordinates by 270 degrees to have 360/0 in the north
        self.polar_coords += 270
        # flip southfacing coordinates
        self.polar_coords[dx<0] -= 360
        self.polar_coords[self.polar_coords <0] *= (-1)
        # flip direction to clockwise
        self.polar_coords -= 360
        self.polar_coords *= (-1)
        # calculate sectors
        self.sectors = np.round((self.polar_coords/360)*numsectors)
        self.sectors[self.sectors == numsectors] = 0
        self.sector_list = np.unique(self.sectors)
        # define zones
        self.zones = self.distzones * 1000 + self.sectors
        self.zone_list = np.unique(self.zones)
    
    def calc_scf_factor(self, **args):
        """
        Calculate 

        Args:
            **args (dict): List of arguments.

        Returns:
            None.

        """       
        # get width of distance zones and station elevation
        distzonewidth = args.get("distzonewidth", 50)
        station_elev = args.get("station_elevation", 0)
        # create station object
        station = ConfigDict()
        station.add("x", self.x_min + 0.5*(self.x_max-self.x_min))
        station.add("y", self.y_min + 0.5*(self.y_max-self.y_min))
        station.add("z", station_elev)
        # calculate distance zones
        self.calc_distance_zones(station=station, distzonewidth=distzonewidth)
        
        # create target grid for correction factor
        self.scf_cf_grid = np.ones_like(self.data)

        # create scf grid if not provided
        if "scf" not in args and "scf_grid" not in args:
            args["scf_grid"] = np.ones_like(self.data)
            # calculate from swe grid if provided
            if "swe_grid" in args:
                args_conv = dict()
                args_conv["swe_threshold"] = 0
                args["scf_grid"] = self._reclass_swe_to_sca(args["swe_grid"], 
                                                            **args)
                
        # set cf to one
        cf = 1
                
        # iterate over distance zones
        for dz in self.distzone_list:
            # get distance 
            args["dist"] = 0.5 * distzonewidth * dz
            
            # if a grid is provided, get mean swe for the current dist zone
            if "swe_grid" in args:
                args["swe"] = np.nanmean(args["swe_grid"][self.distzones == dz])
            
            # if a grid is provided, get mean swe for the current dist zone
            if "sm_grid" in args:
                args["sm"] = np.nanmean(args["sm_grid"][self.distzones == dz])
            
            # if a grid is provided, get mean scf for the current dist zone
            if "scf_grid" in args:
                args["scf"] = np.nanmean(args["scf_grid"][self.distzones == dz])
            
            # get name of method accoridng to keyword arg
            _method_name = "_calc_scf_factor_{}".format(args["scf_method"])
            # define error string to be in case the method not implemented
            _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
                _method_name)
            
            # create kw dict for calculation
            cf_args = dict()
            
            # add possible key words
            for kw in ["scf", "dist", "sm", "swe"]:
                if kw in args:
                    cf_args[kw] = args[kw]
            
            # calc correction factor
            cf *= getattr(self,_method_name,lambda **x: self.log.log(
                _err_string))(**cf_args)
            # assign value
            self.scf_cf_grid[self.distzones == dz] = cf
            
        # assign cf
        self.scf_cf = cf
        self.scf_cf_grid = np.nanmean(self.scf_cf_grid)
            
    def convert_soil_matrix_to_sm(self):
        """Convert a URANOS soil matrix to soil moisture values"""
        self.sm = self.data.copy()
        self.sm[self.sm <= 170] /= 2
        self.sm[self.sm == 203] = 10 # building
        self.sm[self.sm == 210] = 10 # road
        self.sm[self.sm == 254] = 100 # water
        self.sm /= 100
    
    def import_grid(self,
                    filename,
                    domain=None,
                    rasterband=1):
        """
        Import grid from file.

        Args:
            filename (string): path to grid.
            numsectors (object, optional): Domain object. Defaults to None.
            rasterband (int): number of band of raster to import.

        Returns:
            None.

        """
        
        if os.path.isfile(filename):
            ds = gdal.Open(filename)
            band = ds.GetRasterBand(rasterband)
            # get geotrans
            args = dict()
            args["geotrans_method"] = "gdal"
            args["dataset"] = ds
            self.get_geotrans(**args)
            # no domain defined
            if domain is None:
                self.data = band.ReadAsArray()
            else:
                # get data for full extent
                data_full = band.ReadAsArray()
                # domain defined, check for aoi
                self.data = self.reduce_data_to_aoi(data_full, domain)
            self.get_centroids()
            # delete temporary variables
            del ds, band
        else:
            self.log.log("ERROR:\tFile {} not found.".format(filename))
    
    def import_id_grid(self,
                       filename):
        """
        Import ID-grid from file, e.g. HRU-ID grid.

        Args:
            filename (string): path to grid.

        Returns:
            None.

        """
        
        self.import_grid(filename)
        self.id_list = np.unique(self.data[np.isfinite(self.data)]).tolist()
        if self.data.dtype == np.uint8 and 255 in self.id_list:
            self.id_list.remove(255)
        
    def reduce_data_to_aoi(self, data_full, domain):
        """get data points in area of interest"""
        # get aoi based on domain
        self._get_aoi(domain)
        
        # reduce data
        data = data_full[self.subset.row_min:self.subset.row_max,
                         self.subset.col_min:self.subset.col_max]
        
        # check for res
        if self.res != domain.res:
            self.log.log("Severe:\tDiffering grid resolution. ")
        
        # set new geotrans from domain
        attr_list = ["x_min", "x_max", "y_min", "y_max", "geotrans"]
        for a in attr_list:
            setattr(self, a, getattr(domain, a))
            
        return data

    
class GridNetCDF(Gridfile):
    """Import grid that is stored as single NetCDF with time dimension"""
    def _check_date(self, it):
        """check whether timestep is available in current file"""
        date_exists = True
        if not hasattr(self, "time_values") or it not in self.time_values:
            date_exists = False
        return date_exists
    
    def get_centroids(self):
        """
        Calculate coordinates from corners.

        Returns:
            None.

        """
        # check x variable name
        if "x" in self.ds.variables:
            x_var = "x"
        elif "lon" in self.ds.variables:
            x_var = "lon"
        # check y variable name
        if "y" in self.ds.variables:
            y_var = "y"
        elif "lon" in self.ds.variables:
            y_var = "lon"
        
        # assign coordinates
        self.xlist = np.array(self.ds.variables[x_var])
        self.ylist = np.array(self.ds.variables[y_var])
            
        # set geographical domain in original crs
        self.res = fabs(self.xlist[1] - self.xlist[0])
        self.x_min = np.min(self.xlist) - 0.5 * self.res
        self.x_max = np.max(self.xlist) + 0.5 * self.res
        self.y_min = np.min(self.ylist) - 0.5 * self.res
        self.y_max = np.max(self.ylist) + 0.5 * self.res
        
        # shape of dataset
        self.ds_height = len(self.ylist)
        self.ds_width = len(self.xlist)
        
        # get arrays of centroids
        self.xgrid,self.ygrid = np.meshgrid(self.xlist,self.ylist)
        self.ygrid = np.rot90(self.ygrid, 2)
        
    def connect_dataset(self,
                        filename):
        """
        Connect NetCDF dataset.

        Args:
            filename (str): path to NetCDF grid.

        Returns:
            None.

        """
        # connect to NetCDF file
        self.ds = netCDF4.Dataset(filename, 'r')
        self.variable_names = list(self.ds.variables)
        # get time variable
        self.time = self.ds.variables['time']
        self.time_values = netCDF4.num2date(self.time[:], self.time.units)
        self.time_values = np.array([
            datetime.strptime(t.isoformat(), "%Y-%m-%dT%H:%M:%S") for t in self.time_values]
            )
        self.time_values = np.array([
            t.replace(microsecond=0) for t in self.time_values
            if t is not None
            ])
        
    def import_grid(self,
                    filename,
                    var,
                    it,
                    domain=None,
                    rasterband=1):
        """
        Import grid from file.

        Args:
            filename (string): path to NetCDF grid.
            var (str): Name of variable to open.
            it (object): Timestep to open.
            numsectors (object, optional): Domain object. Defaults to None.
            rasterband (int): number of band of raster to import.

        Returns:
            None.

        """
        if not hasattr(self, "ds"):
            # connect to NetCDF file
            self.connect_dataset(filename)
            
        # check variable
        if var not in self.variable_names:
            self.log.log("Error:\tVariable name not found.")
        elif not hasattr(self, "data_all"):
            self.data_all = self.ds.variables[var]
            
        # check whether timestep is available in current file
        _date_exists = self._check_date(it)
        if not _date_exists:
            self.log.log("Error:\tDate not in self.time_values")
            
        # open data
        if hasattr(self, "data_all") and _date_exists:
            # get position of timestep
            time_step = np.where(self.time_values == it)[0][0]
            # no domain defined
            if domain is None:
                self.data = self.data_all[time_step, :, :]
                if type(self.data) is ma.masked_array:
                    data = self.data.data
                    data[self.data.mask] = np.nan
                    self.data = data
            else:
                # get data for full extent
                self.data_full = self.data_all[time_step, :, :]
                if type(self.data_full) is ma.masked_array:
                    data_full = self.data_full.data
                    data_full[self.data_full.mask] = np.nan
                    self.data_full = data_full
                # domain defined, check for aoi
                self.data = self.reduce_data_to_aoi(self.data_full, domain)
            self.get_centroids()
        else:
            self.log.log("Error:\tImport data failed.")
            
class PointShape(object):
    """Point Shapefile object"""
    def __init__(self, 
                 log_object,
                 cfg,
                 epsg_code = 4326):
        """
        Create new or open existing point shapefile.

        Args:
            log_object (object): Object containing logfile.
            cfg (object): Scenario config object.
            epsg_code (int, optional): EPSG-code of layer. Defaults to "4326".

        Returns:
            None.

        """
        self.log = log_object
        self.cfg = cfg
        self.epsg_code = epsg_code
        # create geotools class
        self.geo = Geotools(self.log)

    def create_file(self,
                    file_name,
                    layer_name="yulia_output"):
        """create output file
        
        Args:
            file_name (string): File name.
            layer_name (string, optional): Name of the layer. Defaults to "yulia_output".

        Returns:
            None.

        """
        self.driver_name = "ESRI Shapefile"
        self.driver = ogr.GetDriverByName(self.driver_name)
        
        # create the spatial reference
        self.srs = osr.SpatialReference()
        self.srs.ImportFromEPSG(self.epsg_code)
        
        if os.path.exists(file_name):
            # open output shapefile if it already exists
            deleted = self.driver.DeleteDataSource(file_name)
            if deleted == 0:
                _err_text = "Error:\tNot able to delete file. "
                _err_text += "Check filelocks. Try restarting python kernel."
                self.log.log(_err_text)
        # Create the output shapefile
        self.data_source = self.driver.CreateDataSource(file_name)    
        self.layer = self.data_source.CreateLayer(layer_name,
                                                  geom_type=ogr.wkbPoint)
    
    def create_fields(self,
                      field_dict):
        """
        Create fields.
        
        Args:
            field_dict (dict): Dictionary with field names and types.

        Returns:
            None.

        """
        for field_name in field_dict:
            # field value
            _val = field_dict[field_name]
            # generate method name according to variable
            _method_name = "_create_field_{}".format(_val)
            _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
                _method_name)
            # call specific initialisation function
            getattr(self, _method_name, lambda x: self.log.log(_err_string))(
                field_name)
    
    def _create_field_str(self,
                          field_name,
                          field_len=24):
        """Create field for string"""
        _field = ogr.FieldDefn(field_name, ogr.OFTString)
        _field.SetWidth(field_len)
        self.layer.CreateField(_field)
        
    def _create_field_int(self,
                          field_name):
        """Create field for integer"""
        self.layer.CreateField(ogr.FieldDefn(field_name, ogr.OFTInteger)) 
        
    def _create_field_float(self,
                            field_name):
        """Create field for floats"""
        self.layer.CreateField(ogr.FieldDefn(field_name, ogr.OFTReal)) 
    
    def add_point(self,
                  xlist,
                  ylist,
                  zlist,
                  **args):
        """
        Add point to shapefile object.

        Args:
            x (float): X-coordinate.
            y (float): Y-coordinate.
            z (float): Elevation.
            **args (dict): additional fields.

        Returns:
            None.
        """
        # check if float
        if type(xlist) is not list:
            xlist = [xlist]
        if type(ylist) is not list:
            ylist = [ylist]
        if type(zlist) is not list:
            zlist = [zlist]
        for x, i in enumerate(xlist):
            y = ylist[i]
            z = zlist[i]
            # create feature
            feature = ogr.Feature(self.layer.GetLayerDefn())
            # set attributes
            for _field_name in args.keys():
                feature.SetField(_field_name, args[_field_name])
            # create point
            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(x, y, z)
            # Set the feature geometry using the point
            feature.SetGeometry(point)
            # Create the feature in the layer (shapefile)
            self.layer.CreateFeature(feature)
            # Dereference the feature
            feature = None
        
    def import_data(self, 
                    file_name):
        
        if os.path.isfile(file_name):
            shape_data = ogr.Open(file_name, 0)
        else:
            err_text = f"Error:\tCould not open file {file_name}"
            self.log.log(err_text)
            return
        
        # get layer of shapefile
        layer = shape_data.GetLayer()
        
        # check if there is need for reprojecting the files
        reproject_on_the_fly = self._check_epsg_codes(layer)
        
        # save column names
        self._get_layer_namelist(layer)
        
        # check if a field with names exists
        self._get_name_field(layer)
        
        # chek if a filed with elevation exists
        self._get_elevation_field(layer)
        
        # set ConfigDict object for features and empty names list
        self.points = ConfigDict()
        point_names = list()
        
        # iterate over features
        for index in range(layer.GetFeatureCount()):
            
            # get feature and geometry
            feature = layer.GetFeature(index)
            geometry = feature.GetGeometryRef()
            x_coord = geometry.GetX()
            y_coord = geometry.GetY()
            
            # re-project on-the-fly if needed
            if reproject_on_the_fly:
                reproj_args = dict()
                reproj_args["xlist"] = [x_coord]
                reproj_args["ylist"] = [y_coord]
                reproj_args["epsg_in"] = int(self.cfg.epsg_shapefile)
                reproj_args["epsg_out"] = int(self.cfg.epsg)
                reproj_args["reproj_engine"] = self.cfg.get("reproj_engine", 
                                                            "gdal")
                x_coord_reproj, y_coord_reproj = self.geo.reproject(**reproj_args)
                x_coord = x_coord_reproj[0]
                y_coord = y_coord_reproj[0]
            
            # get name of feature
            if self.cfg.name_defined:
                feature_name = feature.GetField(self.cfg.name_field)
                if len(feature_name) < 1:
                    feature_name = "Point_" + str(index)
            else:
                feature_name = "Point_" + str(index)
            
            # save poiint name
            point_names.append(feature_name)    
            
            # create instance for current feature
            self.points.add_subclass(feature_name)
            
            # save coordinates
            point = getattr(self.points, feature_name)
            point.add("x", x_coord)
            point.add("y", y_coord)
            
            # add elevation data if defined
            if self.cfg.elev_defined:
                point.add("z", feature.GetField(self.cfg.elev_field))
            
            # add other field data
            for i, fieldname in enumerate(self.column_names):
                point.add(fieldname, feature.GetField(i))
                
    def _check_epsg_codes(self, layer):
        """Get epsg code of layer"""
        
        reproj = False
    
        if self.cfg.epsg != self.epsg_code:
            reproj = True
    
        # add to cfg
        self.cfg.add("epsg_shapefile", self.epsg_code)
    
        return reproj
    
    def _get_layer_namelist(self, layer):
        """Get names of layer columns"""
        self.column_names = list()
        for i in range(layer.GetLayerDefn().GetFieldCount()):
            self.column_names.append(
                layer.GetLayerDefn().GetFieldDefn(i).GetName())
    
    def _get_elevation_field(self, layer):
        """checks whether a field with station elevations exists."""
        self.cfg.add("elev_defined", False)
        for i,fieldname in enumerate(self.column_names):
            if re.search("[Ee]lev", fieldname) or re.search("ELEV", fieldname):
                self.cfg.add("elev_field", i)
                self.cfg.add("elev_field_name", fieldname)
                self.cfg.add("elev_defined", True)
    
    def _get_name_field(self, layer):
        """checks whether a field with station names exists."""
        self.cfg.add("name_defined", False)
        for i,fieldname in enumerate(self.column_names):
            if re.search("[Nn][Aa][Mm][Ee]", fieldname):
                self.cfg.add("name_field", i)
                self.cfg.add("name_field_name", fieldname)
                self.cfg.add("name_defined", True)
        if not self.cfg.name_defined:
            e = "Error:\tPoint Shapefile has no name field"
            self.log.log(e)
    
    def _get_point_id(self, geotrans, xcoord, ycoord):
        """returns rows and colums of a point. """
        res = geotrans[1]
        basin_min_x = geotrans[0]
        basin_max_y = geotrans[3]
        row = int(floor((basin_max_y - ycoord) / res))
        col = int(floor((xcoord - basin_min_x) / res))
        return row, col
    
    def get_point(self, loc):
        """
        Return point information based on location name.

        Args:
            loc (str): Name of location (point).

        Returns:
            Point (obj). Config dict obj of location.

        """
        if hasattr(self.points, loc):
            return getattr(self.points, loc)
        else:
            self.log.log(f"Error:\tLocation {loc} not found in shapefile.")
            return None
        
    def update_elev_from_dem(self, location, domain):
        """
        Update point elevation from DEM

        Args:
            location (str): Name of location (point).
            domain (object): Domain object with DEM.

        Returns:
            None.

        """
        # get point
        loc = self.get_point(location)
        
        # get row and col for this point
        row, col = self._get_point_id(domain.geotrans, loc.x, loc.y)
        
        # get elevation from DEM
        loc.add("z", domain.dem[row, col])
        
    def __del__(self):
        """
        Save and close the data source.

        Returns:
            None.

        """
        self.data_source = None