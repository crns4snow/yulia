# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 11:16:35 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from __future__ import division, print_function
from math import exp
from scipy import integrate

def f_p(p):
    p0 = 1013.25
    f = 0.4922 / (0.86-exp(-p/p0))
    return f
    
def f_veg(H_veg):
    a = 1-0.17*(1-exp(-0.41*H_veg))*(1+exp(-9.25))
    return a
    
def D86(r,bulk,theta):
    p0 = 8.321
    p1 = 0.14249
    p2 = 0.96655
    p3 = 0.01
    p4 = 20.0
    p5 = 0.0429
    a = 1.0/bulk * (p0 + p1 * (p2 + exp(-p3*r)) * (p4 + theta) / (p5 + theta) )
    return a
    
def F0():
    p0 = 3.7
    return p0
    
def F1(h,theta):
    p0 = 8735
    p1 = 22.689
    p2 = 11720
    p3 = 0.00978
    p4 = 9306
    p5 = 0.003632
    a = (p0*(1+p3*h)) * exp(-p1*theta) + p2*(1+p5*h)-p4*theta
    return a
    
def F2(h,theta):
    p0 = 2.7925e-2# * 10**-2
    p1 = 6.6577
    p2 = 2.8544e-2# * 10**-2
    p3 = 0.002455
    p4 = 6.851e-5# * 10**-5
    p5 = 12.2755
    a = ((p4*h-p0) * exp((-p1*theta)/(1+p5*theta))+p2) * (1+p3*h)
    return a

def F3(h,theta):
    p0 = 247970
    p1 = 23.289
    p2 = 374655
    p3 = 0.00191
    p4 = 258552
    a = p0 * (1+p3*h) * exp(-p1*theta) +p2 -p4*theta
    return a
    
def F4(h,theta):
    p0 = 5.4818e-2# * 10**-2
    p1 = 21.032
    p2 = 0.6373
    p3 = 7.91e-2# * 10**-2
    p4 = 5.425e-4# * 10**-4
    a = p0*exp(-p1*theta) + p2 - p3*theta + p4*h
    return a

def F5(h,theta):
    p0 = 39006
    p1 = 15002330
    p2 = 2009.24
    p3 = 0.01181
    p4 = 3.146
    p5 = 16.7417
    p6 = 3727
    a = (p0 - p1 / (p2*theta + h - 0.13)) * (p3 - theta) * exp(-p4*theta) - p5 * h * theta + p6
    return a

def F6(h,theta):
    p0 = 6.031 * 10**-5
    p1 = 98.5
    p2 = 0.0013826
    a = p0*(h+p1)+p2*theta
    return a
    
def F7(h,theta):
    p0 = 11747
    p1 = 55.033
    p2 = 4521
    p3 = 0.01998
    p4 = 0.00604
    p5 = 3347.4
    p6 = 0.00475
    a = (p0 * (1-p6*h)*exp(-p1*theta*(1-p4*h))+p2-p5*theta) * (2+p3*h)
    return a
    
def F8(h,theta):
    p0 = 1.543 * 10**-2
    p1 = 13.29
    p2 = 1.807 * 10**-2
    p3 = 0.0011
    p4 = 8.81 * 10**-5
    p5 = 0.0405
    p6 = 26.74
    a = ((p4*h-p0)*exp((-p1*theta)/(1+p5*h+p6*theta)) + p2) + (2+p3*h)
    return a
    
def W_r(r,h,theta):
    w = 0
    if r <= 1:
        w = (F1(h,theta) * exp(-F2(h,theta)*r) + F3(h,theta) * exp(-F4(h,theta)*r)) * (1 - exp(-F0()*r))
    elif r > 0.5 and r <= 50:
        w = F1(h,theta) * exp(-F2(h,theta)*r) + F3(h,theta) * exp(-F4(h,theta)*r)
    elif r > 50 and r <= 600:
        w = F5(h,theta) * exp(-F6(h,theta)*r) + F7(h,theta) * exp(-F8(h,theta)*r)
    else:
        print("Error: radius too small or too big")
    return w

def W_r_star(r,h,theta,p,H_veg=0):
    r_star = r / f_p(p) / f_veg(H_veg)
    w = W_r(r_star,h,theta)
    return w
    
def R_86(h,theta):
    total = integrate.quad(W_r,0.5,600, args=(h,theta,))
    r86 = 1
    temp = [0.0,0.0]
    p = 0.86
    while temp[0]/total[0] < p:
        temp = integrate.quad(W_r,0.5,r86, args=(h,theta,))
        r86 += 1
    r = 0.51
    total = 0
    while r <= 600:
        total += W_r(r,h,theta)
        r += 0.1
    r86_2 = 0.51
    temp = 0
    while temp/total < p:
        temp += W_r(r86_2,h,theta)
        r86_2 += 0.1
    return r86,r86_2

def R_86_grid(grid,distgrid):
    import numpy as np
    grid[distgrid <= 0.5] = np.nan
    grid[distgrid >= 600] = np.nan
    #total = np.nansum(grid)/(3.14*600)
    r = 0.5
    pi = 3.14
    i = 0.2
    total = 0
    while r <= 600:
        selected = np.logical_and(distgrid > r,distgrid <= r+i)
        total += np.nansum(grid[selected])/((2*(r+0.5*i)*pi)-1)
        r += i
    r86 = 0.5
    temp = 0
    while temp/total < 0.86:
        selected = np.logical_and(distgrid > r86,distgrid <= r86+i)
        temp += np.nansum(grid[selected])/((2*(r86+0.5*i)*pi)-1)
        r86 += i
    return r86

def R_86_weighted(p,H_veg,r86):
    r86w = f_p(p) * f_veg(H_veg) * r86
    return r86,r86w