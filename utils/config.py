# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 15:32:31 2020

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
from collections import OrderedDict

# python package imports
import pandas as pd
import copy


class ConfigDict(OrderedDict):
    """extended configuration dictionary class """
        
    def add(self, key, value, dropna=True):
        """
        method to add additional key:value pairs, also excessable by attribut;
        comma sep strings are parsed to lists while leading and trailing 
        whitespace are deleted
        """
        #convert bool string to  type bool
        if isinstance(value, str):
            if value == "True":
                value = True
            if value == "False":
                value = False
        
        #convert strings containing comma to list
        if isinstance(value, str) and (',' in value):
            value = [x.strip() for x in value.split(',')]
        
        #check if input is float nan (empty xls field) and drop 
        if not (dropna==True and (isinstance(value, float) and pd.isnull(value))):  
            # also ignore entries without correct key
            if key != 0:
                # add to dict
                self[key] = value
                # additionally set as attribute
                setattr(self,
                        key,
                        value)
                
    def add_subclass(self,
                     name,
                     subset=None):
        """ adds a new sub-ConfigDict, 
            optionally parse subset (dict or pd.series) to new subclass"""
        
        if name != 0:
            self.add(name,
                     ConfigDict())
        
            if isinstance(subset, 
                          pd.Series):
                self[name].add('name', name)
                self[name]._add_series_to_cfgdict(subset)
            if isinstance(subset, 
                          dict):
                self[name]._add_series_to_cfgdict(pd.Series(subset))
            
    def read_csv(self, filename):
        """import config dict from csv, wrapper for pandas read_csv"""
        df = pd.read_csv(filename, sep=" = ", index_col=0, engine='python')
        for key, value in df.iterrows():
            self.add(value.name, value.value)
            
    def remove(self, key):
        """remove key from config dict"""
        # remove attribute
        if hasattr(self, key):
            delattr(self, key)
        
        # remove key from dict
        if key in self:
            self.pop(key, None)
        
            
    def to_csv(self, filename):
        """export config dict to csv"""
        with open(filename, "w+") as f:
            # add header
            f.write("# key = value\n")
            # add entries
            for i, key in enumerate(self):
                f.write("{} = {}\n".format(key, self[key]))
            
    def _add_series_to_cfgdict(self, series):
        """ adds all pd.series entries to cfg dict, 
            key sub-string 'ignored' will be excluded"""
        for key, value in series.items():
            if 'ignored' in key:
                pass
            else:
                self.add(key, value)
               
            
    def check_list(self, key):
        """
        method check and force variable to type list
        
        Args:
            key (str): variable to be checked.
        """
        if hasattr(self, key):
            
            var = getattr(self, key)
            
            if not isinstance(var, list):
                self.add(key, [var])
        # check if there is an existing out_channels list from config
        else:
            self.add(key, list())
            
    def copy(self):
        """
        returns a deepcopy of self
        """
        return copy.deepcopy(self)
        
            
class Config(ConfigDict):
    """
    Main config class for supratool based on extended dictionaries. The class 
    can be initialised by the corresbonding xlsx table containing all 
    information.
    """
    
    def __init__(self, 
                 filename=None,
                 print_status=True):
        
        self.add_subclass('operate')
        self.add_subclass('generate')
                
        if filename is not None:       
            #add filename to cfg 
            self.operate.add('cfg_filename', filename)
            self._read_config(filename,
                              print_status)
            
    def _read_config(self, 
                     filename,
                     print_status):
        
        if print_status:
            print('Reading config from file: {}'.format(filename))

        data = pd.read_excel(filename, 
                             sheet_name=None)
        
        for sheet in data.keys():
            items_df = data[sheet]
            items_df = items_df.dropna(axis=0, how='all')
            
            # drop all  lines marked with '#'
            if '#' in items_df.columns:
                items_df = items_df[items_df['#'] != '#']
                items_df = items_df.drop(columns=['#'])
            
            # check for sheet with extra information
            if len(sheet.split("_")) > 1:
                main_sheet = sheet.split("_")[0]
                sub_sheet = sheet[len(main_sheet)+1:]
                
                # check if sheet already exists
                if not hasattr(self, main_sheet):
                    self.add_subclass(main_sheet)
                
                # add sub_sheet as subclass
                getattr(self, main_sheet).add_subclass(sub_sheet)
                
                # rearange subsheet df
                items_df.index = items_df.name
                items_df.drop(columns="name", 
                              inplace=True)
                
                # add df to sub_sheet subclass
                for key, series in items_df.iterrows():
                    getattr(getattr(self, main_sheet), 
                            sub_sheet).add_subclass(key, series)

            else:
                # check if sheet already exists
                if not hasattr(self, sheet):
                    self.add_subclass(sheet)
                    
                # add items to subclass
                for i, item in items_df.iterrows():
                    self[sheet].add(item.loc["name"], 
                                     item.loc["value"])
