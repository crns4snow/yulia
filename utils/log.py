# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 13:44:08 2020

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python import
import datetime
import os
import sys

# yulia import
from yulia.utils import Utils


class Logfile():
    """creating a log file"""

    def __init__(self,
                 wd,
                 verbose=False,
                 exit_if_severe=True,
                 existing_filename=None):
        """
        Initialise logfile.

        Args:
            wd (string): working directory.
            verbose (bool, optional): verbosee mode. Defaults to False.
            exit_if_severe (bool, optional): Sys.exit() on severe errors. Defaults to False.
            existing_filename (string, optional): Name of existing logfile. Defaults to None.

        Returns:
            None.

        """
        self._utils = Utils()
        # default: create new file
        if existing_filename is None:
            timestamp = datetime.datetime.now()
            logfolder = os.path.join(wd,'log')
            self._utils.check_folders([wd,logfolder])
            self.f = os.path.join(logfolder,"logfile_" + timestamp.strftime(
                "%Y-%m-%d_%H%M" + ".txt"))
            logf = open(self.f, "w+")
            logf.close()
        # use some old file
        else:
            self.f = existing_filename
        self.verbose = verbose
        self.exit_if_severe = exit_if_severe

    def log(self, 
            text):
        """
        Write text to logfile and print it in verbose mode.

        Args:
            text (string): Text to be logged.

        Returns:
            None.

        """
        
        # create timestamp
        t = datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S ")
        
               
        logf = open(self.f, "a+")
        t += text + "\n"
        logf.write(t)
        if self.verbose:
            print(text)
        if self.exit_if_severe:
            if text[:6] == "SEVERE" or text[:6] == "Severe":
                print(text)
                text = "\nTerminating Program due to severe error. See logfile for details."
                logf.write(text)
                print(text)
                sys.exit()
        logf.close()