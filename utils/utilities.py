# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 14:39:41 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import os


class log_print(object):
    def log(self,
            text):
        print(text)

class Utils(object):
    """Class with utility functions."""
    def __init__(self, 
                 log_object=None):
        """
        Initialise with logfile object.

        Args:
            log_object (object, optional): Logfile. Defaults to None.

        Returns:
            None.

        """
        
        if log_object is not None:
            self.log = log_object
        else: 
            self.log = log_print()
    
    def check_args(self,
                   args, 
                   item_list):
        """
        Check if args dict contains neccessary item(s).

        Args:
            args (dict): Argument dictionary.
            item_list (list of string): List of keywords.

        Returns:
            None.

        """
        
        for item in item_list:
            if item not in args:
                self.log.log("ERROR:\tKeyword '{}' is missing in args.".format(item))
    
    def check_folders(self,
                      folders):
        """check for folders need and creates them if necessary
        

        Args:
            folders (list of strings): DESCRIPTION.
            verbous (bool, optional): verbous mode. Defaults to False.

        Returns:
            None.

        """
    
        for folder in folders:    
            if len(folder) > 0 and not os.path.exists(folder):
                os.mkdir(folder)
    
    def check_file(self,
                      file, message='ERROR:\t file "{}" not found.'):
        """check for existence of file and issue message and exit, if missing
        
        Args:
            file (string): file name.
            message (string, optional): Error message to issue.

        Returns:
            None.

        """
        if not os.path.exists(file):
            _err_string = message.format(file)
            self.log.log(_err_string)
            quit()
                
    def check_if_list(self,
                      value):
        """
        Check if value is list. Convert to list if not.
    
        Args:
            value (*): input variable (any type).
    
        Returns:
            value (list): DESCRIPTION.
    
        """
        if type(value) is not list:
            value = [value]
        return value
    
    def generate_exportfolder(self, 
                              subfolder_list):
        """
        Generate exportfolder string checking and creating subfolders.

        Args:
            subfolder_list (list): Hierarchical list of subfolders.

        Returns:
            exportfolder (str): Exportfolder.

        """
        exportfolder = ""
        for subfolder in subfolder_list:
            exportfolder = os.path.join(exportfolder, subfolder)
            if ":" in subfolder:
                continue
            self.check_folders([exportfolder])
        return exportfolder
    
    def print_progress_bar(self,
                           iteration,
                           total, 
                           prefix = 'Progress:',  
                           suffix = '',
                           decimals = 1, 
                           length = 50, 
                           fill = '█', 
                           printEnd = "\r"):
        """
        Call in a loop to create terminal progress bar
        Source: https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console   
    
        Args:
            iteration (int): current iteration.
            total (int): total iterations.
            prefix (str, optional): prefix string. Defaults to 'Progress:'.
            decimals (int, optional): positive number of decimals in percent complete. Defaults to 1.
            length (int, optional): character length of bar. Defaults to 50.
            fill (str, optional): bar fill character. Defaults to '█'.
            printEnd (str, optional): end character. Defaults to "\r".
    
        """
        
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
        if iteration == total:
            print("\n")
    
    def string_to_list(self,
                       value_dict,
                       value_key_list,
                       sep=","):
        """
        Convert string in value dict to list.

        Args:
            value_dict (dict): Dictionary with values.
            value_key_list (iteratable): List of value key.
            sep (str, optional): Separator. Defaults to ",".

        Returns:
            value_dict (dict): Adapted dictionary.
        """
        
        for _key in value_key_list:
            value_dict[_key] = value_dict[_key].split(sep)
            
        return value_dict