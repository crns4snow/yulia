# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 20:37:37 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# generic python imports
from math import pi
from math import exp

# python package imports
import numpy as np


class Meteo(object):
    def calc_air_humidity(self, rh, t):
        """
        Calculate absolute air humidity.

        Args:
            rh (float or array-like): Relative humidity [%].
            t (float or array-like): Air temperature [deg C].

        Returns:
            float or array-like: Air humidity.

        """
        self.t = t              # temperatrure [deg C]
        self.T = t + 273.15     # temperature [K]
        self.rh = rh            # relative humidity [%]
        return self.calc_rho()
    
    def calc_air_pressure(self, z):
        """
        Calculate mean air pressure according to elevation.
        Based on FAO meteorological furmulas, eq (7).
    
        Args:
            z (float or array-like): Elevation [m].
    
        Returns:
            p (float or array-like): Air pressure [mb].
    
        """
        return 1013*np.power(((293-0.0065*z)/293), 5.26)
            
    def calc_atm_depth(self, p):
        """
        Calc atmospheric depth [g/cm^2] from air pressure [mbar].
        Delunel et al., 2014, eq. 5.

        Args:
            p (float or array-like): air pressure [mbar].

        Returns:
            d (float or array-like): Atmospheric depth [g/cm^2].

        """
        g = 9.807 # m s^2
        return 10*p/g
    
    def calc_atm_depth_from_elev(self, z):
        """
        Calc atmospheric depth [g/cm^2] from air pressure [mbar].
        Delunel et al., 2014, eq. 5.

        Args:
            z (float or array-like): Elevation [m].

        Returns:
            d (float or array-like): Atmospheric depth [g/cm^2].

        """
        g = 9.807 # m s^2
        return 10*self.calc_air_pressure(z)/g
    
    def calc_rho(self):
        """
        Calculate water vapor density in the free atmosphere [kg / m3|
        Harder and Pomeroy, 2013 - A.8. 
        Needed: relative humidity RH [%] and air temperature [K]      

        Returns:
            float or array-like: water vapor density [kg / m3|.

        """
        
        mw = 0.01801528             # weight water vapor [kg/mol]
        R = 8.31441               # universal gas constant [J / mol * kg]
        self.calc_sat_vp()
        self.rho = 1000. * (mw * ((self.rh/100.)*self.vp)) / (R * (self.T))
        return self.rho
    
    def calc_sat_vp(self):
        """
        Calculate saturation vapor pressure [Pa]. 
        Magnus form eq as used in SES

        Returns:
            None.

        """

        # saturation vapor pressure at triple point temperature [Pa]
        e0      = 610.7      
        self.vp = e0 * np.exp(22.33*(self.t) / (self.T-2))
    
    def calc_topographic_shileding_factor(self, horizon_angle, m=2.3):
        """
        

        Args:
            horizon_angle (float): Angle of the horizon in degree.
            m (string, optional): Weighting factor. Defaults to 2.3.

        Returns:
            float: topographic shielding factor.

        """
        
        # convert degree to radians
        h_r = (horizon_angle/360) * (2*pi)

        return 1-np.sin(h_r)**(1+m)

    def calc_cutoff_rigidity_scaling(self, Rc, alpha=10.275, k=0.9615):
        """
        lattitude scaling factor (Dorman function, from Desilets et al 2006.

        Args:
            Rc (float): Cutoff rigidity. 
            alpha (float, optional): alpha parameter. Defaults to 10.275.
            k (float, optional): k parameter. Defaults to 0.9615.

        Returns:
            float: cutoff rigidity scaling factor.

        """
        
        return 1 - exp(-alpha*Rc**(-k))