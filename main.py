# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 11:25:09 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from yulia.operate import Core


#f = "_configs/yulia_config_sierra_coarse.xlsx"
#f = "_configs/yulia_config_hexland_tracks.xlsx"
#f = "_configs/yulia_config_hexland_tracks_0.xlsx"
#f = "_configs/yulia_config_hexland_tracks_dry.xlsx"
#f = "_configs/yulia_config_hexland_tracks_0_dry.xlsx"
#f = "_configs/yulia_config_sierra_neutronica.xlsx"
#f = "_configs/yulia_config_sierra_neutronica2.xlsx"
f = "_configs/yulia_config_agia.xlsx"
# f = "_configs/yulia_config_sierra_half.xlsx"
#f = "_configs/yulia_config_test_air_corr.xlsx"

c = Core(config_file_name=f)
c()