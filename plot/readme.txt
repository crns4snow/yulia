plot.py – DYNAMIC PLOTTING ENGINE
General plotting class for 'quick and dirty' standard plotting of work-in-progress URANOS results. All configurations should be set in the ‘config.xlsx’ file (sheet name: ‘plot’). Path and creation date of the results file ‘YYYY-MM-DD_ncts.csv’ MUST be given in the ‘config.xlsx’-file.

DYNAMIC OPTIONS TO BE SET IN THE ‘CONFIG.XLSX’ FILE:
-	Y-values: 1 to 5 energy levels
-	X-values: if multiple x values are given, multiple plots will be produced
-	plot layout, paths, names, labels, plotting/saving options

STATIC VALUES:
-	colors for energy levels

TO IMPLEMENT:
-	Dynamic colors for energy levels (optional).
-	Log file entry (optional). As of now, empty log file will be produced.
-	Check variable names: pep8 (e.g. energyList)
-	self.add('cfg', cfg); also for log file;
-	automise paths ('file_path', 'save_plt'); create subfolder plots automatically;
