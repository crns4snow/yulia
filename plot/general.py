# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 09:55:15 2022

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Jan Schmieder.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# python package imports
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

class General():
    '''
    Plotting class for work in progress plots.
    Y- variable is always either neutron counts or scaled neutron counts.
    X-variable is dynamic, e.g. SWE, SCF or multiple (produces multiple plots)
    Plot settings should be adapted in the 'config.xslx'-file.
    '''    
    def __init__(self,
                 cfg,
                 log):
        """
        Initialise class

        Args:
            cfg (object): plotting cfg object.
        """        
        # get cfg for plotting
        self.cfg = cfg
        
        # log obj
        self.log = log
        
        # look-up list (all energy levels)
        self.cfg.energyList = ["ThermalNeutron",         # 1
                               "Epithermal",             # 2
                               "IntermediateEnergy",     # 3
                               "FastNeutron",            # 4
                               "Selected"]               # 5
        self.cfg.colorList = ["darkblue",                # 1
                              "blue",                    # 2
                              "blueviolet",              # 3
                              "purple",                  # 4
                              "orange"]                  # 5      
        
    def read_ncts(self):
        '''
        Import .csv-files (neutron counts and respective standard deviation)
        and join data frames.
        '''
        # get file path of 'YYYY-MM-DD_ncts.csv'
        if hasattr(self.cfg.plot, 'file_path'):
            file_path = self.cfg.plot.file_path
        else:
            sys.exit("\n"
                     "Please specify 'file_path' in 'config.xlsx'!")                  
        
        # get file creation date (YYYY-MM-DD)
        if hasattr(self.cfg.plot, 'file_date'):
            file_date = self.cfg.plot.file_date
        else:
            sys.exit("\n"
                     "Please specify 'file_date' " 
                     "(YYYY-MM-DD) in 'config.xlsx'!")                  
        
        # load neutron counts
        ncts = pd.read_csv(file_path + '/' + 
                           file_date + "_ncts.csv",
                           index_col=0)
        
        # load neutron counts standard deviation
        std = pd.read_csv(file_path + '/' + 
                          file_date + "_std.csv",
                          index_col=0)
        
        ## join ntcs and std
        ncts = pd.merge(ncts, std[self.cfg.energyList],
                        how="left", left_index=True, right_index=True,
                        suffixes=('', '_std'))
        return ncts

    def scale_ncts(self):
        '''
        Relative Neutron Counts, Scaled to Scenario Minimum [%].
        
        Please set scale factors (nThe...) manually in config.xlsx!
        If not set, the minimum value for each
            energy level will be calculated and used!
        
        '''
        # specify data frame
        ncts = self.cfg.ncts
        
        # get scaling switch
        if hasattr(self.cfg.plot, 'scale_y'):
            self.scale_y = self.cfg.plot.scale_y
        else:
            self.scale_y = False
            
        # scaling
        if self.scale_y == True:
            ## get neutron counts for scenario minimum
            # thermal neutrons [per million]
            if hasattr(self.cfg.plot, 'nThe'):
                nThe = self.cfg.plot.nThe # 536.42
            else:
                nThe = min(ncts['ThermalNeutron'])
            # epitheral neutrons [per million]
            if hasattr(self.cfg.plot, 'nEpi'):
                nEpi = self.cfg.plot.nEpi # 305.31
            else:
                nEpi = min(ncts['Epithermal'])
            # intermediate energy neutrons [per million]
            if hasattr(self.cfg.plot, 'nInt'):
                nInt = self.cfg.plot.nInt # 441.22
            else:
                nInt = min(ncts['IntermediateEnergy'])
            # fast neutrons [per million]
            if hasattr(self.cfg.plot, 'nFas'):
                nFas = self.cfg.plot.nFas # 668.01
            else:
                nFas = min(ncts['FastNeutron'])
            # selected neutrons [per million]
            if hasattr(self.cfg.plot, 'nSel'):
                nSel = self.cfg.plot.nSel # 422.47
            else:
                nSel = min(ncts['Selected'])
            
            # scale it [-]
            ncts['ThermalNeutron'] /= nThe
            ncts['ThermalNeutron_std'] /= nThe
            ncts['Epithermal'] /= nEpi
            ncts['Epithermal_std'] /= nEpi
            ncts['IntermediateEnergy'] /= nInt
            ncts['IntermediateEnergy_std'] /= nInt
            ncts['FastNeutron'] /= nFas
            ncts['FastNeutron_std'] /= nFas
            ncts['Selected'] /= nSel
            ncts['Selected_std'] /= nSel
            
            # to [%]
            ncts['ThermalNeutron'] *= 100
            ncts['ThermalNeutron_std'] *= 100
            ncts['Epithermal'] *= 100
            ncts['Epithermal_std'] *= 100
            ncts['IntermediateEnergy'] *= 100
            ncts['IntermediateEnergy_std'] *= 100
            ncts['FastNeutron'] *= 100
            ncts['FastNeutron_std'] *= 100
            ncts['Selected'] *= 100
            ncts['Selected_std'] *= 100
        else:
            pass
        return ncts

    def from_to_radii(self):
        '''
        Derive from-to radii of donuts (distance zones) [in m].
        '''
        # specify data frame
        _ncts = self.cfg.ncts
        
        # get distance zone number and respective width
        donut_size = _ncts[['distzone', 'width_distance_zones']] # subset df
        donut_size = donut_size.groupby(['distzone'],           # aggregate df
                                        as_index=False).mean() 
        # rename
        donut_size.columns=['_zone','_width']
        # get inner radii
        donut_size['_from'] = (donut_size['_zone'] * 
                               donut_size['_width']) - donut_size['_width']
        #get outer radii
        donut_size['_to'] = (donut_size['_zone'] *
                             donut_size['_width'])     
        return donut_size       
    
    def plot_specs(self):
        '''
        Get plot specifications from 'config.xlsx'.
        Set default specs.
        '''
        # show plot
        if hasattr(self.cfg.plot, 'show_plt'):
            self.show_plt = self.cfg.plot.show_plt     
        else:
            self.show_plt= True     

        # save plot
        if hasattr(self.cfg.plot, 'save_plt'):
            self.save_plt = self.cfg.plot.save_plt     
        else:
            self.save_plt= False     

        # plot layout: sub plot matrix (n_rows, n_cols)
        if hasattr(self.cfg.plot, 'n_rows'):
            n_rows = self.cfg.plot.n_rows
        else:
            n_rows = 1
        if hasattr(self.cfg.plot, 'n_cols'):
            n_cols = self.cfg.plot.n_cols
        else:
            n_cols = len(self.cfg.donut_size)
        self.plot_layout = (n_rows, n_cols)
        
        # variable to split data into sub plots (str)
        if hasattr(self.cfg.plot, 'sub_var'):
            self.sub_var = self.cfg.plot.sub_var
        else:
            self.sub_var = 'distzone'
 
        # x-variable(s) (str)
        if hasattr(self.cfg.plot, 'x'):
            self.x = self.cfg.plot.x
        else:
            sys.exit("\n"
                     "Please specify 'x'-variable in 'config.xlsx'!")                  
        
        # figure size (width, heigth)
        cm = 1 / 2.54
        if hasattr(self.cfg.plot,'plt_width'):
            width = self.cfg.plot.plt_width
        else:
            width = 30
        if hasattr(self.cfg.plot, 'plt_height'):
            height = self.cfg.plot.plt_height
        else:
            height = 15
        self.fig_size = (width * cm, height * cm)
      
        # get scenario name from .csv-file    
        self.sc_name = self.cfg.ncts.index[0].split('_')[0]

        # super title
        if hasattr(self.cfg.plot, 'title'):
            self.suptitle = self.cfg.plot.title     
        else:
            self.suptitle = f"Scenario: '{self.sc_name}'"     

        # plot path
        if self.save_plt == True:
           if hasattr(self.cfg.plot, 'plt_path'):
               self.plt_path = self.cfg.plot.plt_path
           else:
               sys.exit("\n"
                        "Please specify 'plt_path' in 'config.xlsx'!")                  
        else:
            pass     
                           
        # plot format
        if hasattr(self.cfg.plot, 'plt_fmt'):
            self.plt_fmt = self.cfg.plot.plt_fmt     
        else:
            self.plt_fmt = '.png'     
        
        # main legend title
        if hasattr(self.cfg.plot, 'leg_title'):
            self.leg_title = self.cfg.plot.leg_title     
        else:
            self.leg_title = 'Neutron Energy Level:'     

        # sub-plot legend title 
        if hasattr(self.cfg.plot, 'sub_leg_title'):
            self.sub_leg_title = self.cfg.plot.sub_leg_title     
        else:
            self.sub_leg_title = self.sub_var     

        # y-axis label
        if hasattr(self.cfg.plot, 'y_text'):
            self.y_text = self.cfg.plot.y_text     
        elif self.scale_y == True:
            self.y_text = ('Relative Neutron Counts, '
                          'Scaled to Scenario Minimum [%]')
        else:
            self.y_text = 'Neutron Counts [-]'
                        
    def plot(self):
        '''
        Plotting function:
            
            Plot will be split into multiple subplots.
            For each x variable a plot will be produced.
        '''
        # make list of x variables, independant of entries
        if type(self.x) is list:
            x_list = self.x
        else:
            x_list = [self.x]
            
        # start loop for x values    
        for x in x_list:
            ## specify variables to use
            sub_var = self.sub_var
            ncts = self.cfg.ncts
            donut_size = self.cfg.donut_size
            
            # x-axis label
            if len(x_list) > 1:
                self.x_text = x
            elif hasattr(self.cfg.plot, 'x_text'):
                self.x_text = self.cfg.plot.x_text     
            else:
                self.x_text = x     
            
            # plot name (file name for saving)             
            if self.scale_y == True:
                _y = 'scaledNeutronCounts'
            else:
                _y = 'NeutronCounts'
            self.plt_name = (f'{self.sc_name}_{_y}_vs_{x}_'
                             f'{self.cfg.plot.file_date}')
    
            if (len(x_list) == 1 and
               hasattr(self.cfg.plot, 'plt_name')):
                   self.plt_name = self.cfg.plot.plt_name
            else:
                pass
                   
            ## number of energy level / color to use
            # Defaults to: All Energy Levels!
            if hasattr(self.cfg.plot, 'y'):
                if isinstance(self.cfg.plot.y, int):
                    selection = np.array([self.cfg.plot.y])
                else:
                    selection = np.array(list(map(int, self.cfg.plot.y)))
            else:
                selection = np.array([1, 2, 3, 4, 5])
                
            ## derive energy / color list to use
            energy_list = np.array(self.cfg.energyList)[selection-1].tolist() 
            color_list = np.array(self.cfg.colorList)[selection-1].tolist()
    
            ## dynamic plot: please check if all desired settings are made
            plt.ioff()
            fig, axs = plt.subplots(self.plot_layout[0], # number of rows
                                    self.plot_layout[1],  # number of cols
                                    sharex=True,
                                    sharey=True,
                                    figsize=self.fig_size)
            axs = axs.ravel() # important for variable subplot orientation
            fig.suptitle(self.suptitle)
    
            # let energy level loop start here
            for y, col in zip(energy_list, color_list):
                ## subset dataframe
                yerr = y+'_std' # (str) y value error: standard deviation
                df = ncts[[sub_var, x, y, yerr]]
                df_copy = df
                
                # split data frame by sub_var and put them into lists
                df_split = []
                for locals()[sub_var], df in df.groupby(sub_var):
                    str1 = sub_var
                    str2 = str(int(locals()[sub_var]))
                    new_name = str1 + str2
                    exec('{} = pd.DataFrame(df)'.format(new_name))
                    df.name = new_name
                    df_split.append(df)
    
    
                for i in range(len(df_split)):
                    axs[i].errorbar(x=df_split[i][x],
                                    y=df_split[i][y],
                                    yerr=df_split[i][yerr],
                                    ecolor=col,
                                    ls='none')
                    axs[i].scatter(x=df_split[i][x],
                                   y=df_split[i][y], 
                                   c=col)  
                    axs[i].legend(handles=[],
                                  loc="upper center",
                                  title=f"{self.sub_leg_title} "
                                        f"{int(df_split[i][sub_var][1])}\n"
                                        f"({donut_size._from[i]} - "
                                        f"{donut_size._to[i]}m)")
                    axs[i].grid(linestyle='--')
             
            # add legend and axes text    
            fig.legend(energy_list, title=self.leg_title, loc='center right')    
            fig.text(0.5, 0.04, self.x_text, ha='center')
            fig.text(0.04, 0.5, self.y_text, va='center', rotation='vertical')
    
            # save plot
            if self.save_plt == True:
                path_name_fmt = (self.plt_path + 
                                '/' + 
                                self.plt_name +
                                self.plt_fmt)
                plt.savefig(path_name_fmt)
                self.log.log ('\n'
                              'Plot saved:\n'
                              f'{path_name_fmt}')
            else:
                pass
    
            # show plot
            if self.show_plt == True:
                plt.show()
            else:
                pass
        
    def __call__(self):
        """main call of the class"""
        
        # add data to object
        self.cfg.add('ncts', self.read_ncts())
 
        # scale data
        self.cfg.add('ncts', self.scale_ncts())

        # extract distance zone radii
        self.cfg.add('donut_size', self.from_to_radii())
        
        # get and manipulate plot specification
        self.plot_specs()
        
        # create plot
        self.plot()
