# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:06:26 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import psutil
import subprocess
from subprocess import DEVNULL
import os
import time

# python package imports
from joblib import Parallel, delayed
import numpy as np

# yulia imports
from yulia.utils import Meteo
from yulia.utils import Utils
from yulia.utils import Config
from yulia.utils import Logfile
from yulia.generate import landscape_models
from yulia.operate import ValueRanges
from yulia import analyse
from yulia import plot


class Core(object):
    """Core class to operate URANOS generating geometry and configuration"""

    def __init__(self,
                 cfg_object=None,
                 log_object=None,
                 config_file_name=None):
        """
        Initialise class

        Args:
            cfg (object): Config object.
            log (object): Logfile object.

        Returns:
            None.

        """

        # load configuration
        if cfg_object is not None:
            # load existing object
            self.cfg = cfg_object
        else:
            
            # check existance of config_file_name and load cfg from file
            if config_file_name is not None:
                if not os.path.exists(config_file_name):
                    self.log = Logfile("")
                    self.log.log("Severe:\t'Config-file {}' not found.".format(config_file_name))
                else:
                    self.cfg = Config(config_file_name)
            else:
                # create log in execution folder and terminate program.
                self.log = Logfile("")
                self.log.log("Severe:\tNo config_file_name defined.")

        # assign log object or create new instance of Logfile
        if log_object is not None:
            self.log = log_object
        else:
            self.log = Logfile(self.cfg.operate.get("base_dir", ""),
                               self.cfg.operate.get("verbose", True))

        # meteo utils class
        self.met = Meteo()

        # general utils class
        self.utils = Utils(self.log)

        # get number of available physical cores
        _use_all_cores = self.cfg.operate.get("use_all_logical_cores", False)
        self.cfg.add("num_cores", psutil.cpu_count(logical=_use_all_cores))
        # by default: don't use all of them
        num_cores_idle = self.cfg.operate.get("num_cores_idle", 0)
        self.cfg.add("use_cores", self.cfg.num_cores - num_cores_idle)

    def initialise_scenario(self,
                            sc_name,
                            initialise_domain=None):
        """Initialise all landscape models and generate scenario"""

        # get option whether to initialise domain. default is True if generate active
        if initialise_domain is None:
            initialise_domain = self.cfg.operate.get("initialise_domain", False)
            if self.cfg.operate.get("generate", False) or self.cfg.operate.get("analyse", False):
                initialise_domain = True
        self.cfg.operate.add("initialise_domain", initialise_domain)

        # initialise landscape scenarios (geometry in URANOS wording)
        err_text = "Severe:\tUnknown Scenario: '{}'.".format(sc_name)
        cfg_s = getattr(self.cfg,
                        sc_name,
                        lambda x: self.log.log(err_text))
        err_text = "Severe:\tError initialising scenario '{}'.".format(sc_name)
        lm_class = getattr(landscape_models,
                           cfg_s.landscape_model,
                           lambda x: self.log.log(err_text))
        
        # check if domain is defined
        if not hasattr(self, "domain"):
            self.domain = None
        
        # check if location is being varied
        if "location" in self.cfg.operate.var_list:
            # reset domain
            self.domain = None
        
        # create new instance of landscape model class
        realisation = lm_class(self.cfg,
                               self.log,
                               sc_name,
                               self.cfg.operate.initialise_domain,
                               self.domain)
        
        # set domain
        if self.domain is None and hasattr(realisation, "domain"):
            self.domain = realisation.domain
            
        # set new instance of landscape model class
        setattr(self, sc_name, realisation)

    def generate_scenario_sublevel(self,
                                   base_cfg,
                                   sub_list,
                                   subdir_list,
                                   level):
        """Function to get the next sublevel in subscenario generation"""

        # get maximal sublevel from number of ranges
        max_level = self.cfg.n_ranges-1

        # abort if level exceeds number of ranges
        if level > max_level:
            self.log.log("Error:\tWrong usage of scenario_sublevel.")
        else:
            # get range name
            range_name = self.cfg.operate.subscenario_ranges[level]
            # get corresponding value_range definition
            range_cfg = getattr(self.ranges.cfg, range_name)

            # iterate over range
            for i, val in enumerate(range_cfg.values):
                # get corresponding name
                sub_name = range_cfg.name_list[i]
                subdir_list[level] = sub_name
                # max depth not yet reached
                if level < max_level:
                    # add sub-scenario name
                    sub_list[level] = sub_name + "_"
                    # add variable
                    base_cfg.add(range_cfg.variable, val)
                    # go to next level
                    base_cfg,
                    sub_list,
                    base_cfg, sub_list, subdir_list = self.generate_scenario_sublevel(base_cfg,
                                                                                      sub_list,
                                                                                      subdir_list,
                                                                                      level+1)

                # last level reached
                elif level == max_level:
                    # add sub-scenario name
                    sub_list[level] = sub_name
                    # add variable
                    base_cfg.add(range_cfg.variable, val)
                    # create name of sub_scenario
                    sc_name = base_cfg.base_name + "_"
                    for n in sub_list:
                        sc_name += n
                    # add sub_list for defining sub directory
                    base_cfg.add("sub_dir_list", subdir_list)
                    # set new scenario
                    setattr(self.cfg, sc_name, base_cfg.copy())
                    # add subscenario to scenario list
                    self.cfg.operate.sub_scenarios.append(sc_name)

            return base_cfg, sub_list, subdir_list

    def generate_subscenarios(self):
        """Generate sub-scenarios according to value-ranges"""
        if hasattr(self.cfg.operate, "subscenario_ranges"):
            # add empty sub-scenario list
            self.cfg.operate.add("sub_scenarios", list())
            for sc_name in self.cfg.operate.scenarios:
                # check if list
                sub_ranges = self.utils.check_if_list(
                    self.cfg.operate.subscenario_ranges)
                self.cfg.operate.add("subscenario_ranges", sub_ranges)

                # get list of sub-scenario variables to be modified
                self.ranges = ValueRanges(self.cfg, self.log)
                self.cfg.operate.add("var_list", list())
                for range_name in self.cfg.operate.subscenario_ranges:
                    # get corresponding value_range definition
                    range_cfg = getattr(self.ranges.cfg, range_name)
                    # add variable to var list
                    if range_cfg.variable in self.cfg.operate.var_list:
                        # search for the range with identical variable and
                        # append values
                        for _rn in self.cfg.operate.subscenario_ranges:
                            _r_cfg = getattr(self.ranges.cfg, _rn)
                            if _r_cfg.variable == range_cfg.variable:
                                _r_cfg.values = np.unique(
                                    _r_cfg.values + range_cfg.values)
                                _r_cfg.name_list = np.unique(
                                    _r_cfg.name_list + range_cfg.name_list)
                                break
                        # remove range from list
                        self.cfg.operate.subscenario_ranges.remove(range_name)
                    else:
                        self.cfg.operate.var_list.append(range_cfg.variable)

                # initialise base scenario
                self.initialise_scenario(sc_name, initialise_domain=False)

                # copy base-scenario cfg
                base_cfg = getattr(self, sc_name).cfg.copy()
                # add current name as base name
                base_cfg.add("base_name", sc_name)
                # reset station import
                base_cfg.add("has_station", False)

                # get number of ranges
                n_ranges = len(self.cfg.operate.subscenario_ranges)
                self.cfg.add("n_ranges", n_ranges)

                # get name of first range
                start_level = 0
                first_range = self.cfg.operate.subscenario_ranges[start_level]
                # get corresponding value_range definition
                range_cfg = getattr(self.ranges.cfg, first_range)

                # generate scenario sublevels
                for i, val in enumerate(range_cfg.values):
                    # add sub-list
                    sub_list = list()
                    sub_dir_list = list()
                    for s in range(n_ranges):
                        sub_list.append("_")
                        sub_dir_list.append("_")
                    # add name of current level
                    sub_dir_list[start_level] = range_cfg.name_list[i]
                    # set value for current level
                    base_cfg.add(range_cfg.variable, val)
                    # check if further sublevels are defined
                    if n_ranges > 1:
                        # add name of current level
                        sub_list[start_level] = range_cfg.name_list[i] + "_"
                        # add sub-levels
                        self.generate_scenario_sublevel(base_cfg,
                                                        sub_list,
                                                        sub_dir_list,
                                                        start_level+1)
                    else:
                        # add name of level
                        sub_list[start_level] = range_cfg.name_list[i]
                        # create name of sub_scenario
                        sc_name = base_cfg.base_name + "_"
                        for n in sub_list:
                            sc_name += n
                        # add sub_list for defining sub directory
                        base_cfg.add("sub_dir_list", sub_dir_list)
                        # set new scenario
                        setattr(self.cfg, sc_name, base_cfg.copy())
                        # add subscenario to scenario list
                        self.cfg.operate.sub_scenarios.append(sc_name)

            # replace scenario list with automatically generated subscenarios
            self.cfg.operate.add("scenarios", self.cfg.operate.sub_scenarios)

    def generate_scenario(self, sc_name):
        """Generate configuration files for scenario"""

        # get scenario object
        sc_obj = getattr(self, sc_name)

        # write config file
        sc_obj.generate_uranos_cfg_file()

        # write geometry file
        sc_obj.generate_uranos_geometry()

    def start_model(self, sc_name):
        """Start URANOS simulation for selected scenario"""

        # get scenario object
        sc_obj = getattr(self, sc_name)

        # rewrite URANOS cfg if scenario generation was not performed
        if not self.cfg.operate.get("generate", False):
            # write config file
            sc_obj.generate_uranos_cfg_file()

        # log model start time
        self.log.log("Info:\t" + sc_name + ' started')
    
        # check if console is in verbose or silent mode
        write_stdout = self.cfg.operate.get("console_output", True)

        # generate str for subprocess call
        u_exe = os.path.normpath(os.path.normpath(self.cfg.operate.uranos_exe))
        cfg_file = os.path.normpath(os.path.normpath(sc_obj.cfg.path_config))
        call_str = "{} noGUI {}".format(u_exe, cfg_file)

        # start model with stdout
        if write_stdout:

            p = subprocess.Popen(call_str,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT,
                                 universal_newlines=True)

            for line in p.stdout:
                # process line here
                print(line, end='')

        # start model without std output (DEVNULL sink)
        else:
            p = subprocess.Popen(call_str,
                                 universal_newlines=True,
                                 stdout=DEVNULL)
            # wait until process is finished
            while p.poll() is None:
                time.sleep(0.5)

        # convert URANOS outputs to Geotiff
        sc_obj._convert_uranos_results_to_geotiff()
        
        # log model finishing time
        self.log.log("Info:\t" + sc_name + ' finished')
        
        return

    def analyse_results(self):
        """Analyse URANOS results for selected scenario"""
        
        # get corresponding analyse class name
        ac_name = self.cfg.analyse.get("analyse_class", "General")
        
        # create instance of analyse class
        self.analyse = getattr(analyse, ac_name)(self.log, 
                                                 self.cfg,
                                                 self.cfg.analyse,
                                                 self.cfg.operate.scenarios)
        
        # import results for each scenario
        for sc_name in self.cfg.operate.scenarios:
            # get scenario object
            sc_obj = getattr(self, sc_name)
            
            # import density maps
            self.analyse.import_density_maps(sc_obj)
        
        # analyse results
        self.analyse()
        
        # export results
        self.analyse.export_results()

    def create_plots(self):
        """Plot results for selected scenario"""
       
        # get corresponding plot class name
        pc_name = self.cfg.plot.get("plot_class", "General")
        
        # create instance of plot class
        self.plot = getattr(plot, pc_name)(self.cfg,
                                           self.log)
        
        self.plot()

    def __call__(self):
        """Main call of yulia"""

        # get list of landscape elements and check if it's a list
        sc_list = self.utils.check_if_list(self.cfg.operate.scenarios)
        self.cfg.operate.add("scenarios", sc_list)

        # generate sub-scenarios if needed
        self.generate_subscenarios()
        
        # initialise scenario objects
        for sc_name in self.cfg.operate.scenarios:
            self.initialise_scenario(sc_name)

        # generate configuration if activated
        if self.cfg.operate.get("generate", False):
            self.log.log("Info:\tGenerating URANOS geometries.")
            if self.cfg.operate.get("parallel", True):
                # parallel version
                Parallel(n_jobs=self.cfg.use_cores)(
                    delayed(self.generate_scenario
                            )(s) for s in self.cfg.operate.scenarios)
            else:
                for sc_name in self.cfg.operate.scenarios:
                    self.generate_scenario(sc_name)

        # start URANOS if activated
        if self.cfg.operate.get("operate", False):
            self.log.log("Info:\tStarting URANOS simulations.")
            if self.cfg.operate.get("parallel", True):
                # parallel version
                Parallel(n_jobs=self.cfg.use_cores)(
                    delayed(self.start_model
                            )(s) for s in self.cfg.operate.scenarios)
            else:
                # non-parallel
                for sc_name in self.cfg.operate.scenarios:
                    self.start_model(sc_name)

        # analyse results if activated
        if self.cfg.operate.get("analyse", False):
            self.log.log("Info:\tAnalysing results.")
            self.analyse_results()

        # create plots if activated
        if self.cfg.operate.get("plot", False):
            self.log.log("Info:\tGenerating Plots.")
            self.create_plots()
