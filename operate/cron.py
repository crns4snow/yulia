# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 15:42:30 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import datetime
import pytz

# python package imports
import numpy as np
import matplotlib.dates


class Cron():
    def __init__(self, cfg, log):
        self.cfg = cfg
        self.log = log
        
    def arange(self, start_date, end_date, freq):
        """
        Arange datetime array

        Args:
            start_date (datetime): First date.
            end_date (datetime): Last date.
            freq (str): String in pandas freq-style.

        Returns:
            None.

        """
        # convert freq
        freq_dict = self.convert_freq(freq)
        # get hours
        hours = freq_dict["hours"]
        
        # generate date range
        date_range = matplotlib.dates.num2date(matplotlib.dates.drange(
            start_date, 
            end_date,
            datetime.timedelta(hours=hours)))
        
        # set tzinfo to value defined in cfg or to None
        date_range = np.array([t.replace(tzinfo=self.get_timezone()) for t in date_range])
        # check if last time-step is in date range already. if not append it
        if end_date not in date_range:
            date_range = np.append(date_range, end_date)
            
        return date_range
    
    def convert_freq(self, freq):
        """Convert freq string to dictionary with information"""
        out = dict()
        
        # check direction 
        if freq[:1] == "-":
            # backward direction
            out["+"] = False
            out["-"] = True
            # remove minus
            freq = freq[1:]
        else:
            # forward direction
            out["+"] = True
            out["-"] = False
            
        # remove '+' if present
        if freq[:1] == "+":
            # remove plus
            freq = freq[1:]
        
        # check for seconds
        if "S" in freq:
            if len(freq) > 1:
                sec = float(freq[:-1])
            else:
                sec = 1
        else:
            sec = 0
                
        # check for minutes (written as "T")
        if "T" in freq:
            if len(freq) > 1:
                minutes = float(freq[:-1])
            else:
                minutes = 1
        else:
            minutes = 0
        
        # check for minutes (written as "min")
        if "min" in freq:
            if len(freq) > 3:
                minutes = float(freq[:-3])
            else:
                minutes = 1
        else:
            minutes = 0
    
        # check for hours
        if "H" in freq:
            if len(freq) > 1:
                hours = float(freq[:-1])
            else:
                hours = 1
        else:
            hours = 0
            
        # check for days
        if "D" in freq:
            if len(freq) > 1:
                days = float(freq[:-1])
            else:
                days = 1
        else:
            days = 0
            
        # assign values
        out["seconds"] = sec
        out["minutes"] = minutes + sec/60
        out["hours"] = hours + minutes/60 + sec/3600
        if days > 0:
            out["hours"] = 24 * days
    
    def get_current_timestep(self):
        """get the current time step"""
        return datetime.datetime.now(self.get_timezone())
    
    def get_timezone(self):
        """Get timezone definition"""
        # get timezone from config if defined
        if "timezone" in self.cfg:
            tz = pytz.timezone(self.cfg.timezone)
        else:
            tz = None
        return tz