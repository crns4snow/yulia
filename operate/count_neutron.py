# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 11:48:33 2022

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan, Jan Schmieder.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# yulia imports
from yulia.operate import Core
from yulia.analyse import NeutronCounts


class CountNeutrons(Core):
    
    def analyse_results(self):
        """Analyse URANOS results for selected scenario"""
        
        # create instance of analyse class
        self.analyse = NeutronCounts(self.log, 
                                     self.cfg,
                                     self.cfg.analyse,
                                     self.cfg.operate.scenarios)
        
        # import results for each scenario
        for sc_name in self.cfg.operate.scenarios:
            # get scenario object
            sc_obj = getattr(self, sc_name)
            
            # get actual number of sim neutrons for each (sub-)scenario
            self.analyse.get_sim_nt(sc_obj)
    
    def __call__(self):
        """Main call of yulia"""

        # get list of landscape elements and check if it's a list
        sc_list = self.utils.check_if_list(self.cfg.operate.scenarios)
        self.cfg.operate.add("scenarios", sc_list)

        # generate sub-scenarios if needed
        self.generate_subscenarios()
        
        # initialise scenario objects
        for sc_name in self.cfg.operate.scenarios:
            self.initialise_scenario(sc_name)

        # analyse results if activated
        self.analyse_results()