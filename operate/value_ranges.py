# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 14:06:18 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
from datetime import datetime

# python package imports
import numpy as np

# yulia imports
from yulia.operate import Cron
from yulia.utils import Utils


class ValueRanges():
    def __init__(self, cfg_full, log):
        self.cfg_full = cfg_full
        self.log = log
        self.cfg = cfg_full.operate.ranges
        # utils class
        self.utils = Utils(self.log)
        # class for datetime ranges
        self.cron = Cron(self.cfg_full.operate, self.log)
        # initialise ranges
        self._initialise_ranges()
        
    def _initialise_ranges(self):
        """Initialise defined value ranges"""
        # iterate over defined keys
        for i, range_name in enumerate(self.cfg):
            
            # create empty string list
            str_list = list()
            
            # get val_range
            val_range = getattr(self.cfg, range_name)
            
            # get variable
            variable = val_range.variable
            
            # check for existing value_list
            if hasattr(val_range, "value_list"):
                values = self.utils.check_if_list(val_range.value_list)
                # check if values are numeric
                if type(values[0]) == str:
                    if values[0].isnumeric():
                        values =  [float(values[i]) for i in range(len(values))]
                val_range.add("values", values)
                
                # check if values are dates or numeric
                if "date" in variable or type(values[0]) == datetime:
                    val_range.add("dates", True)
                    if type(values[0]) == str:
                        # try to guess the dateformat used
                        if "." in values[0]:
                            if len(values[0]) <= 10:
                                dtformat = "%d.%m.%Y"
                            else:
                                dtformat = "%d.%m.%Y %H:%M"
                        elif "-" in values[0]:
                            if len(values[0]) <= 10:
                                dtformat = "%d-%m-%Y"
                            else:
                                dtformat = "%d-%m-%Y %H:%M"
                        
                        try:
                            values = [datetime.strptime(val, dtformat) for val in values]
                            val_range.add("values", values)
                        except:
                            err_text = "Error:\tCould not identify datetime "
                            err_text += "format for value range '{}'.".format(range_name)
                            err_text += "Use '%d.%m.%Y %H:%M' or %d-%m-%Y %H:%M"
                            self.log.log(err_text)
                else:
                    val_range.add("dates", False)
                
            # check for range defined by min, max and step
            elif hasattr(val_range, "min") and hasattr(val_range, "max") and hasattr(val_range, "step"):
                # get min value
                min_val = getattr(val_range, "min")
                max_val = getattr(val_range, "max") + 1
                step = getattr(val_range, "step")
                
                # check if values are dates or numeric
                if "date" in variable or type(min_val) == datetime:
                    val_range.add("dates", True)
                    if step != "from_nest":
                        values = self.cron.arange(min_val, max_val, step)
                    else:
                        # TODO: get datelist from NEST dataset
                        self.log.log("Error:\TGenerating date list from NEST not yet implemented.")
                else:
                    val_range.add("dates", False)
                
                # create array from min to max (incl. max) with step = step
                values = np.arange(min_val, max_val, step)
                setattr(val_range, "values", values)
            
            # create corresponding list of strings for scenario naming
            for val in values:
                if val_range.dates:
                    # convert datetime object to string
                    s = val.strftime("%Y%m%d-%H%M")
                else:
                    # remove unused decimals
                    if type(val) != str and (int(val) - val) == 0:
                        val = int(val)
                    # convert to string and replace unsupported values
                    s = str(val).replace(".","-")
                # append to list
                str_list.append(f"{val_range.variable}_{s}")
                
            # add string list to value range definition
            val_range.add("name_list", str_list)