# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 20:14:28 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan, Till Francke.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import os

# python package imports
import numpy as np

# yulia imports
from yulia.generate.landscape_models import Basic
from yulia.generate import layer_types
# import sys
import re  # for searching with regexp
import pandas as pd
from sys import maxsize

from yulia.utils import ConfigDict

# grid names expected in the ncdf file (key) and where to put them to in Yulia (value)
# these are the grids that are searched for in the netcdfs and which are finally merged
relevant_grids = {"material_id": "material",
                      "porosity": "porosity",
                      "density": "density", #legacy alias for "dens"
                      "dens": "density",
                      "sm":"sm"}
class Topo3DExternalNcdf(Basic):
    """Class for generating 3D landscapes"""
    dbg_maxdim = maxsize #debug setting: limit maximum x-y- extent of matrices to this number. Set to "maxsize" to disable
    #dbg_maxdim = 500 #debug setting: limit maximum x-y- extent of matrices to this number. Set to "maxsize" to disable

    def shorten_array(self, arr, max_elements):
        """
        Format an array for printing, reducing the display to "max_elements" at its beginning and its end

        Parameters
        ----------
        arr : numpy-array
        array to format
        max_elements: integer
        total number of elements to display


        Returns
        -------
        fstring: strng
          formatted string for (potentially) shortended output of array

        Remarks
        -------

        Example
        -------
        shorten_array(arr=np.arange(start=1, stop=200), max_elements=10)

        """
        import numpy as np

        if (len(arr) <= max_elements):
            fstring = format(arr)
        else:
            f1 = format(arr[0: int(np.ceil (max_elements / 2))])
            f2 = format(arr[  -int(np.floor(max_elements / 2)):])
            fstring = f1[:-1] + " ... " + f2[1:]
        return(fstring)

    def _warn_superfluous(self, accepted_attributes):
        passed_attributes = list(self.cfg.__dict__.keys())  # get names of attributes in config xls
        ignored_attrs = np.setdiff1d(passed_attributes, accepted_attributes)
        if len(ignored_attrs) > 0:
            self.log.log(
                "WARNING: some attributes specified in '{}' will be ignored: {}".format(self.cfg.name, ignored_attrs))

    def _prepare_material_codes(self):  # expand material codes given in ranges
        material_codes = self.cfg_full["material"][
            'codes_distributed']  # material codes and default densities as used by URANOS

        material_codes = pd.DataFrame(material_codes).T  # convert material codes into dataframe

        r = re.compile("\\.")
        material_codes["code"] = material_codes.code.astype("str")  # convert column into string

        range_codes = list(filter(r.search, material_codes["code"]))  # find codes given in ranges (e.g. "1...30")

        for range_code in range_codes:
            cfrom, cto = re.split("\\.+", range_code)
            cfrom = int(cfrom)
            cto = int(cto)

            # create data frame with expanded codes
            cur_row = material_codes.code == range_code  # get row of aggregated record
            sequence = np.setdiff1d(np.arange(cfrom, cto),
                                    100)  # define sequence, skip the "100", as it has a special meaning
            smkeydf = pd.DataFrame({"code": sequence})
            smkeydf["name"] = [material_codes.name[cur_row][0] + "%d" % i for i in sequence]
            smkeydf["density"] = material_codes.density[cur_row][0]
            smkeydf["unit"] = material_codes.unit[cur_row][0]
            smkeydf["description"] = material_codes.description[cur_row][0]

            material_codes = pd.concat([material_codes[~cur_row], smkeydf])

        material_codes.code = material_codes.code.astype("int")  # convert column into string

        material_codes = material_codes.set_index("code")
        material_codes.description = material_codes.description.astype("str")  # convert column into string
        # ".join(material_codes.description["1"])
        # material_codes.description["1"]+"c"

        # find groups of special codes (used later on)
        special_codes = dict()
        special_codes["soil"] = [index for index, row in material_codes.iterrows() if
                                 re.search("^ *[Ss]oil", "".join(row["description"]))]
        special_codes["ground"] = [index for index, row in material_codes.iterrows() if
                                   re.search("^ *[Ss]oil|^ *[Ss]tones", "".join(row["description"]))]
        special_codes["air"] = [index for index, row in material_codes.iterrows() if
                                re.search("^['[ ]*(Air|Detector)", "".join(row["description"]))]
        special_codes["soil_sm"] = [index for index, row in material_codes.iterrows() if
                                    re.search("^soil_sm_distributed",
                                              "".join(row["name"]))]  # get codes of soil with variable soil moisture

        # correct density and unit of "air"
        r = re.compile("^['[ ]*(Air|Detector)")
        # air_desc = list(filter(r.search, material_codes.description)) #find codes given in ranges (e.g. "1...30")
        # air_codes = material_codes.index[np.isin(material_codes.description, air_desc)].astype("int")
        # air_codes = np.unique(np.concatenate(([0], air_codes))) #add the 0, as it is used internally for "nothing"
        # air_codes = air_codes.astype("int")

        material_codes.loc[special_codes[
            "air"], "density"] = 1.2  # this is the correct density of air in [kg/m³], instead of its pressure
        material_codes.loc[special_codes["air"], "unit"] = "kg/m3"

        # this still needs to be done, without messing too much with URANOS' densities
        ##"with Air" materials need to be corrected in their density to include the air in total density
        ##get codes of material ADDITIONALLY containing air ("with Air")
        # with_air_codes = [i for i, item in descri.items() if (i not in air_codes) and re.search("with Air", "".join(item))]
        # for code in with_air_codes:
        #    lookup[code] = lookup[code] + lookup[air_codes[-1]]

        self.cfg_full["material"]['codes_distributed_exp'] = material_codes  # store expanded lists for later access
        self.cfg_full["material"]['special_codes'] = special_codes  # store special_codes for later access

    def _initialise_landscape(self):
        """Initialise all landscape elements"""

        self._warn_superfluous(accepted_attributes=["landscape_model",  # topncdf-specific attributes
                                                    "cutoff_rigidity",
                                                    "air_humidity",
                                                    "source_layer",
                                                    "detector_layer",
                                                    "landscape",
                                                    "export_geotiffs",
                                                    "export_to_uranos",
                                                    "domain_definition",
                                                    "location_definition",
                                                    "detector_height_above_ground",
                                                    "realization_ncdf_path",
                                                    "ncdf_action",
                                                    "plot_layer_geometry",
                                                    "skip_obsolete_exports",
                                                    "thickness_dummy_detector_layer",
                                                    "pad_dummy_detector_layer",
                                                    'domain_size',  # attributes inherited or set by Yulia
                                                    'has_dem',
                                                    'has_geotrans',
                                                    'has_station',
                                                    'layer_func_list',
                                                    'layer_list',
                                                    'name',
                                                    'path_config',
                                                    'path_geometry',
                                                    'scenario_dir',
                                                    'atm_pressure_correction',
                                                    'cfg_version',
                                                    'crop_z_min'
                                                    ])
        self._prepare_material_codes()  # expand material codes given in ranges

        # set atm_depth if air_pressure is defined
        if hasattr(self.cfg, "air_pressure"):
            atm_depth = self.met.calc_atm_depth(self.cfg.air_pressure)
            self.cfg.add("atm_depth", atm_depth)

        self.cfg.soil_moisture = 2  # dummy value needed in class basic, not actually used

        # get list of landscape elements and check if it's a list
        le_list = self.utils.check_if_list(self.cfg.landscape)
        self.cfg.add("landscape", np.array(le_list))

        # check soil moisture value against soil porosity
        # sm = self.cfg.get("soil_moisture", 25)
        # if sm > self.cfg.get("soil_porosity", 0.5)*100:
        #    sm = 100 - self.cfg.get("soil_porosity", 0.5)*100
        # self.cfg.add("soil_moisture", sm)

        # create layer objects
        # skipped

        # add subdived layers (if activated)
        self._add_subdived_layers()

        # check if source, detector and ground layers exist in landscape
        self._set_layer_func_list()

        # check if source, detector and ground layers exist in landscape
        self.check_layer_func_list()

        # derive URANOS layer geometry
        # skipped #for class "Topo3D_ncdf", this information is already there

        # save that initialisation was completed
        self.cfg.add("landscape_initialised", True)

    def _add_subdived_layers(self):
        # """add additional subdivided layers to landscape"""
        # get list of landscape elements and check if it's a list
        # add_layers = getattr(self.cfg, "landscape_add_layers",
        #                          lambda: None)
        # add_layers == None
        # le_list = self.utils.check_if_list(self.cfg.landscape_add_layers)
        # self.cfg.add("landscape_add_layers", np.array(le_list))

        # create layer objects #!retrieves and sets all compartment-related attributes
        # self._create_layer_objects()

        # prepare elevation zones #!create DEM zones needed for assembling grids later in _prepare_landscape, _fill_landscape, _calculate_atm_density
        # self._prepare_dem_zones() #brauche ich wohl nicht


        # prepare elevation zones list and 3D landscape arrays #!prepare geometries
        # self._prepare_landscape() #?brauche ich das?

        # fill landscape layers #!actual combination of layers
        # self._fill_landscape()

        if getattr(self.cfg, "ncdf_action", "") not in ["load", "merge"]:
            self.log.log("SEVERE: 'ncdf_action' must be one of ['merge', 'load']")

        if self.cfg.ncdf_action == "load":
            # load existing NetCDFfile to fill landscape (instead of generating one)
            from os.path import exists  # for checking file existence
            ncdf_file = getattr(self.cfg, "realization_ncdf_path", "")
            if exists(ncdf_file):
                self.log.log("... Loading from existing composite NetCDF-file '{}'".format(ncdf_file))
                self._load_composite_ncdf()
            else:
                self.log.log("WARNING:\t'netCDF-file {}' not found for loading, trying to merge anew...".format(ncdf_file))
                self.cfg.ncdf_action = "merge"

        if self.cfg.ncdf_action == "merge":
            self.log.log("...  Assembling multiple NetCDF files...{}".format(""))
            self._geometry_from_ncfds()  # generate common layer geometry from specified ncdfs
            self._merge_ncdfs()  # combine compartments to vertical geometry of URANOS-layers

        #np.min(self.sm[23, :, :]) #debug
        #self.sm_bak=self.sm
        #self.sm=self.sm_bak
        self.log.log("...  Converting properties to URANOS-codes...{}".format(""))
        self._convert_density_porosity_to_codes()  # convert density [g/cm³] and porosity [-] matrices into URANOS codes for output

        self.log.log("...  Adjusting and exporting layers...{}".format(""))
        self._adjust_layers()  # re-create / correct the "layer" attributes according to the imported matrices

        # add layers to landscape  #passiert hier schon in adjust_layers
        # self._add_to_landscape()
    def _convert_density_porosity_to_codes(
            self):  # convert density [g/cm³] and porosity [-] matrices into URANOS codes for output

        # check consistence
        dens_specified = self.density != 0
        if np.any(self.density[dens_specified] <= self.sm[dens_specified]/100):
            self.log.log("WARNING: Specified soil moisture is higher than specified total density for some cells. Corrected density accordingly.")
            self.density[dens_specified] = np.maximum(self.density[dens_specified], self.sm[dens_specified]/100)

        # compute density multiplicators from ratio of parameterized to specified density
        density_uncorrected = self._materialcodes2density(material=self.material,
                                                            porosity=0)  # compute density represented by the material codes  (excluding porosity and soil moisture)

        # scaling via "porosity": down-scale lighter/low-density voxels using porosity (instead of density factor), as the latter also scales sm and thus restricts its range
        self.porosity = 1 - (self.density - self.sm/100) / density_uncorrected # URANOS porosity required to achieve *specified* total density [-]
        self.porosity[self.density == 0] = 0  # don't change porosity, where no density (=0) was specified, as it remains standard value
        self.porosity[self.sm  == 0] = 0      # don't change density, where no soil moisture is specified - we rather scale it using the density factor later
        self.porosity[self.porosity < 0] = 0 #voxel with density higher than standard would need negative porosity. Instead, we'll correct their density in a later step.
        self.porosity = np.round(self.porosity, decimals=3)  # avoid some numerical noise

        # scaling via "density multiplier": needed to correct voxel with higher-than normal density, and used for all voxels without soil moisture
        density_uncorrected = self._materialcodes2density(material=self.material,
                                                          porosity=self.porosity)  # compute density represented by the material codes and porosity (excluding soil moisture)
        # density multiplier required to achieve *specified* total density
        density_factor = (self.density - self.sm/100) / density_uncorrected
        density_factor[self.density == 0] = 1  # don't change density, where no density (=0) was specified, as it remains standard value
        density_factor = np.round(density_factor, decimals=3)  # avoid some numerical noise

        # exclude special cases "air", "detector", "soil", which should not be altered
        #special_codes = self.cfg_full["material"]['special_codes']
        #materials_not_to_change = special_codes["soil"] #+ special_codes["air"]
        #density_factor[np.isin(self.material, materials_not_to_change)] = 1

        #impose soil moisture: this changes the material codes of the soil voxels
        soil_cells = np.isin(self.material, self.cfg_full["material"]['special_codes']["soil"])

        theta_adjusted = self.sm[soil_cells] / density_factor[soil_cells]  #[Vol. %] "reverse" / compensate the effect of any density_factor != 1, so we effectively stay at the specified soil moisture [%]
        n_theta_excess = np.sum(theta_adjusted>85) #check if any cells need to have a soil moisture specified higher than possible in URANOS
        if (n_theta_excess > 0):
            self.log.log("WARNING: {:2.1f} % of soil cells have a low density and high soil moisture, which cannot be represented in URANOS.".format(n_theta_excess/len(theta_adjusted)*100))

        self.material[soil_cells] = self._soilmoisture2URANOScode(theta_adjusted)

        # prepare output for using Yulia routines
        # convert multiplier to integer codes according to URANOS (to adhere to standards in Yulia)
        setattr(self, "density", self._density_factor2URANOScode(density_factor))  # set density factor
        self.porosity = np.round(self.porosity * 100, decimals=0).astype(int) # convert [-] to % (to adhere to standards in Yulia)

    def _materialcodes2density(self, material, porosity):
        # compute dry bulk density represented by the *voxel* material codes,  (i.e. including porosity, excluding soil moisture) 
        # NOTE: material and porosity are passed via arguments to allow using this function also on matrices not yet assigned to these attributes of self
        # returns density matrix in [g/cm³]

        # material_codes = self.cfg_full["material"]['codes_distributed'] #material codes and default densities as used by URANOS

        #        #look up default densities used by URANOS [kg/m³]
        #        lookup = {val["code"]:val["density"]     for mat, val in material_codes.items()}
        #        descri = {val["code"]:val["description"] for mat, val in material_codes.items()}
        #
        #        #expand codes which are given as ranges (e.g. soil)
        #        keys =  np.array(list(lookup.keys()))
        #        r = re.compile("\\.")
        #        range_codes = list(filter(r.search, keys)) #find codes given in ranges (e.g. "1...30")
        #        for range_code in range_codes:
        #            cfrom,cto =re.split("\\.+", range_code)
        #            cfrom= int(cfrom)
        #            cto  = int(cto)
        #            for code in range(cfrom, cto+1):
        #                lookup[code] = lookup[range_code]
        #                descri[code] = descri[range_code]
        #            lookup.pop(range_code) #remove range entry
        #            descri.pop(range_code) #remove range entry

        material_codes = self.cfg_full["material"]['codes_distributed_exp']

        # catch workaround for detector voxels specified with a code > 500
        detector_voxels = material > 500
        material[detector_voxels] = material[detector_voxels] - 500

        # check for unspecified material codes
        codes_in_grid = np.unique(material)  # unique codes used in the current grid

        missing = np.setdiff1d(codes_in_grid, material_codes.index)
        if len(missing) > 0:
            self.log.log(
                "SEVERE: Material codes {} used but not specified in sheet 'material_codes_distributed' of config-XLS.".format(
                    missing))

        # compute the *would-be* density as interpreted by URANOS solely from the material codes
        #import numpy as np #test
        #material = np.zeros((1000, 1000, 1006), dtype=np.float32)

        density_uncorrected = np.zeros(shape=material.shape, dtype=np.float32)
        # for k, v in lookup.items():
        #    density_uncorrected[material==k] = v
        for code in codes_in_grid:
            density_uncorrected[material == code] = material_codes.loc[code, "density"]

        density_uncorrected = density_uncorrected * 1e-3  # convert [kg/m3] to [g/cm3]

        # consider porosity
        #rho_air = material_codes.loc[material_codes.name == "air", "density"].values[0]
        rho_air = 0 #pore space in URANOS is filled with vacuum
        density_uncorrected = (density_uncorrected * (1 - porosity)) + (rho_air * porosity)
        
        # consider  soil moisture (if any)
        # we only want dry bulk density here, so the next lines are commented out
        # density_uncorrected = (density_uncorrected * (1 - porosity)) + (
        #         rho_air * (porosity)) + (
        #                           self._URANOScode2soilmoisture(
        #                               material))  # add soilmoisture (where appropriate) to total density

        # restore workaround for detector voxels specified with a code > 500
        material[detector_voxels] = material[detector_voxels] + 500

        return (density_uncorrected)

    def _density_factor2URANOScode(self, density_factor):
        # convert density factor (multiplier) to its representation in URANOS style (8 bit integer))
        mult_less_2 = density_factor <= 2
        density_factor[mult_less_2] *= 100
        density_factor[~ mult_less_2] += 200
        return (np.uint8(np.round(density_factor)))  # convert to byte

    def _URANOScode2soilmoisture(self, uranoscode):
        # convert URANOS material code to volumetric soil moisture  [-]
        sm = np.zeros_like(uranoscode, dtype=np.float32)

        special_codes = self.cfg_full["material"]['special_codes']

        soil_sm_ix = np.isin(uranoscode,
                             special_codes["soil_sm"])  # get index to IDs describing soil with variable soil moisture

        sm[soil_sm_ix] = uranoscode[soil_sm_ix] / 2 / 100
        return (sm)
    def _soilmoisture2URANOScode(self, sm):
        # convert volumetric soil moisture [-] to URANOS material code
        special_codes = self.cfg_full["material"]['special_codes']["soil_sm"]

        uranoscode = np.round(sm * 2 ).astype(int)
        #uranoscode = np.round(sm * 2 * 100).astype(int) #before fix

        uranoscode[np.logical_and(sm>=0.4975, sm<=0.50)  ] = 99
        uranoscode[np.logical_and(sm> 0.5,    sm<=0.5025)] = 101 #replace code 100, as it has a special meaning

        if (np.any(uranoscode < 2)):
            uranoscode[uranoscode<2] = 2
            self.log.log("WARNING: Some specified soil moisture values are lower than 0.01 and are set to 0.01 for URANOS.")

        if (np.any(uranoscode > 170)):
            uranoscode[uranoscode>170] = 170
            self.log.log("WARNING: Some specified soil moisture values are higher than 0.85 and are set to 0.85 for URANOS.")
        return (uranoscode)

    def _geometry_from_ncfds(self):
        """
        - derive layer geometry of vertical layers from set of
         compartments with different vertical resolutions stored as netCDFs
        - visualize vertical layer geometry
        """
        print("Finding common layer geometry ...")
        # %% settings
        from os.path import exists  # for checking file existence
        import netCDF4  # for reading netcdf
        import numpy as np
        #        import sys


        plot_layer_geometry = getattr(self.cfg, "plot_layer_geometry", lambda: False)
        compartments = getattr(self.cfg, "landscape")

        self._get_layer_specs(compartments) #check and get paths and vertical resolution to compartments
        vert_res = self.cfg.vert_res
        compartment_source = self.cfg.compartment_source

        # # optional reduction of vertical resolution (i.e. aggregation)
        # vert_res = {
        #     # surface
        #     'relief': 0,
        #     'atmosphere': 0,
        #     'vegetation': 0,
        #     'snow': 0,
        #     # subsurface,      0,
        #     'SM_distribution': 0,
        #     'groundwater': 0,
        #     'soil_chemistry': 0,
        #     'soil_physics': 0,
        #     # 'vegetation'    :0
        # }

        # dummy / default boundaries, if no netCDF is found
        compartment_boundaries_default = {
            # surface
            'relief': np.array([0, 5, 10]),
            # 'relief'         :np.arange(0, 20, 1),
            'atmosphere': np.array([0, 1.5, 2, 20]),
            'vegetation': np.array([-1, 0, 0.05, 1, 10]),
            'snow': np.array([0, 0.3]),

            # subsurface,     :np.array([
            'SM_distribution': np.array([0, -0.05, -0.1, -0.2]),
            'groundwater': np.array([0, -1, -2]),
            'soil_chemistry': np.array([0, -0.05, -0.1, -0.2]),
            'soil_physics': np.array([0, -0.05, -0.1, -0.2]),
            'default': np.array([0, 1])
            # default for other compartments
            # 'vegetation'    :np.array([])
        }

        compartments = np.array(list(compartment_source.keys()))
        # compartment_boundaries = {key:val for key,val in compartment_boundaries.items() if key in compartments}
        compartment_boundaries = {}
        for compartment in compartments:
            if compartment in compartment_boundaries_default.keys():
                compartment_boundaries[compartment] = compartment_boundaries_default[compartment]
            else:
                compartment_boundaries[compartment] = compartment_boundaries_default['default']

                # %% get boundaries from netcdf-files, if present
        x_ = y_ = None  # for checking identical extents
        for compartment, bounds in compartment_boundaries.items(): #read compartments from netCDF, check and extract boundaries
            print(compartment, ":")
            filename = compartment_source[compartment]
            if not exists(filename):
                self.log.log(
                    "SEVERE: {} does not exist, enable ('use default' in source code to override): {}".format(filename,
                                                                                                              bounds))
                # self.log.log(" {} does not exist, using default: {}".format(filename, bounds))
                continue
            nc_handle = netCDF4.Dataset(filename)

            for var in ['x', 'y', 'z', 'zmin']:
             if not var in nc_handle.variables.keys():
                    self.log.log("SEVERE: variable '{}' not found in '{}', terminated.".format(var, filename))

            coords = {}  # for collecting x, y, and z dimension coords
            coords['x_'] = nc_handle.variables['x'][:]  # x-coords of grid cells
            nx = len(coords['x_'])
            coords['y_'] = nc_handle.variables['y'][:]  # y-coords of grid cells
            ny = len(coords['y_'])
            coords['z_'] = nc_handle.variables['z'][:]  # upper boundaries of layers
            nz = len(coords['z_'])
            zmin = nc_handle.variables['zmin'][:]  # read lower boundary of lowest layer

            do_flip = {}  # check which dimensions need to be flipped to match Yulia standards
            do_flip["x"] = coords['x_'][0] > coords['x_'][-1]
            do_flip["y"] = coords['y_'][0] < coords['y_'][-1]  # we want y-axis to point northwards
            do_flip["z"] = coords['z_'][0] < coords['z_'][-1]  # we want the higher layers first

            for idim in do_flip.keys():
                if do_flip[idim]:
                    coords[idim + "_"] = np.flip(coords[idim + "_"], axis=0)

            if x_ is None:  # first lap in loop
                x_ = coords['x_'][0:self.dbg_maxdim] #debug option: reduce to subset for testing
                y_ = coords['y_'][0:self.dbg_maxdim]
            else:
                if not (np.array_equal(x_, coords['x_'][0:self.dbg_maxdim]) and
                        np.array_equal(y_, coords['y_'][0:self.dbg_maxdim])):
                    # x != nc_handle.variables['x'][:] or y != nc_handle.variables['y'][:]:
                    self.log.log("SEVERE: Dimension mismatch of '{}' (x: {}:, y:{}) with previous grids (x: {}:, y:{}), terminated.".format(filename,
                                                                                                                                      x_                                         [[0, -1]],
                                                                                                                                      y_                                         [[0, -1]],
                                                                                                                                      nc_handle.variables['x'][0:self.dbg_maxdim][[0, -1]],
                                                                                                                                      nc_handle.variables['y'][0:self.dbg_maxdim][[0, -1]],
                                                                                                                                ))
            tt = np.hstack([nc_handle.variables['z'][:], nc_handle.variables['zmin'][:]])
            tt = np.sort(tt)
            tt = np.unique(tt)
            tt = np.round(tt, decimals=3) #avoid numerical rounding effects
            compartment_boundaries[compartment] = tt
            nc_handle.close()

            print(" {} ({})".format(self.shorten_array(compartment_boundaries[compartment], max_elements= 20), len(compartment_boundaries[compartment])))

        # %% reduce resolution, if selected
        compartment_boundaries_reduced = compartment_boundaries.copy()  # the effective layer boundaries will be based on the available data, but may be simplified, if selected
        for compartment in compartments:
            if not (compartment in vert_res.keys()):
                continue  # just skip, if no specifications for this compartment are available
            if (vert_res[compartment] != 0):  # remove all layer boundaries that are not a multiple of the requested resolution
                if (not (vert_res[compartment] in abs(compartment_boundaries_reduced[compartment]))):
                    self.log.log("SEVERE: implementation (\"delta_z\") of compartment layer '" + compartment + "' is " + str(
                        vert_res[compartment]) + ", which is not part of its native resolution. Change one of them.")
                compartment_boundaries_reduced[compartment] = compartment_boundaries_reduced[compartment][
                    compartment_boundaries_reduced[compartment] % vert_res[compartment] == 0]
                print("{} with reduced resolution to delta_z={}:".format(compartment, vert_res[compartment]))
                print(compartment_boundaries_reduced[compartment])

        # %% combine and generate overall vertical geometry ####
        if len(np.setdiff1d(compartments, "relief")) == 0:  # only "relief" specified
            if len(compartment_boundaries_reduced["relief"]) == 1:  # flat relief?
                self.log.log(
                    "Warning: Single compartment 'relief' is flat without vertical extent. Assuming 0.5 m layer thickness.")
                compartment_boundaries_reduced["relief"] = np.array(list(
                    (compartment_boundaries_reduced["relief"][0] - 0.5, compartment_boundaries_reduced["relief"][0])))
            all_bounds = compartment_boundaries_reduced["relief"]

        else:  # more compartments beside "relief"
            from itertools import chain  # for unlisting lists
            from itertools import product  # for doing combinations
            import collections  # for frequency tables

            all_bounds = np.empty(shape=0)  # for collecting boundaries absolute to zero
            rel_bounds = np.empty(shape=0) #for collecting boundaries relative to surface defined by prior compartments
            def _add_boundary(bounds, new_bounds): #combine already collected boundaries by simply concatening
                tt = np.concatenate((bounds, list(new_bounds)), axis=0)
                #tt_unique = np.unique(tt)
                #return((tt, tt_unique))
                return (tt)
            def _stack_boundary(bounds, new_bounds): #combine already collected boundaries by folding
                tt_unique = np.unique(bounds)
                if (len(tt_unique)==0 or len(new_bounds)==0): #if any of the arrays is empty, special treatments is needed
                    tt = np.concatenate((tt_unique, new_bounds))
                else:
                    tt = np.array(list(product(tt_unique, new_bounds))).sum(axis=1)
                #tt_unique = np.sort(np.unique(tt))  # keep only unique values and sort ascending
                #return ((tt, tt_unique))
                return (tt)

            for compartment in np.flip(compartments):
                # if not self.cfg.is_surface[compartment]:  # compartment forms a new surface the subsequent compartments are added to
                #     all_bounds = np.concatenate((all_bounds, list(compartment_boundaries_reduced[compartment])), axis=0) #add boundaries of current compartment
                # else:   #stackable
                #     count_bounds = collections.Counter(sorted(all_bounds))  # count how often each boundary is needed (only for visualization later, may get lost if multiple stackable layers are encounterd)
                #     all_bounds_unique = np.sort(np.unique(all_bounds))  # keep only unique values and sort ascending
                #     # combine with different elevations from this "stackable" layer
                #     all_bounds_unique = np.array(list(product(all_bounds_unique, compartment_boundaries_reduced[compartment]))).sum(
                #         axis=1)
                #     all_bounds_unique = np.sort(np.unique(all_bounds_unique))  # sort ascending
                #     all_bounds = all_bounds_unique

                if self.cfg.reference_to[compartment] == "surface":
                    src = rel_bounds
                    #ref_offset = 0
                else:
                    src = all_bounds
                    #ref_offset =  self.cfg.reference_to[compartment]
                    compartment_boundaries_reduced[compartment] += self.cfg.reference_to[compartment] #shift boundaries by selkected offset

                if self.cfg.is_surface[compartment]:
                    fun = _stack_boundary #combine already collected boundaries by folding
                    src = rel_bounds # combine with the relative bounds
                else:
                    fun = _add_boundary  #combine already collected boundaries by simply concatenating

                #(tt, tt2) = fun(bounds=src, new_bounds=compartment_boundaries_reduced[compartment])
                tt = fun(bounds=src, new_bounds=compartment_boundaries_reduced[compartment]) # + ref_offset)
                if self.cfg.is_surface[compartment]:
                    if self.cfg.reference_to[compartment]!="surface":
                        #(tt, tt2) = _add_boundary(all_bounds, tt)
                        tt = _add_boundary(all_bounds, tt)
                        rel_bounds = np.empty(shape=0) #reset relative boundaries

                if self.cfg.reference_to[compartment]=="surface":
                    rel_bounds = tt
                else:
                    all_bounds = tt


            if (len(rel_bounds) > 0): #any pending relative boundaries, that so far haven't found a surface?
                #(all_bounds, tt2) = _add_boundary(bounds=all_bounds, new_bounds=rel_bounds) #make the pending boundaries relative to 0
                all_bounds = _add_boundary(bounds=all_bounds, new_bounds=rel_bounds) #make any  pending relative boundaries relative to 0

        #reduce total vertical/or horizontal extent, if selected
        all_bounds = np.array(all_bounds)
        all_bounds = np.round(all_bounds, decimals=3)  # avoid numerical rounding effects

        #if selected, do vertical cropping
        inf = float("inf")
        crop_z_min = getattr(self.cfg, "crop_z_min", -inf)
        crop_z_max = getattr(self.cfg, "crop_z_max",  inf)

        index2keep = np.logical_and(all_bounds >= crop_z_min, all_bounds <= crop_z_max)
        all_bounds = all_bounds[index2keep]

        count_bounds = collections.Counter(sorted(all_bounds))  # count how often each boundary is needed (only for visualization later)

        compartment_boundaries_reduced["total"] = np.unique(all_bounds)  # store all found boundaries
        # only meaningful for flat relief:
        compartment_boundaries_reduced["total_surf"] = compartment_boundaries_reduced["total"][
            compartment_boundaries_reduced["total"] >= 0]
        compartment_boundaries_reduced["total_sub"] = compartment_boundaries_reduced["total"][
            compartment_boundaries_reduced["total"] <= 0]

        # %% plot original and reduced resolution of compartment layers
        if plot_layer_geometry != False:
            import matplotlib.pyplot as plt

            def pseudolog(x,
                          power=1 / 4):  # funky transformation to be able to plot large ranges of y-values in quasi-log-style
                x = np.array(x)
                sigma = 0.05
                base = np.exp(1)
                return (np.arcsinh(x / (2 * sigma)) / np.log(base))

            def trans(x):
                #    return(x) #no transformation
                return (pseudolog(x))  # use pseudolog transformation

            # find limits for plot
            miny = 1.2 * min(compartment_boundaries_reduced["total"])
            maxy = 1.2 * max(compartment_boundaries_reduced["total"])

            n_c_layers = len(compartments)  # total number of compartments

            fig = plt.figure()
            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])  # main axes

            # ax.set_xticks([i for i,k in enumerate(compartments)])
            # ax.set_xticklabels(compartments)
            at = np.array([0, 0.05, 0.1, 0.5, 1, 2, 5, 10, 100, 500])
            at = np.unique(np.concatenate((-at, at)))
            ax.set_yticks(trans(at))
            ax.set_yticklabels(at)
            ax.set_xticks([-10])  # no ticks for x-axis
            plt.xlabel('# of layers')
            plt.ylabel('above ground [m]')
            ax.axis([0.5 - 1, 1 + n_c_layers + 1] + list(trans([miny, maxy])))

            # add zero line
            plt.hlines(0, -20, 20, color="black", linewidth=1)

            # number of boundaries per compartment
            # n_bounds = [len(el) for i, el in enumerate(compartment_boundaries.values())]

            #           max_n_boundaries = max(n_bounds) #maximum number of boundaries specified

            # layer_widths = np.zeros( (max_n_boundaries-1, len(surface_layers)) )

            #            hside=-1
            #            lay_bounds = compartment_boundaries [compartment]

            # cmap = plt.get_cmap('Set1') # get palette
            # cmap = plt.get_cmap('Set3') # get palette
            cmap = plt.get_cmap('Set2')  # get palette

            # plot rectangle based on specification of corner points, not width
            def rect(x, y, color=None,
                     linestyle=None, lw=1):
                x = np.array(x)
                y = np.array(y)
                plt.plot(x[[0, 0, 1, 1, 0]], y[[0, 1, 1, 0, 0]], color=color,
                         linestyle=linestyle, lw=lw)

            # function to plot the bounds/layers of a compartment
            def plot_bar2(lay_bounds, compartment, hside, count_bounds=None):
                # hside = 1 #horizontal plot position of bar. -1: left;  1: right of tick
                # vside = 1 #vertical   plot position of bar. -1: below; 1: above x-axis
                bwidth = 0.25  # width of bars
                spacing = 0.02  # spacing of bars
                maxwid = 10  # maximum width of separator lines in final bar
                compartment_no = np.nonzero(compartment == compartments)[0]
                if len(compartment_no) == 0:
                    compartment_no = len(compartments)  # put unfound entries (ie "total") to the end
                else:
                    compartment_no = compartment_no[0]

                xleft = compartment_no + hside * (bwidth + spacing)
                xright = compartment_no + hside * spacing
                if (len(lay_bounds) > 1):
                    for i in range(len(lay_bounds)):
                        rect(x=[xleft, xright], y=[trans(lay_bounds[i - 1]), trans(lay_bounds[i])],
                             color=cmap(compartment_no),
                             linestyle=np.where(hside < 0, 'solid', 'dotted'),
                             # facecolor = 'blue',
                             )

                # add lines with thickness according to frequency
                if count_bounds != None:
                    # for i in range(len(count_bounds)-1):
                    for key, item in count_bounds.items():
                        y = trans(key)
                        plt.plot([xleft, xright], [y, y], linewidth=maxwid * item / max(count_bounds),
                                 color=cmap(compartment_no))
                        plt.text(xright + 1, y, s=item, horizontalalignment='left',
                                 verticalalignment='center')
                    plt.text(s="#times boundary used", x=compartment_no + 1 + spacing + bwidth * 1.1, y=trans(0.01),
                             horizontalalignment='left',
                             verticalalignment='bottom', color="black", rotation="vertical")

                # add number of layers
                plt.text(s=len(lay_bounds) - 1, x=(xleft + xright) / 2, y=trans(0.01), horizontalalignment='center',
                         verticalalignment='bottom', color=cmap(compartment_no))
                # addname of compartment
                plt.text(s=compartment, x=compartment_no + spacing + bwidth * 1.1, y=trans(0.01),
                         horizontalalignment='left',
                         verticalalignment='bottom', color=cmap(compartment_no), rotation="vertical")

            # end function plot_bar2

            for key, item in compartment_boundaries.items():
                if self.cfg.reference_to[compartment] == "surface":
                    ref_offset = 0
                else:
                    ref_offset = self.cfg.reference_to[compartment]
                # plot original resolution
                plot_bar2(lay_bounds=item+ref_offset, compartment=key, hside=-1)

                if sum(vert_res.values()) != 0:  # plot reduced resolution
                    plot_bar2(lay_bounds=compartment_boundaries_reduced[key], compartment=key, hside=1)

            # plot resulting composite resolutions
            plot_bar2(lay_bounds=compartment_boundaries_reduced["total"], compartment="total", hside=1,
                      count_bounds=count_bounds)

            # legend("topright", legend = compartments, lty=1, col=1:length(compartments))
            ax.plot([], label='original', linestyle='solid', color="black")
            ax.plot([], label='aggregated', linestyle='dotted', color="black")
            ax.legend()

            dest_file = os.path.join(self.cfg.scenario_dir,
                                     plot_layer_geometry)
            fig.savefig(dest_file, bbox_inches='tight', dpi=150)

        # return values
        self.compartment_boundaries = compartment_boundaries_reduced
        # update domain properties
        z_ = compartment_boundaries_reduced["total"] #these are the upper boundaries of the layers
        zmin = min(z_)
        z_ = z_[1:] #the first entry is the lowermost boundary and stored separately
        nz = len(z_)
        nx = len(x_)
        ny = len(y_)
        domain_props = {"domain_size": x_[-1] - x_[0] + x_[1] - x_[0],
                        # grid resolution
                        "res": (x_[-1] - x_[0]) / (len(x_) - 1),
                        # width and height of the grid,
                        "width": len(x_),
                        "height": max(z_) - zmin,
                        # coordinates of grid corners,
                        "x_min": x_[0],
                        "x_max": x_[-1],
                        "y_max": y_[0],
                        "y_min": y_[-1],
                        "nz": nz,
                        "upper_limits": -np.flip(z_), #
                        "zmin": - zmin,
                        "nx": nx,
                        # these two are actually redundant, but we use them here to allow non-quadratic grids for testing
                        "ny": ny
                        }

        self._update_domain_from_props(domain_props)  # set domain properties
        print("  -> common layer geometry involves {} layers: {}".format(nz, np.round(np.concatenate(([zmin], z_)), decimals=2)))


    def _get_layer_specs(self, compartments):
        """
          - extract and check optional specification (path, delta_z, reference_to, is_surface) from configuration settings and store them as dictionaries in
          self.cfg.landscape.vert_res,  self.cfg.landscape.compartment_source, self.cfg.landscape.reference_to, self.cfg.landscape.is_surface
        """
        if hasattr(self, "vert_res") and hasattr(self, "compartment_source"): #check if these attributes have already been determined
            return()

        self.cfg.vert_res = {}
        self.cfg.compartment_source = {}
        self.cfg.reference_to = {}
        self.cfg.is_surface = {}
        for compartment in compartments:
            if hasattr(self.cfg_full.generate.layers, compartment):
                yulia_layer = self.cfg_full.generate.layers[compartment]
            else:
                self.log.log(
                    "SEVERE:\tLayer '{}' specified in field 'landscape' not found in sheet 'generate_layers' .".format(
                        compartment))

            if yulia_layer.distribution != "distributed":
                self.log.log(
                    "SEVERE:\tLayer '{}' must have distribution 'distributed' in sheet 'generate_layers' .".format(
                        compartment))

            tt = getattr(yulia_layer, "delta_z", 0)
            if str(tt) == "":
                d_z = 0
            try:
                d_z = float(tt)
            except (TypeError, ValueError):
                self.log.log(
                    "WARNING:\Grid '{}' specified in field 'geodata' of sheet 'generate_layers' needs a numeric value for 'delta_z'.".format(
                        grid_name))
            tt = 0
            self.cfg.vert_res[compartment] = tt

            tt = getattr(yulia_layer, "reference_to", 0)
            if (tt == "0" or tt == ""):
                tt = 0
            if (tt != "surface"):
                try:
                    tt = float(tt)
                except (TypeError, ValueError):
                    self.log.log(
                        "WARNING:\Value of 'reference_to' for '{}' specified in field 'geodata' of sheet 'generate_layers' must be a number or 'surface', ignored.".format(
                            grid_name))
                    tt = 0
            self.cfg.reference_to[compartment] = tt

            tt = getattr(yulia_layer, "is_surface", False)
            if (tt == "0" or tt == ""):
                tt = False
            allowed_vals = [0, 1]
            if not (tt in allowed_vals):
                self.log.log(
                    "WARNING:\Value of 'is_surface' for '{}' specified in field 'geodata' of sheet 'generate_layers' must be one of {}, ignored.".format(
                        grid_name, allowed_vals))
                tt = False
            self.cfg.is_surface[compartment] = bool(tt)

            if hasattr(yulia_layer, "geodata"):
                grid_name = yulia_layer.geodata
            else:
                self.log.log(
                    "SEVERE:\tLayer '{}' must have distribution attribute 'geodata' in sheet 'generate_layers' .".format(
                        compartment))

            if hasattr(self.cfg_full.generate.geodata, grid_name):
                grid_specs = self.cfg_full.generate.geodata[grid_name]
            else:
                self.log.log(
                    "SEVERE:\Grid '{}' specified in field 'geodata' of sheet 'generate_layers' not found in sheet 'generate_geodata' .".format(
                        grid_name))

            self.cfg.compartment_source[compartment] = grid_specs.path

            #return (self)

    def _merge_ncdfs(self):
        """
        - combine compartments to vertical geometry of URANOS-layers
        """
        import numpy as np
        standalone = False
        # %% for standalone operation
        from os.path import exists, dirname  # for checking file existence
        from os import makedirs
        import netCDF4  # for reading netcdf
        #        import sys #for sys.exit
        from itertools import product  # for doing combinations

        print("Merging compartments to common geometry...")

        grids2merge = relevant_grids  # these are the grids that are searched for in the netcdfs and which are finally merged

        composite_grids = {}  # dictionary of composited/merged grids

        if standalone:
            # %% arguments to be passed when reshaped as a function

            from _read_central_table import read_vjfc_xls
            xls_file = "../../requirements_virtual_JFC.xlsx"
            realization_id = "hex1"
            compartment_source, combination_rules = read_vjfc_xls(xls_file, realization_id=realization_id)

            dest_dir = "../../1_realizations/" + realization_id + "/"  # destination directory for resulting composite NetCDFs
            fn = dest_dir + 'fulldomain_3D.nc'

            # return value
            # ...
            # currently saved as "fulldomain_3D.nc" in dest_dir

            # %% settings
            # (to be passed as arguments or read from Yulia config file)
            import pickle

            # dimensions of domain to produce (in cells)
            n_x = 1000
            n_y = 1000

            # compartment_boundaries_reduced: the combined vertical sequence of layers as determined by 1_derive_geometry.py
            # currently re-imported from pickle-file
            with open(realization_id + '.pkl', 'rb') as file:
                compartment_boundaries_reduced = pickle.load(file)
            n_z = len(compartment_boundaries_reduced[
                          "total"]) - 1  # total number of layers to generate (is one less than the number of boundaries)

        else:  # not "standalone"
            #get dimensions from ncdf
            n_x = self.domain.nx
            n_y = self.domain.ny
            n_z = self.domain.nz

            # the combined vertical sequence of layers as determined by derive_geometry.py
            fn = getattr(self.cfg, "realization_ncdf_path", "")  # get path to netCDF to load
            dest_dir = dirname(fn)  # destination directory for resulting composite NetCDFs
            # set "target" for the grids that will be generated

        from datetime import datetime
        start = datetime.now()

        composite_grids["material"] = np.zeros((n_y, n_x, n_z),
                                                  dtype=np.uintc)  # matrix cube holding the resulting merged material IDs
        # grid will be created later only when they are needed
        # composite_grids["porosity"]    = np.zeros((n_y, n_x, n_z),
        #                                           dtype=float)  # matrix cube holding the resulting merged porosities
        # composite_grids["density"]     = np.zeros((n_y, n_x, n_z),
        #                                           dtype=float)  # matrix cube holding the resulting merged density

        compartment_boundaries_reduced = self.compartment_boundaries
        compartments = np.array(list(compartment_boundaries_reduced.keys()))
        # only use the names of the *primary* compartments, discard the composited ones
        compartments = compartments [~np.in1d(compartments , ["total", "total_sub", "total_surf"])]

        self._get_layer_specs(compartments)  # check and get paths and vertical resolution to compartments
        vert_res = self.cfg.vert_res
        compartment_source = self.cfg.compartment_source

        missing = np.setdiff1d(compartments, list(compartment_source.keys()))
        if len(missing) > 0:
            self.log.log(
                "Compartment(s) {} not specified in sheet 'compartment_combination_rules' of table '{}'.".format(
                    missing, xls_file))

          # 10.634636: slower, but necessary, when the match falls between bounds
        def find_match(bounds, value):  # for finding matching layers: return index of 'value' in 'bounds'
            value = np.float32(value) #otherwise, float64 value wreck the comparison to euqality
            for ix, vb in enumerate(bounds):
                if vb >= value:
  #                  if (ix == 0) and value < bounds[0]:  # match is below first boundary
  #                      return (-1)
 #                   else:
#                        return (ix)
                    return (ix-1)
            return (-1)


        # def find_match(bounds, value):  # for finding matching layers: return index of 'value' in 'bounds'
        #     import numpy as np
        #     #value=2
        #     #bounds = np.array([1, 5, 9, 10])
        #     ix = np.where(value <= bounds)[0]
        #
        #     if (len(ix) == 0):
        #         ix = -1 #nothing found
        #     else:
        #         ix = ix[0]
        #         if (ix == 0) and value < bounds[0]: #match is below first boundary
        #             ix = -1 #nothing found
        #     return (ix)

        vfind_match = np.vectorize(find_match, excluded=['bounds']) #vectorized version of find_match

        # bounds = np.array([1,2,3,4,5])
        # value =4
        # find_match(bounds, value)

        # %% combine layers

        vertical_reference = np.zeros((n_y, n_x), dtype=np.float32)  # contains the vertical reference datum for subsequent layers
        # may be later modified by relief
        vertical_reference_updated = vertical_reference.copy()  # for storing a copy in work
        vertical_reference_unique = 0  # value used to indicate a homogenous vertical reference, if any. May turn to None later in the case of heterogenous relief

        inf = float("inf")
        crop_z_min = getattr(self.cfg, "crop_z_min", -inf)
        crop_z_max = getattr(self.cfg, "crop_z_max",  inf)

        # combine/merge compartments to construct URANOS layers ####
        for compartment in compartments:  # ["relief"]
            # for compartment in ["atmosphere"]: #atmosphere
            print("...adding compartment '" + compartment + "'")

            # open netcdf-file of current layer
            filename = compartment_source[compartment]
            if not exists(filename):
                # self.log.log("SEVERE: {} does not exist, terminated.".format(filename))
                print("'{}' does not exist, skipped.".format(filename))
                continue

            nc_handle = netCDF4.Dataset(filename)

            coords = {}  # for collecting x, y, and z dimension coords
            coords['x_'] = nc_handle.variables['x'][:]  # x-coords of grid cells
            coords['y_'] = nc_handle.variables['y'][:]  # y-coords of grid cells
            coords['z_'] = nc_handle.variables['z'][:]  # upper boundaries of layers

            do_flip = {}  # check which dimensions need to be flipped to match Yulia standards
            do_flip["x"] = coords['x_'][0] > coords['x_'][-1]
            do_flip["y"] = coords['y_'][0] < coords['y_'][-1]  # we want y-axis to point northwards
            do_flip["z"] = coords['z_'][0] > coords['z_'][-1]  # we want the lower layers first (for internal treatment in this subroutine only, later inverted)

            for idim in do_flip.keys():
                if do_flip[idim]:
                    coords[idim + "_"] = np.flip(coords[idim + "_"], axis=0)

            x_ = coords['x_'][0:self.dbg_maxdim]
            y_ = coords['y_'][0:self.dbg_maxdim]

            # check consistency of dimensions
            if (n_x != len(coords['x_'][0:self.dbg_maxdim])):
                self.log.log(
                    "SEVERE: Dimension mismatch: nx_boundaries={} does not match nx_netCDF={}, terminated.".format(n_x,
                                                                                                                   len(
                                                                                                                       nc_handle.variables[
                                                                                                                           'x'][
                                                                                                                       :])))
            if (n_y != len(coords['y_'][0:self.dbg_maxdim])):
                self.log.log(
                    "SEVERE: Dimension mismatch: ny_boundaries={} does not match ny_netCDF={}, terminated.".format(n_y,
                                                                                                                   len(
                                                                                                                       nc_handle.variables[
                                                                                                                           'y'][
                                                                                                                       :])))

            # check consistency with boundaries from netcdf-file
            bounds_temp = np.hstack([coords['z_'], nc_handle.variables['zmin'][:]])
            bounds_temp = np.sort(np.unique(bounds_temp))
            if (vert_res[compartment] != 0):  # remove all layer boundaries that are not a multiple of the requested resolution
                if (not (vert_res[compartment] in abs(compartment_boundaries_reduced[compartment]))):
                    self.log.log(
                        "SEVERE: implementation (\"delta_z\") of compartment layer '" + compartment + "' is " + str(
                            vert_res[
                                compartment]) + ", which is not part of its native resolution. Change one of them.")
                bounds_temp = bounds_temp[bounds_temp % vert_res[compartment] == 0]


            if len(bounds_temp) == 1 and compartment == "relief":  # special case: flat relief
                bounds_temp = compartment_boundaries_reduced["relief"]  # force consistency with values fixed before

            if self.cfg.reference_to[compartment] == "surface":
                ref_offset = vertical_reference  #vertical reference is a prior surface
            else:
                bounds_temp += self.cfg.reference_to[compartment]  # shift boundaries by selected offset
                ref_offset = 0  #vertical reference is already accounted for in the shifted boundaries

            if (compartment_boundaries_reduced[compartment].shape != bounds_temp.shape):
                self.log.log(
                    "SEVERE: Dimension mismatch: z_boundaries={} does not match z_netCDF={}, terminated.".format(
                        compartment_boundaries_reduced[compartment], nc_handle.variables['z'][:]))
            if (any(compartment_boundaries_reduced[compartment] != bounds_temp)):
                self.log.log(
                    "SEVERE: Dimension mismatch: z_boundaries={} does not match z_netCDF={}, terminated.".format(
                        compartment_boundaries_reduced[compartment], nc_handle.variables['z'][:]))

            if len(bounds_temp) == 1 and compartment != "relief":
                print("... ...only one layer with zero vertical extent, skipped.")
                continue
            # if compartment!="relief":
            # s    bounds_temp = bounds_temp[1:] #discard lowermost bound
            bounds_temp = np.round(bounds_temp, decimals=2) # rounding is necessary due to numeric imprecision in comparison otherwise
            bounds_temp = bounds_temp.astype(np.float32, copy=False)


            # extract relevant matrix cubes; transpose, if in reverse order of dimensions
            grids2load = np.intersect1d(list(grids2merge.keys()), list(nc_handle.variables.keys()))
            if (len(grids2load) == 0):  # check if any grid has been found
                self.log.log("SEVERE: No valid 3D-grids found in the file, terminated.")
            print("... adding {}".format(grids2load))
            sourcegrids = {}  # list of 3D-grids loaded from current compartment file
            required_dim_order = ('y', 'x', 'z')  # order of dimensions required in the following steps (changed later, in remaining parts, though)

            for gg in grids2load:
                dest = relevant_grids[gg] #get key where to put the current grid
                a = nc_handle.variables[gg].dimensions
                if (np.size(a) != np.size(required_dim_order)) or (any(np.sort(a) != np.sort(required_dim_order))):
                    self.log.log("SEVERE: Invalid or missing dimensions. Expected: {} Found: {} .".format(required_dim_order, a))
                #                 if nc_handle.variables[gg].dimensions==('z', 'y', 'x'):
               #                    sourcegrids[gg] = nc_handle.variables[gg][:].T
                # else:
                sourcegrids[dest] = nc_handle.variables[gg][:]
                corres = [a.index(val) for i, val in enumerate(required_dim_order)]  # find corresponding order of dimensions
                sourcegrids[dest] = np.transpose(sourcegrids[dest], axes=corres)  # transpose to "y,x,z" as required in the following script

                for idim in do_flip.keys():
                    if do_flip[idim]:
                        axis_i = np.where(np.array(required_dim_order) == idim)[0][0]
                        sourcegrids[dest] = np.flip(sourcegrids[dest], axis=axis_i)  # flip matrix if required

            nc_handle.close()

            #reduce bounds, if cropping has been enabled
            # find lowermost and uppermost elevation to insert into
            min_z_expected = max(crop_z_min, min(bounds_temp) + np.min(ref_offset))
            min_z_expected = max(min_z_expected, compartment_boundaries_reduced["total"][1]) #we don't need to search below the lower boundary of the lowest layer
            max_z_expected = min(crop_z_max, max(bounds_temp) + np.max(ref_offset))

            # def bound(value, low, high, ):
            # # limit a value to a range between low and high
            #     return max(low, min(high, value))
            #
            # min_z_expected = bound(crop_z_min, crop_z_max, min(bounds_temp) + np.min(ref_offset))
            # max_z_expected = bound(crop_z_min, crop_z_max, max(bounds_temp) + np.max(ref_offset))

            if (min_z_expected > max_z_expected): #this compartment does not fit in the predetermined geometry
                if (crop_z_min != -inf or crop_z_max != inf): #z-cropping had been enabled, so this is a valid case
                    self.log.log("Compartment {} is completely outside predetermined geometry, skipped.".format(compartment))
                    continue
                else: #z-cropping had NOT been enabled, so something went wrong
                    self.log.log("SEVERE: Cannot accommodate compartment {} into predetermined geometry. File a bug issue.".format(compartment))

            # find lowermost and uppermost relevant URANOS layers to insert into
            first_relevant_slice = find_match(bounds=compartment_boundaries_reduced["total"][0:], value=min_z_expected)
            last_relevant_slice  = find_match(bounds=compartment_boundaries_reduced["total"][1:], value=max_z_expected)

            if (first_relevant_slice == -1) | (last_relevant_slice == -1):
                self.log.log(
                    "SEVERE: Vertical range of '{}' is {}, which does not match joint boundaries {}. Re-run derivation of geometry by setting \"ncdf_action\" to \"merge\" in xls.".format(
                        compartment, bounds_temp, compartment_boundaries_reduced["total"]))
            last_relevant_slice = min(last_relevant_slice+1, len(compartment_boundaries_reduced["total"])-2)

            if (compartment == "relief"):  # "relief" must also be filled below its actual start, so make this work ii: this could be implemented as a trickle_down flag for other layers, too
                relief_first_slice = first_relevant_slice
                first_relevant_slice = 0  # for building relief, start right at the bottom

            #if (len(bounds_temp)>1): #flat relief may have only one boundary
            #    bounds_temp = bounds_temp[1:]  # discard lowermost bound, as we are not using information below the lowermost extent of the compartment

            # go through all following higher URANOS layers
            ix = np.zeros((n_y, n_x),
                          dtype=int)  # matrix holding the z-indices to the vertically corresponding layer of the compartment currently in work

            # for display purposes only: display the changed part in the column (if any)
            display_coords = (0, 0)  # row, col to be used for displaying progress
            display_coords = (493, 135)  # row, col to be used for displaying progress
            display_colum_last = composite_grids["material"][display_coords[0], display_coords[1], :].copy()
            layer_start_display = 0

            for i in range(first_relevant_slice,
                           last_relevant_slice + 1):  # go through the composite layers, where the current compartment needs to be inserted to
                #for i in range(first_relevant_slice,  5):  # rr removeme, debug only
                #    print("DEBUG SHORTCUT... ... {}/{}".format(i, last_relevant_slice))
                print("... ... {}/{}".format(i, last_relevant_slice))

                # determine position of current URANOS layer
                # lower_bound = compartment_boundaries_reduced["total"][i  ]
                upper_bound = compartment_boundaries_reduced["total"][i + 1] #upper limit of currently-treated URANOS layer [m]

                if self.cfg.reference_to[compartment] == "surface":  # Is this compartment referenced to a 0 (or a prior surface?)
                    vertical_reference_unique_temp = vertical_reference_unique    #set the vertical reference to offset resulting from prio compartments (surfaces)
                else:
                    vertical_reference_unique_temp = 0 #self.cfg.reference_to[compartment] #set the vertical reference to constant value, only for the current compartment


                # find slices/indices in source compartment that correspond to current URANOS layer
                if vertical_reference_unique_temp != None:  # if the vertical reference is uniform, things can be simplified
                    ix[:, :] = find_match(value=np.round(upper_bound - vertical_reference_unique_temp, decimals=2),
                                          # rounding is necessary due to numeric imprecision in comparison otherwise
                                          bounds=bounds_temp)

                else:  # non-uniform vertical reference
                    z_values = np.round(upper_bound - vertical_reference, decimals=2)
                    #let's save some work by computing the corresponding layer ("find_match") for each unique z-value only once

                    unique_z_values = np.unique(z_values)
                    unique_ix  = vfind_match(value=unique_z_values, bounds=bounds_temp)
                    for ii,z_val in enumerate(unique_z_values):
                        ix[z_values == z_val] = unique_ix[ii] #fill the array of the indices with the indices computed for the unique instances


                found_cells = np.sum(ix != -1)
                if found_cells == 0:
                    if (compartment == "relief") and (i <= relief_first_slice):
                        ix[:, ] = 0  # we are still below the relief surface, just fill with first relief layer
                    else:
                        continue  # no correspondence in current compartment, proceed to next URANOS layer

                # ix_converted = 1:(n_x*n_y)   + (ix-1) * n_x * n_y #convert index of z-dimension to single-value overall index

                # replace/overwrite any existing IDs

                y_x_index = np.array(list(product(range(0, n_y), range(0, n_x))))

                for gg in sourcegrids.keys():  # merge the loaded grids into the composite ones
                    if (not gg in composite_grids.keys()): #check if the required grid has already been created. If not, create it
                        composite_grids[gg] = np.zeros_like(composite_grids["material"], dtype=np.float32)

                    # "pointer" to the corresponding layers of the source compartment
                    source = sourcegrids[gg][(y_x_index[:, 0],
                                              y_x_index[:, 1],
                                              ix.flatten())]

                    # source =  material_id[[tuple(y_x_index[:,0]),
                    #            tuple(y_x_index[:,1]),
                    #            tuple(ix.flatten() )]]

                    # if (not all(source == source1)):
                    #    self.log.log("SEVERE: strange!")

                    # find non-na values and mask them
                    source[source == sourcegrids[gg].get_fill_value()] = np.ma.masked
                    source[source == sourcegrids[gg]._fill_value] = np.ma.masked #old version - probably not robust in sóme cases
                    source[source == 0] = np.ma.masked
                    source[ix.flatten() == -1] = np.ma.masked

                    non_nas = ~ source.mask.reshape(n_y,
                                                    n_x)  # matrix-shaped index to matrix of composited values, where values need to be written to
                    # non_nas = np.logical_not ( (source ==  material_id._fill_value) | (source == 0) | (ix.flatten() == -1) | source.mask)
                    if (compartment=="detector" and np.any(non_nas)): #debug
                        pass

                    # compartment_combination_rules:
                    # "replace": overwrite all prior layers
                    composite_grids[gg][:, :, i][non_nas] = source.compressed()

                    # "fill": only write on voxels not defined so far
                    #composite_grids[gg][:, :, i][non_nas] = source.compressed()

                    # mix properties
                    # ...(not yet implemented)


                if self.cfg.is_surface[compartment]:  # is this forming a new reference surface, e.g. 'relief'?
                    vertical_reference_updated[
                        non_nas] = upper_bound  # update the vertical reference for the following layers, if this is a new surface

                #for display purposes only: display the changed part in the column (if any)
                display_colum_new = composite_grids["material"][display_coords[0], display_coords[1], :]
                layer_first_change = np.where(display_colum_last != display_colum_new)[0]
                if (len(layer_first_change)!=0):
                    display_colum_last = display_colum_new.copy() #update current state of monitored column
                    layer_first_change = layer_first_change[0] #use the actual index to the first element that has been changed
                    if (np.abs(layer_start_display - (layer_first_change-3)) > 4): #if the first layer to be displayed changed by more than 5, update it
                        layer_start_display = max(0, layer_first_change-3)

                    print(" merged["+str(display_coords[0])+","+str(display_coords[1])+",0-10]: ", composite_grids["material"][display_coords[0], display_coords[1], :][0:10],
                          "       merged["+str(display_coords[0])+","+str(display_coords[1])+",", layer_start_display,":]: ", self.shorten_array(composite_grids["material"][display_coords[0], display_coords[1], :][layer_start_display:], max_elements= 12))
                    # if compartment == "vegetation":
                    #     pass

            # end loop thru URANOS layers
            if ((
                    vertical_reference != vertical_reference_updated).any()):  # has there been any change to the vertical reference?
                vertical_reference = vertical_reference_updated  # use the new reference for the following layers

            if (vertical_reference[0, 0] == vertical_reference).all():  # check if all values are equal
                vertical_reference_unique = vertical_reference.min()  # the vertical reference changed, but homogenously. Makes things easier
            else:
                vertical_reference_unique = None  # no longer a unique value for vertical reference
            # end loop thru compartment layers


        # set remaining unset voxels to "air", see MaterialCodes.txt
        composite_grids["material"][composite_grids["material"] == 0] = 1
        # tidy up
        del (vertical_reference)
        del (vertical_reference_unique)
        del (vertical_reference_updated)
        del (sourcegrids)

        if (not "sm" in composite_grids.keys()):  # check if soil moisture has been read. If not, create it and set to zero (obsolete when SM has been read directly)
            composite_grids["sm"] = self._URANOScode2soilmoisture(composite_grids["material"])
        del grids2merge["density"] #remove entry alias just used for legacy
        # check if all grids have been read. If not, create it and set to zero, so we can use it henceforward
        for key,val in grids2merge.items():
            if (not val in composite_grids.keys()):
                composite_grids[val] = np.zeros_like(composite_grids["material"], dtype=np.float32)

        # find voxels with theta specified but not being soil
        soil_cells = np.isin(composite_grids["material"], self.cfg_full["material"]['special_codes']["soil"])
        non_soil = np.logical_and(composite_grids["sm"] != 0, np.logical_not(soil_cells))
        if (np.any(non_soil)):
            self.log.log("WARNING: soil moisture has been specified for {} voxels, which are not soil. Ignoring these.".format(np.sum(non_soil)))
            composite_grids["sm"][non_soil] = 0. #correct SM for voxels that are no soil

        # %% compute *dry* density values, where not explicitly specified yet (needed for complete density grid)

        # compute density represented by the material codes and porosity [g/cm3], (excluding soil moisture)
        density_uncorrected = self._materialcodes2density(material=composite_grids["material"],
                                                            porosity=composite_grids[
                                                                "porosity"])

        not_specified = composite_grids["density"] == 0  # these voxels have no density explicitly specified so far
        composite_grids["density"][not_specified] = - density_uncorrected[
            not_specified]  # assign density implicitly assigned via material codes. Use negative values as only these will further be corrected by air pressure effects
        del (density_uncorrected)

        # for soil cells, add the soil moisture  [%] to total density. Preserve negative sign used as flag for "unspecified"
        composite_grids["density"][soil_cells] += np.sign(composite_grids["density"][soil_cells]) * \
                                                  composite_grids["sm"][soil_cells]/100

        # %% set attributes so values are available in _calculate_atm_density
        for key, val in composite_grids.items():
            setattr(self, key,
                    val)  # set generated grids (material, porosity and density) to attributes of self (for further use later)

        # calculate atmospheric density and correct density for air voxels accordingly
        if (getattr(self.cfg, "atm_pressure_correction", False) ==True):
            self._calculate_atm_density(elevs=compartment_boundaries_reduced["total"])  # correct for atmospheric density
        composite_grids["density"] = self.density #retrieve results stored in attribute of self
        #np.all(self.density == composite_grids["density"])
        composite_grids["density"] = abs(composite_grids["density"]) #remove negative sign used as flag before

        end = datetime.now()
        print("Time of execution: {}".format(end - start)[5:])
        #        print("shortcut")
        #        return()

        # %% save netcdf ####

        # create target directory, if not existing
        makedirs(dest_dir, exist_ok=True)

        self.log.log("...  NetCDFs merged, saving composite NetCDF as '{}'...".format(fn))
        nc_handle = netCDF4.Dataset(fn, 'w', format='NETCDF4')

        # create dimensions of NetCDF
        nc_handle.createDimension('x', n_x)
        x = nc_handle.createVariable('x', 'i', ('x',))
        #x[:] = np.arange(0, n_x, 1.0)
        #x[:] = coords["x_"]
        x[:] = x_
        x.units = "m"

        nc_handle.createDimension('y', n_y)
        y = nc_handle.createVariable('y', 'i', ('y',))
        #y[:] = np.arange(0, n_y, 1.0)
        #y[:] = coords["y_"]
        y[:] = y_
        y.units = "m"

        nc_handle.createDimension('z', n_z)
        z = nc_handle.createVariable('z', 'f', ('z',))
        z[:] = compartment_boundaries_reduced["total"][1:]
        z.units = "m"

        zmin = nc_handle.createVariable('zmin', 'f')
        zmin[:] = compartment_boundaries_reduced["total"][0]
        zmin.units = "m"

        material_id_nc = nc_handle.createVariable(varname="material_id", datatype="i2", dimensions=('y', 'x', 'z'), compression='zlib')
        material_id_nc.units = "id"
        material_id_nc[:, :, :] = composite_grids["material"]

        density_nc = nc_handle.createVariable(varname="dens", datatype="f", dimensions=('y', 'x', 'z'), compression='zlib', least_significant_digit=6)
        density_nc.units = "g/cm3"
        density_nc[:, :, :] = composite_grids["density"]
        composite_grids["density"] = np.array(density_nc[:])  #make truncation of digits consistent with the variable stored in memory

        porosity_nc = nc_handle.createVariable(varname="porosity", datatype="f", dimensions=('y', 'x', 'z'), compression='zlib', least_significant_digit=6)
        porosity_nc.units = "cm3/cm3"
        porosity_nc[:, :, :] = composite_grids["porosity"]
        composite_grids["porosity"] = np.array(porosity_nc[:])  # make truncation of digits consistent with the variable stored in memory

        sm_nc = nc_handle.createVariable(varname="sm", datatype="f", dimensions=('y', 'x', 'z'), compression='zlib', least_significant_digit=6)
        sm_nc.units = "cm3/cm3"
        sm_nc[:, :, :] = composite_grids["sm"]
        composite_grids["sm"] = np.array(sm_nc[:])  # make truncation of digits consistent with the variable stored in memory

        nc_handle.close()
        self.log.log("... done. Re-arranging grids ...")

        # %% set attributes, i.e. "return values" for later use in other methods
        for key, val in grids2merge.items():
            grid = composite_grids[val]
            grid = np.flip(grid, axis=2) #  flip along z ("lowest layer first" to "highest first")
            grid = np.transpose(grid, axes = (2, 0, 1))  # transpose y,x,z to z,y,x as required by Yulia
            #axes = (2, 1, 0))  # transpose y,x,z to z,x,y as required by Yulia
            #                                            axes=(2, 0, 1)))  # transpose x,y,z to z,x,y as required by Yulia

            setattr(self, val, grid) #export generated grids (material, porosity and density) to attributes of self (for further use later)
        self.log.log("... done.")

        # %% verify written file
        if (False):  # rr debug, remove
            nc_handle = netCDF4.Dataset(fn, 'r', format='NETCDF4')

            gg = "material_id"
            # check for correct order of dimensions
            required_dim_order = ('y', 'x', 'z')  # order of dimensions required in the following steps (changed later, though)
            a = nc_handle.variables[gg].dimensions
            if (np.size(a) != np.size(required_dim_order)) or (any(np.sort(a) != np.sort(required_dim_order))):
                self.log.log(
                    "SEVERE: Invalid or missing dimensions. Expected: {} Found: {} .".format(required_dim_order, a))

            material_id2 = nc_handle.variables[gg][:]
            corres = [a.index(val) for i, val in
                      enumerate(required_dim_order)]  # find corresponding order of dimensions
            material_id2 = np.transpose(material_id2, axes=corres)

            np.min(composite_grids["material"])
            np.max(composite_grids["material"])

            np.min(material_id2)
            np.max(material_id2)

            material_id2[10, 10, :]

    def _update_domain_from_props(self, domain_props):
        """update domain properties and related matrices based on properties of loaded ncdf passed via props"""
        if(not "nx" in domain_props.keys()):
            nx = round(domain_props["domain_size"] / domain_props["res"])
        if (not "ny" in domain_props.keys()):
            ny = round(domain_props["domain_size"] / domain_props["res"])
        #ny = nx * 2
        #print("remove me")
        matrix_dims = (domain_props["nz"],) + (domain_props["ny"], domain_props["nx"])
        # check if matrix dimensions need to be changed. Otherwise, existing matrices will not be touched
        if (domain_props["nz"] != 0) and not (hasattr(self, "material") and self.material.shape == matrix_dims):
            # adjust matrix dimensions
            # create 3D output array for material (material_code 1 = air)
            self.material = np.ones(matrix_dims, dtype=np.ushort)
            # create 3D output array for density (in g/cm³, later converted to URANOS multiplicator 1-00)
            self.density = np.zeros(self.material.shape, dtype=np.float32)
            # create 3D output array for porosity ([-], later converted to URANOS %)
            self.porosity = np.zeros(self.material.shape, dtype=np.float32)

        self.domain._set_domain_from_ncdf(domain_props=domain_props)

    def _set_domain_from_ncdf_file(self, filename, use_nz=True):
        """set domain properties, extent and matrices based on the given ncdf file"""
        import netCDF4  # for reading netcdf

        nc_handle = netCDF4.Dataset(filename)
        for var in ['x', 'y', 'z', 'zmin']:
            if not var in nc_handle.variables.keys():
                self.log.log("SEVERE: variable '{}' not found in '{}', terminated.".format(var, filename))

        coords = {}  # for collecting x, y, and z dimension coords
        coords['x_'] = nc_handle.variables['x'][0:self.dbg_maxdim] #0:self.dbg_maxdim: debug option: use subset only. x-coords of grid cells
        nx = len(coords['x_'])
        coords['y_'] = nc_handle.variables['y'][0:self.dbg_maxdim] #0:self.dbg_maxdim: debug option: use subset only. y-coords of grid cells
        ny = len(coords['y_'])
        coords['z_'] = nc_handle.variables['z'][:]  # upper boundaries of layers
        nz = len(coords['z_'])

        do_flip = {}  # check which dimensions need to be flipped to match Yulia standards
        do_flip["x"] = coords['x_'][0] > coords['x_'][-1]
        do_flip["y"] = coords['y_'][0] < coords['y_'][-1]  # we want y-axis to point northwards
        do_flip["z"] = coords['z_'][0] < coords['z_'][-1]  # we want the higher layers first

        for idim in do_flip.keys():
            if do_flip[idim]:
                coords[idim + "_"] = np.flip(coords[idim + "_"], axis=0)

        x_ = coords['x_']
        y_ = coords['y_']
        z_ = coords['z_']


        if use_nz:
            nz = len(z_)
            zmin = nc_handle.variables['zmin'][:]
        else:
            nz=0 #ignore number of z-layers (useful if they have not yet been determined and only the lateral extent is to be set)
            zmin=0


        nc_handle.close()

        domain_props = {"domain_size": x_[-1] - x_[0] + x_[1] - x_[0],
                        # grid resolution
                        "res": (x_[-1] - x_[0]) / (len(x_) - 1),
                        # width and height of the grid,
                        "width": len(x_),
                        "height": max(z_) - zmin,
                        # coordinates of grid corners,
                        "x_min": x_[0],
                        "x_max": x_[-1],
                        "y_max": y_[0],
                        "y_min": y_[-1],
                        "nz": nz,
                        "upper_limits": -z_, #"-" because of URANOS convention with negative pointing upward
                        "zmin": -zmin,       #"-" because of URANOS convention with negative pointing upward
                        "nx": nx, #these two are actually redundant, but we use them here to allow non-quadratic grids for testing
                        "ny": ny
                        }

        self._update_domain_from_props(domain_props=domain_props)
        #return(domain_props) #just for debugging, delete me

    def _create_mixedlayer_objects(self, layer_list):
        """create layer objects of class ""General"" for mixed layers """
        ##
        # generate a simple cfg for this layer to be used as a template for all
        layer_cfg_tmpl = ConfigDict()
        layer_cfg_tmpl.add("layer_type", "General")
        # dummy value
        layer_cfg_tmpl.add("material", "air")
        layer_cfg_tmpl.add("distribution", "distributed")

        # copy attributes from self to the template "layer_cfg_tmpl"
        attr_list = ["domain_size",  # ?do we need these?
                     "resolution"]
        for attr in attr_list:
            if hasattr(self.cfg, attr):
                layer_cfg_tmpl.add(attr, getattr(self.cfg, attr))

        for layer_id, layer_name in enumerate(layer_list):
            # create individual layer configs for each requested layer
            layer_cfg = layer_cfg_tmpl.copy()  # copy the template
            setattr(self.cfg_full.generate.layers, layer_name, layer_cfg)

            # add layer id to cfg
            layer_cfg.add("id", layer_id)
            layer_cfg.add("name", layer_name)

            # create instance of landscape element class
            layer_class = getattr(layer_types,
                                  layer_cfg.layer_type,
                                  lambda x: self.log.log(
                                      "SEVERE:\tUnknown Layer Type: '{}'.".format(
                                          layer_cfg.layer_type)))
            setattr(self, layer_name, layer_class(layer_cfg,
                                                  self.cfg,
                                                  self.cfg_full,
                                                  self.log,
                                                  self.domain))

    # def _prepare_dem_zones(self):
    #     """prepare DEM zones grid"""

    #     # get discretisation step
    #     step_size = self.cfg.discretisation

    #     # calculate elevation zones
    #     self.elev_zones = (self.domain.dem/step_size).astype(np.int64)
    #     self.elev_zones *= step_size
    #     # get min and max of elevation
    #     self.cfg.add("elev_min", self.elev_zones.min())
    #     self.cfg.add("elev_max", self.elev_zones.max())

    def _set_layer_func_list(self):
        # assign layers with special functions, will be exproted to UranosGeometryConfig.dat
        # layers are numbered from top to bottom
        nlayer = len(self.cfg.landscape)
        # find "ground_layer" for URANOS (needed only for display reasons?)
        #        voxel_codes_table = self.cfg_full["material"]['codes_distributed'] #voxel material codes and default densities as used by URANOS
        #       descri = {val["code"]:val["name"] for mat, val in voxel_codes_table.items()}
        # air_codes = [i for i, item in descri.items() if re.search("^ *[aA]ir|^ *[dD]etector", "".join(item))]
        # air_codes = np.unique(np.concatenate(([0], air_codes))) #add the 0, as it is used internally for "nothing"
        #        cells_with_air = np.repeat(0, nz) #for counting cells with air
        # diffs = np.abs(np.diff(cells_with_ground))

        nz = nlayer
        special_codes = self.cfg_full["material"][
            'special_codes']  # get list of special material code groups, in this case for "ground"

        cells_with_ground = np.repeat(0, nz)  # for counting cells with ground

        for i in range(nz):
            cells_with_ground[i] = np.isin(self.material[i, :, :], special_codes["ground"]).sum()

        diffs = np.diff(cells_with_ground)
        gl_index = np.where(diffs == np.max(diffs))[0][
                       0] + 1  # find layer mit maximum increase in ground cells - we assume this is the ground layer

        dl_index = np.where(np.array(self.cfg.landscape) == "dummy_detector")[0]  # find detector layer
        if (len(dl_index) == 0):
            dl_index = max(gl_index - 1,
                           0)  # no detector layer found: set the detector layer just above the ground - not meaningful anyway for non-flat topography
            sl_index = min(nlayer - 1, 1)  # put the sourcelayer as second layer
        else:
            dl_index = dl_index[0]  # convert array to int and use the value
            sl_index = np.where(np.array(self.cfg.landscape) == "pad_detector")[0]  # find padding layer
            if (len(sl_index) == 0):
                sl_index = dl_index + 2  # put the sourcelayer below the dummy-detector layer and the padding layer
            else:
                sl_index = sl_index[0] + 1  # convert array to int and use the value

        self.cfg.source_layer = self.cfg.landscape[sl_index]
        self.cfg.detector_layer = self.cfg.landscape[dl_index]
        self.cfg.ground_layer = self.cfg.landscape[gl_index]

    def _load_composite_ncdf(self):
        """fill 3D material/density/porosity space with data loaded from NetCDF"""
        import netCDF4  # for reading netcdf
        from os.path import exists  # for checking file existence

        filename = getattr(self.cfg, "realization_ncdf_path", "")  # get path to netCDF to load

        if not exists(filename):
            self.log.log("SEVERE:\t'NetCDF {}' does not exist.".format(filename))

        self._set_domain_from_ncdf_file(filename=filename, use_nz=True)

        nc_handle = netCDF4.Dataset(filename)
        # vars2load = ["x", "y", "z", "zmin"]  # variables expected in the NetCDF-file
        # missing = np.setdiff1d(vars2load, list(nc_handle.variables.keys()))
        # if (len(missing) > 0):
        #     self.log.log("SEVERE:\t'Variables(s) missing in NetCDF-file: {}.".format(missing))
        #
        # x_ = nc_handle.variables['x'][:]  # x-coords of grid cells (currently not used)
        # nx = len(x_)
        # y_ = nc_handle.variables['y'][:]  # y-coords of grid cells (currently not used)
        # ny = len(y_)
        coords = {}  # for collecting x, y, and z dimension coords
        coords['x_'] = nc_handle.variables['x'][:]  # x-coords of grid cells
        nx = len(coords['x_'])
        coords['y_'] = nc_handle.variables['y'][:]  # y-coords of grid cells
        ny = len(coords['y_'])
        coords['z_'] = nc_handle.variables['z'][:]  # upper boundaries of layers
        nz = len(coords['z_'])

        do_flip = {}  # check which dimensions need to be flipped to match Yulia standards
        do_flip["x"] = coords['x_'][0] > coords['x_'][-1]
        do_flip["y"] = coords['y_'][0] < coords['y_'][-1]  # we want y-axis to point northwards
        do_flip["z"] = coords['z_'][0] < coords['z_'][-1]  # we want the higher layers first


        z_ = coords['z_']  # upper boundaries of layers

        zmin = nc_handle.variables['zmin'][:]  # read lower boundary of lowest layer

        for idim in do_flip.keys():
            if do_flip[idim]:
                coords[idim + "_"] = np.flip(coords[idim + "_"], axis=0)

        grids2load = relevant_grids
        grids_found = list(nc_handle.variables.keys())
        #legacy cleanup:
        if "dens" in grids_found: #if "dens" is found, don't look for "density"
            del(grids2load["density"])
        else:
            if "density" in grids_found: #if "density" is found, don't look for "dens"
                del(grids2load["dens"])


        missing = np.setdiff1d(list(grids2load.keys()), grids_found)
        if (len(missing) > 0):
            self.log.log("WARNING:\t'Grid(s) missing in NetCDF, using defaults: {}.".format(missing))

        # extract relevant matrix cubes; transpose, if in reverse order of dimensions
        required_dim_order = (
            'z', 'y', 'x')  # order of dimensions required in the following steps (changed later, though)

        for src, dest in grids2load.items():
            if (src in missing):
                continue
            # check for correct order of dimensions
            a = nc_handle.variables[src].dimensions #get order of dimensions in netcdf
            if (np.size(a) != np.size(required_dim_order)) or (any(np.sort(a) != np.sort(required_dim_order))):
                self.log.log(
                    "SEVERE: Invalid or missing dimensions. Expected: {} Found: {} .".format(required_dim_order, a))

            corres = [a.index(val) for i, val in
                      enumerate(required_dim_order)]  # find corresponding order of dimensions
            corres = np.array(corres)
            #for debug purposes: only load part of grid
            found_dims = nc_handle.variables[src].shape
            cropped_dims = np.array([maxsize, self.dbg_maxdim, self.dbg_maxdim])  # crop matrix, if selected
            #cropped_dims = np.array(('z', 'y', 'x'))  # crop matrix, if selected
            cropped_dims[corres] = cropped_dims.copy()   # re-arrange cropped extents to match order in ncdf
            cropped_dims = np.minimum(cropped_dims, found_dims) #reduce the found dimensions to the cropped ones

           #grid = nc_handle.variables[src][:]
            grid = nc_handle.variables[src][0:cropped_dims[0], 0:cropped_dims[1], 0:cropped_dims[2]]
            grid = np.transpose(grid, axes=corres) #re-arrange dimensions to match Yulia conventions

            for idim in do_flip.keys():
                if do_flip[idim]:
                    axis_i = np.where(np.array(required_dim_order) == idim)[0][0]
                    grid = np.flip(grid, axis=axis_i)  # flip matrix if required

            # check consistency of dimensions, if existing
            if hasattr(self, dest):
                if (getattr(self, dest).shape != grid.shape):
                    self.log.log(
                        "SEVERE: Array dimensions of '{}' ({}) do not match dims specified in netCDF ({}).".format(src,
                                                                                                                   getattr(
                                                                                                                       self,
                                                                                                                       dest).shape,
                                                                                                                   grid.shape))
            setattr(self, dest, grid) # put grid to appropriate place
        nc_handle.close()
        if not hasattr(self, "sm"): #if no soil moisture was specified...
            self.sm = self._URANOScode2soilmoisture(self.material) #...use material codes

        self.log.log("...NetCDF loaded.")

    def _adjust_layers(self):
        # re-create / correct the "layer" attributes according to the imported matrices

        upper_limits = self.domain.upper_limits
        zmin = self.domain.zmin

        # delete the conceptual layers (ie "compartments") and replace them by name and properties of resulting URANOS-layers according to data found in the NetCDF
        for layer_name in self.cfg.landscape:
            if (hasattr(self, layer_name)):
                delattr(self, layer_name)  # delete layers read from config - these are no longer valid

        # generate new synthetic names for layers
        new_layer_names = ["mixed_layer" + str(i + 1) for i in range(self.domain.nz)]

        # add a dummy detector layer, if requested
        h_dd = str(getattr(self.cfg, "thickness_dummy_detector_layer", ""))

        if (h_dd.isdigit()):
            h_dd = 0
        else:
            h_dd = float(h_dd)

        if (h_dd != 0):
            # enlarge domain
            h_pad = float(str(getattr(self.cfg,"pad_dummy_detector_layer", 0)))
            n_layers2add = 1 + (h_pad != 0)  # number of layers to add (2 or 1 (when padding lyer is zero))
            self.domain.nz = self.domain.nz + n_layers2add  # increase number of layers by padding and dummy layer
            upper_limits = self.domain.upper_limits
            upper_limits = np.concatenate((upper_limits[0] - [h_dd + h_pad, h_pad], upper_limits))
            self.domain.upper_limits = np.unique(upper_limits)
            # zmin = self.domain.zmin = zmin + h_dd #"+" because of inversed z-axis
            # generate new names for dummy layers
            new_layer_names = ["dummy_detector", "pad_detector"][0:n_layers2add] + new_layer_names
            # add padding layer
            if (h_pad != 0):  # add padding layer
                material_new = np.pad(self.material, ((n_layers2add, 0), (0, 0), (0, 0)), 'constant',
                                      constant_values=(self.material[0, 0, 0]))
                density_new = np.pad(self.density, ((n_layers2add, 0), (0, 0), (0, 0)), 'constant',
                                     constant_values=(self.density[0, 0, 0]))
                porosity_new = np.pad(self.porosity, ((n_layers2add, 0), (0, 0), (0, 0)), 'constant',
                                      constant_values=(self.porosity[0, 0, 0]))
                # set dummy detector layer
            material_new[0, :, :] = material_new[0, :, :]  # + 500 #251

            self.material = material_new  # "export" modified matrices
            self.density = density_new
            self.porosity = porosity_new

        self.cfg.landscape = new_layer_names
        self.cfg.landscape_add_layers = new_layer_names
        self._create_mixedlayer_objects(new_layer_names)  # create the actual layer objects

        layer_material_codes = self.voxel_code2layer_code(self.material[:, 0,
                                                          0])  # we just use the first value in the layer. Overriden by URANOS anyway when grids are provided.

        if getattr(self.cfg, "export_to_uranos", True) in [True, "png", "PNG"]:
            self.log.log("Exporting layers to URANOS ...")
        # set properties of newly generated layers
        for pos, layer_name in enumerate(new_layer_names):
            layer_new = getattr(self, layer_name)
            # layer_new =  copy.deepcopy(layer_tmpl) #create copy of template layer
            # layer_new.cfg.upper_limit = np.round(-z_[-(pos+1)], decimals=2)
            layer_new.cfg.upper_limit = np.round(upper_limits[pos], decimals=2)
            # layer_new.cfg.width = np.round(np.diff(a=z_, prepend= zmin)[-(pos+1)], decimals=2)
            layer_new.cfg.width = np.round(np.diff(a=upper_limits, append=zmin)[pos], decimals=2)
            layer_new.cfg.id = pos + 1  # numbering from the top, starting with 1
            # layer_new.cfg.material_code = 10 #just a dummy value using the code for "air". Overriden by URANOS anyway when grids are provided
            layer_new.cfg.material_code = layer_material_codes[
                pos]  # we just use the first value in the layer. Overriden by URANOS anyway when grids are provided.

            # export layer
            self.cfg.n_layers_above = pos
            # if pos>=0: #rr remove me, debug only
            #    print("remove me! debug setting!")
            if getattr(self.cfg, "export_to_uranos", True):
                self._export_layer(e_id=pos, i=1, layer_type="regular")
            if (layer_new.cfg.material_code == -1):
                layer_new.cfg.material_code = 11  # replace -1 ("no matching layer code, export distributed layer") with code for air (ignored by URANOS, but needed to prevent crashes)

    def voxel_code2layer_code(self, voxel_codes):
        """convert a URANOS voxel-code to a corresponding layer-code, if any"""
        special_codes = self.cfg_full["material"]['special_codes']

        layer_codes_table = self.cfg_full["material"]['codes_uniform']  # layer material codes

        vox_names = self.cfg_full["material"]['codes_distributed_exp'].name
        lay_names = {val["code"]: val["name"] for mat, val in layer_codes_table.items()}

        layer_codes = list()
        for voxel_code in voxel_codes:
            # print(voxel_code)
            voxel_code_reduced = voxel_code - 500 * (voxel_code > 500)  # workaround for additional detector layers
            if (not voxel_code_reduced in vox_names.keys()):
                layer_code = -1  # marker for "not found"
            else:
                layer_code = [key for key, val in lay_names.items() if val == vox_names[voxel_code_reduced]]
                if len(layer_code) == 0:  # no corresponding material name found?
                    layer_code = -1  # marker for "not found"
                    if (voxel_code_reduced) in special_codes[
                        "soil"]:  # use a generic "soil"-code,(e.g. for soils with variable soil moisture)
                        layer_code = list(lay_names.keys())[list(lay_names.values()).index("soil")]
                    if (voxel_code_reduced) in special_codes[
                        "air"]:  # use a generic "soil"-code,(e.g. for soils with variable soil moisture)
                        layer_code = list(lay_names.keys())[list(lay_names.values()).index("air")]
                else:  # match found
                    layer_code = layer_code[0]  # use first entry in case of multiple matches
            layer_code = layer_code + 500 * (voxel_code > 500)  # add 500 (flag for "detector layer", if selected)
            layer_codes.append(layer_code)  # append to return list

        # check
        # for voxel_code in voxel_codes:
        #     print(vox_names[voxel_code])

        # for layer_code in layer_codes:
        #     print(lay_names[layer_code])

        return (layer_codes)

    def layer_code2voxel_code(self, layer_codes):
        """convert a URANOS layer-code to a corresponding voxel-code, if any"""

        #        special_codes     = self.cfg_full["material"]['special_codes']
        layer_codes_table = self.cfg_full["material"]['codes_uniform']  # layer material codes

        vox_names = self.cfg_full["material"]['codes_distributed_exp'].name
        lay_names = {val["code"]: val["name"] for mat, val in layer_codes_table.items()}

        voxel_codes = list()
        for layer_code in layer_codes:
            layer_code_reduced = layer_code - 500 * (layer_code > 500)  # workaround for additional detector voxels
            if (not layer_code_reduced in lay_names.keys()):
                voxel_code = -1  # marker for "not found"
            else:
                voxel_code = [key for key, val in vox_names.items() if val == lay_names[layer_code_reduced]]
                if len(voxel_code) == 0:
                    voxel_code = -1  # marker for "not found"
                else:  # no match found
                    voxel_code = voxel_code[0]  # use first entry in case of multiple matches
            voxel_code = voxel_code + 500 * (layer_code > 500)  # add 500 (flag for "detector voxel", if selected)
            voxel_codes.append(voxel_code)  # append to return list

        return (voxel_codes)

    def _calculate_atm_density(self, elevs):
        """calculate and set atmospheric density for all air-voxels"""
        elevs = (elevs[1:] + elevs[:-1]) / 2  #compute vertical central value of all layers [m] (mean of upper and lower boundary of layer), invert URANOS convention

        p_z = self.met.calc_air_pressure(z=elevs) #atmospheric density for all elevations [mb]
        #self.log.log("...  Dieser Teil ist noch nicht fertig: {}".format("",""))
        #for i, e in enumerate(elevs):
        #    atm_density[i] = self.met.calc_atm_depth_from_elev(e) #compute air density from elevation [g/cm³]
        p0 =  getattr(self.cfg, "air_pressure", self.met.calc_air_pressure(z=0)) #get reference pressure or use z=0 [mb]

        special_codes = self.cfg_full["material"]['special_codes'] #retrieve material codes of 'air'
        rho_air = self.cfg_full["material"]['codes_distributed_exp'].loc[special_codes["air"][0]] ["density"]/ 1000  # g/cm³ ?

        atm_density = rho_air * p_z/p0  #rescale density profile to match rho_0 at ground level

        for i, elev in enumerate(elevs):
            cur_layer_density = self.density [:,:,i] #"pointer" to air voxels in current layer

            #set density of "air" voxels
            air_mask = np.in1d(self.material[:,:,i], special_codes['air']).reshape(self.material[:,:,i].shape) #find voxels consisting of air
            air_mask = np.logical_and(air_mask, self.density [:,:,i] <= 0) #only alter voxels that have not already set their density otherwise
            cur_layer_density[air_mask] = atm_density[i]  #set density of air voxels

            air_mask = self.porosity[:,:,i] != 0  # find voxels having air in pores
            air_mask = np.logical_and(air_mask, self.density [:,:,i] <= 0) #only alter voxels that have not already set their density otherwise
            cur_layer_density[air_mask] -= self.porosity[:,:,i][air_mask] * atm_density[i]  # increase density according to porosity and air density ("-=" becasue the respective values are still stored as negatives to distinguish "not to be changed" densities)

            #density_ratio = atm_density[i] / self.cfg.atm_depth #density correction factor for current elevation
            #todo: find voxel containing "with air" and correct density accordingly (using porosity, etc.). Probably not here yet, as the densities are still the "URANOS"-ones (excluding the air)

        return()




    def export_layer(self, layer, layer_type="regular"):
        """don't export layers during geometry generation"""
        pass

    def _export_layer(self, e_id, i, layer_type="regular"):
        """export data to URANOS matrices (and optionally as GeoTiffs)"""
        import os
        import numpy as np

        # get length of landscape above to calculate first ID of add_layers
        n = self.cfg.n_layers_above
        obsolete = {"material": False, "density": False,
                    "porosity": False}  # determine, if a layer can be omitted during export because it contains only homogenous material / default values

        layer_name = self.cfg.landscape[e_id]  # get layer name
        layer_obj = getattr(self, layer_name)  # get layer object

        if getattr(self.cfg, "skip_obsolete_exports", False):  # check if the export of this layer can be skipped
            obsolete["material"] = np.all(
                self.material[e_id, :, :] == self.material[e_id, 0, 0]) and (layer_obj.cfg.material_code != -1) and (self.material[e_id, 0, 0] != 251) #251: matrices with detector layer voxels have to be written anyway
            obsolete["density"] = np.all(self.density[e_id, :, :] == 100)
            obsolete["porosity"] = np.all(self.porosity[e_id, :, :] == 0)

        # if (layer_obj.cfg.material_code == -1):
        #    layer_obj.cfg.material_code = 11

        f_mat = os.path.join(self.cfg.scenario_dir,
                             "{}.dat".format(i + n))
        f_dens = os.path.join(self.cfg.scenario_dir,
                              "{}d.dat".format(i + n))
        f_poro = os.path.join(self.cfg.scenario_dir,
                              "{}p.dat".format(i + n))
        f_mat_png = os.path.join(self.cfg.scenario_dir,
                                 "{}.png".format(i + n))
        f_dens_png = os.path.join(self.cfg.scenario_dir,
                                  "{}d.png".format(i + n))
        f_poro_png = os.path.join(self.cfg.scenario_dir,
                                  "{}p.png".format(i + n))
        f_mat_tif = os.path.join(self.cfg.scenario_dir,
                                 "Material_{}.tif".format(i + n))
        f_dens_tif = os.path.join(self.cfg.scenario_dir,
                                  "Density_{}.tif".format(i + n))
        f_poro_tif = os.path.join(self.cfg.scenario_dir,
                                  "Porosity_{}.tif".format(i + n))
        f_mask = os.path.join(self.cfg.scenario_dir,
                              "Air_mask_detector_{}.tif".format(i + n))

        for f in [f_mat, f_dens, f_poro, f_mat_png, f_dens_png, f_poro_png, f_mat_tif, f_dens_tif, f_poro_tif, f_mask]:
            try:
                os.unlink(f)
            except:
                pass

        # export layer as ASCII
        if getattr(self.cfg, "export_to_uranos", True) == True:
            # create URANOS matrices
            if not obsolete["material"]:
                self.create_matrix_from_array(f_mat, self.material[e_id, :, :])
            if not obsolete["density"]:
                self.create_matrix_from_array(f_dens, self.density[e_id, :, :])
            if not obsolete["porosity"]:
                self.create_matrix_from_array(f_poro, self.porosity[e_id, :, :])
        # export layer as png
        if str(getattr(self.cfg, "export_to_uranos", "")).upper() == "PNG":
            # create URANOS matrices
            import matplotlib.image

            from PIL import Image
            # #in case 16-bit png is needed (currently not supported by URANOS
            # def save_16bit_png(filename, matrix):
            #     m2 = np.ushort(matrix)  # convert to 16 bit integer
            #     im = Image.fromarray(m2)
            #     im = im.convert('I;16')  # 16bit, greyscale
            #     im.save(filename)
            #     return()

            def save_8bit_png(filename, matrix): #8 bit, non-gray

                im = np.uint8(matrix)  # convert to 8 bit integer
#               m2 = np.byte(matrix)  # convert to 8 bit integer
 #              m2 = matrix  # convert to 8 bit integer

                im = Image.fromarray(im)
                #im = im.convert('I;16')  # 8bit, greyscale (strangely, 'I8' doesn't work). Doesn't seem to work, image is saved as all black

                im.save(filename)
                return ()

            if False:
                import numpy as np
                from PIL import Image
                matrix = np.ones((100,100))
                matrix[0:50,0:50] = 4
                m2 = np.uint8(matrix)  # convert to 8 bit integer
                im = Image.fromarray(m2)
                im.save("e:/temp/test.png") #A geht (256 cols)

    #            im = im.convert('I;16')  # geht nicht
    #            im.save("e:/temp/test.png") #geht

                im = Image.fromarray(matrix)
                im = im.convert('grey') #geht nicht
                im.save("e:/temp/test.png")


                im = im.convert('L')
                im.save("e:/temp/test.png") #B. geht

                im = Image.new('L', (100, 100))
                im.save("e:/temp/test.png")

            # #this gives colourful pictures, but not the required 8bit images:
            # if not obsolete["material"]: matplotlib.image.imsave(f_mat_png, self.material[e_id, :, :])
            # if not obsolete["density"]:  matplotlib.image.imsave(f_dens_png, self.density[e_id, :, :])
            # if not obsolete["porosity"]: matplotlib.image.imsave(f_poro_png, self.porosity[e_id, :, :])

            #8bit images:
            if not obsolete["material"]:
                save_8bit_png(f_mat_png,  self.material[e_id, :, :])
            if not obsolete["density"]:
                save_8bit_png(f_dens_png, self.density[e_id, :, :])
            if not obsolete["porosity"]:
                save_8bit_png(f_poro_png, self.porosity[e_id, :, :])

            # m2 = np.uint8(self.density[e_id, :, :])  # convert to 8 bit integer
            # save_8bit_png(f_dens_png, m2)
            # matplotlib.image.imsave(f_dens_png, m2)
            #
            #
            #np.max(self.porosity[e_id, :, :])
            #np.min(self.porosity[e_id, :, :])
            # type(self.porosity[e_id, 0, 0])
            #type(self.density[e_id, 0, 0])



        if getattr(self.cfg,"export_geotiffs", False):
            # export layers as GeoTiff

            if ~obsolete["material"]: self.domain.export_grid(f_mat_tif, self.material[e_id, :, :])
            if ~obsolete["density"]: self.domain.export_grid(f_dens_tif, self.density[e_id, :, :])
            if ~obsolete["porosity"]: self.domain.export_grid(f_poro_tif, self.porosity[e_id, :, :])

        if getattr(self.cfg,"export_detector_mask", True):
            if layer_type == "detector_layer":
                # create file name

                # get mask
                material = self.material[e_id, :, :]
                mask = np.zeros_like(material, dtype=np.int32)
                mask[material == 1] = 1
                # export air mask for detector layer
                self.domain.export_grid(f_mask, mask)

    def _add_layer(self, e_id, i, width, layer_type="regular"):
        """add additional layer"""
        # get length of landscape above to calculate first ID of add_layers
        n = self.cfg.n_layers_above

        # get layer name of ground layer (= last layer in list)
        ground_layer_name = self.cfg.landscape_add_layers[-1]
        # get ground layer
        ground = getattr(self, ground_layer_name)

        # layer_cfg
        sub_layer_cfg = ground.cfg.copy()
        # add information regarding sub layers to cfg
        sub_layer_cfg.add("width", width)
        sub_layer_cfg.add("id", i + n)

        # generate layer name from layer_id
        layer_name = "Subdivided_layer_{}".format(i + n)

        # add additional layer name to Landscape definition
        self.cfg.landscape = np.append(self.cfg.landscape, layer_name)
        # create instance of landscape element class
        layer_class = getattr(layer_types,
                              sub_layer_cfg.layer_type,
                              lambda x, y: self.log.log(
                                  "Error:\tUnknown Layer Type: '{}'.".format(
                                      sub_layer_cfg.layer_type)))
        setattr(self, layer_name, layer_class(sub_layer_cfg,
                                              self.cfg,
                                              self.cfg_full,
                                              self.log,
                                              self.domain))
        # export layer
        self._export_layer(e_id, i, layer_type)

        # log station layer ID
        if layer_type == "station_layer":
            self.log.log("... \tStation layer: {}".format(sub_layer_cfg.id))

        # add new detector layer name
        if layer_type == "detector_layer":
            self.cfg.add("detector_layer", layer_name)
            self.log.log("... \tAdding detector layer: {}".format(sub_layer_cfg.id))

        # add sublayer id count
        i += 1

        return i

    def _add_to_landscape(self):
        """add generated layers to landscape"""

        # sublayer id 
        i = 0

        # set number of first layer to add
        self.cfg.add("n_layers_above", 1)

        # iterate over elev_list
        for e_id, e in enumerate(self.elev_list):
            i = self._add_layer(e_id, i, self.cfg.discretisation)

    def generate_uranos_cfg_file(self):
        if getattr(self.cfg_full.operate, "generate", False):  # if generate==True
            super().generate_uranos_cfg_file()  # call the method from class "basic", otherwise don't do anything, as the config is already there
