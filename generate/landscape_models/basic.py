# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 11:28:48 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
from glob import glob
import os

# python package imports
import numpy as np

# yulia imports
from yulia.utils import Meteo
from yulia.utils import Utils
from yulia.generate import layer_types
from yulia.generate import Domain


class Basic(object):
    def __init__(self,
                 cfg_full,
                 log,
                 scenario_name,
                 initialise_domain=True,
                 domain_object=None):
        """
        Initialise class

        Args:
            cfg_full (object): Config object.
            log (object): Logfile object.
            scenario_name (str): Name of the scenario.
            initialise_domain (bool, optional): switch for domain ini. Defaults to True.
            domain_object (object, optional): Instance of existing domain class. Defaults to None.

        Returns:
            None.

        """
        # get cfg of landscape model
        self.cfg = getattr(cfg_full, scenario_name)

        # full cfg
        self.cfg_full = cfg_full

        # check if another scenario should be used as basis
        if "extend_scenario" in self.cfg:
            self._extend_existing_scenario()

        # add name of scenario to cfg
        self.cfg.add("name", scenario_name)

        # add epmpty layer list
        self.cfg.add("layer_list", list())
        # add functional layer list
        self.cfg.add("layer_func_list", ["source_layer",
                                         "detector_layer",
                                         "ground_layer"])
        # log object
        self.log = log
        # meteo utils class
        self.met = Meteo()
        # general utils class
        self.utils = Utils(self.log)
        # create folders
        self._create_folders()
        # create domain
        if initialise_domain and domain_object is None:
            self.domain = Domain(self.log, self.cfg, self.cfg_full)
            # export domain if dem is defined
            if hasattr(self.domain, "dem") and self.cfg_full.operate.get("export_dem", 
                                                                         True):
                self.domain.export_grid(os.path.join(self.cfg.scenario_dir,
                                                     "dem.tif"),
                                        self.domain.dem)
        elif domain_object is not None:
            self.domain = domain_object
        else:
            self.domain = None

    def _add_subdived_layers(self):
        """add additional subdivided layers to landscape"""
        pass

    def _create_folders(self):
        """create folders and filenames"""
        # scenario folder
        base_dir = self.cfg_full.operate.get("base_dir", "")
        if "/" in base_dir:
            base_dir_list = base_dir.split("/")
        elif os.sep in base_dir:
            base_dir_list = base_dir.split(os.sep)
        else:
            base_dir_list = [base_dir]

        sub_dir, sub_dir_list = self._define_scenario_subdir()
        sc_dir = os.path.join(base_dir, sub_dir)
        sc_dir = os.path.normpath(sc_dir)
        # create subfolders
        self.utils.generate_exportfolder(base_dir_list+sub_dir_list)
        # save scenario folder
        self.cfg.add("scenario_dir", sc_dir)

        # create filenames for config and geometry files
        self.cfg.add("path_config", os.path.join(self.cfg.scenario_dir,
                                                 "Uranos.cfg"))
        self.cfg.add("path_geometry", os.path.join(self.cfg.scenario_dir,
                                                   "UranosGeometryConfig.dat"))

    def _convert_uranos_results_to_geotiff(self):
        """Convert URANOS ascii matrix to geotiff"""
        # generate list of URANOS output files
        file_list = glob(os.path.join(self.cfg.scenario_dir, "d*.csv"))
        for f in file_list:
            # generate output fileaname
            f_out = f[:-4] + ".tif"
            # skip import/convertion for already existing output file
            if os.path.isfile(f_out):
                continue
            # import data
            data = self._import_uranos_ascii(f)
            # convert it to Geotiff
            self.domain.export_grid(f_out, data)

    def _define_scenario_subdir(self):
        """define subfolder for scenario"""
        if hasattr(self.cfg, "base_name"):
            base_name = self.cfg.base_name
        else:
            base_name = self.cfg.name

        # get subdir list:
        sub_dir_list = [base_name] + self.cfg.get("sub_dir_list", list())

        # generate subdirs
        sub_dir = ""
        for sd in sub_dir_list:
            sub_dir = os.path.join(sub_dir, sd)

        return sub_dir, sub_dir_list

    def _extend_existing_scenario(self):
        """Extend an existing scenario"""
        # get base scenario cfg
        base_cfg = getattr(self.cfg_full, self.cfg.extend_scenario).copy()

        # remove extend_scenario key to allow nested configurations
        self.cfg.remove("extend_scenario")

        # update keys and values from base scenario
        for i, key in enumerate(base_cfg):
            # skipp all keys that are already defined
            if key in self.cfg:
                continue
            # add new keys from base_cfg to cfg
            self.cfg.add(key, base_cfg[key])

        # check for nested configuration
        if "extend_scenario" in self.cfg:
            self._extend_existing_scenario()

    def _import_uranos_ascii(self, filename, sep="\t"):
        """import URANOS ascii matrix"""
        # open file in read mode
        with open(filename, 'r') as f:
            # read ascii matrix as list of lines
            lines = f.readlines()
            # check if domain is already defined
            if not hasattr(self, "domain"):
                # infer domain from file
                _l = len(lines)
                # set domain
                domain = np.ones((_l, _l))
            else:
                domain = self.domain.domain
            # create output array
            out = np.zeros_like(domain)
            # remap data to array
            for i, line in enumerate(lines):
                data = line.strip().split(sep)
                for j, d in enumerate(data):
                    out[-i, j] = d
            return out

    def _initialise_landscape(self):
        """Initialise all landscape elements"""

        # set atm_depth if air_pressure is defined
        if hasattr(self.cfg, "air_pressure"):
            atm_depth = self.met.calc_atm_depth(self.cfg.air_pressure)
            self.cfg.add("atm_depth", atm_depth)

        # get list of landscape elements and check if it's a list
        le_list = self.utils.check_if_list(self.cfg.landscape)
        self.cfg.add("landscape", np.array(le_list))

        # check soil moisture value against soil porosity
        sm = self.cfg.get("soil_moisture", 25)
        if sm > self.cfg.get("soil_porosity", 0.5)*100:
            sm = 100 - self.cfg.get("soil_porosity", 0.5)*100
        self.cfg.add("soil_moisture", sm)

        # create layer objects
        for layer_id, layer_name in enumerate(self.cfg.landscape):
            # first check if layer is defined
            self.check_layer_exists(layer_name)

            # get layer cfg
            if not hasattr(self.cfg_full.generate.layers, layer_name):
                e_text = "Severe:\t Layer '{}'not defined.".format(layer_name)
                self.log.log(e_text)
            layer_cfg = getattr(self.cfg_full.generate.layers, layer_name)

            # add layer id to cfg
            layer_cfg.add("id", layer_id+1)
            # add attributes to layer cfg
            attr_list = ["domain_size",
                         "resolution",
                         "soil_moisture",
                         "swe",
                         "biomass"]
            for attr in attr_list:
                if hasattr(self.cfg, attr):
                    layer_cfg.add(attr, getattr(self.cfg, attr))

            # create instance of landscape element class
            layer_class = getattr(layer_types,
                                  layer_cfg.layer_type,
                                  lambda: self.log.log(
                                      "Error:\tUnknown Layer Type: '{}'.".format(
                                          layer_cfg.layer_type)))
            setattr(self, layer_name, layer_class(layer_cfg,
                                                  self.cfg,
                                                  self.cfg_full,
                                                  self.log,
                                                  self.domain))

        # add subdived layers (if activated)
        self._add_subdived_layers()

        # check if source, detector and ground layers exist in landscape
        self.check_layer_func_list()

        # set variables for layer position and width (ground_layer = 0)
        pos = 0

        # iterate over layers to define width above and below ground
        for layer_id, layer_name in enumerate(self.cfg.landscape):
            layer_class = getattr(self, layer_name)
            if layer_name == self.cfg.ground_layer:
                break
            pos -= layer_class.cfg.width

        for layer_id, layer_name in enumerate(self.cfg.landscape):
            layer_class = getattr(self, layer_name)
            layer_class.cfg.add("upper_limit", pos)
            pos += layer_class.cfg.width

        # save that initialisation was completed
        self.cfg.add("landscape_initialised", True)

    def check_layer_exists(self, lname):
        """Check if layer definition exists"""
        err_text = "Layer {} not defined in generate_layers".format(lname)
        if not hasattr(self.cfg_full.generate.layers, lname):
            self.log.log(err_text)

    def check_layer_func_list(self):
        """Check if layer functions are defined and respective layer exist"""
        for lfunc in self.cfg.layer_func_list:
            if not hasattr(self.cfg, lfunc):
                err_text = "Layer function {} is not defined".format(lfunc)
            lname = getattr(self.cfg, lfunc)
            err_text = "Layer '{}' not defined in landscape, ".format(lname)
            err_text += "but used as: {}".format(lfunc)
            if lname not in self.cfg.landscape:
                self.log.log(err_text)

    def create_matrix_from_array(self, filename, array):
        """Export array to URANOS matrix"""
        with open(filename, 'w+') as f:
            out = ""
            for y in range(array.shape[0]):
                for x in range(array.shape[1]-1):
                    out += str(array[y, x]) + " "
                out += str(array[y][-1]) + "\n"
            f.write(out)

    def export_cfg(self):
        """export csv file with current configuration"""
        # generate file name
        f = os.path.join(self.cfg.scenario_dir, "python_cfg.csv")
        # export to csv file
        self.cfg.to_csv(f)

    def generate_uranos_cfg_file(self):
        """Generates URANOS configuration file"""
        # check initialisation
        if not self.cfg.get("landscape_initialised", False):
            # initalise all landscape elements
            self._initialise_landscape()

        # create configuration file
        filename = os.path.join(self.cfg_full.operate.config_dir,
                                self.cfg_full.operate.dummy_config)

        # read dummy config
        with open(filename, 'r') as dummy_cfg:
            cfg = dummy_cfg.readlines()

        # get directory of scenario
        scenario_dir = self.cfg.scenario_dir
        if not os.path.isabs(scenario_dir):
            scenario_dir = os.path.join(os.getcwd(), scenario_dir)
        scenario_dir = scenario_dir.replace("\\", "/")+"/"

        # manipulate dummy_cfg to new config
        cfg[0] = "{}\n".format(scenario_dir)
        cfg[1] = "{}\n".format(scenario_dir)
        cfg[2] = "{}\n".format(self.cfg_full.operate.incoming_dir)
        cfg[4] = "{}\n".format(self.cfg_full.operate.endf_dir)
        cfg[5] = "{}\t Number of Neutrons\n".format(
            self.cfg_full.operate.nr_neutrons)
        cfg[6] = "{}\t Dimension [mm]\n".format(self.cfg.domain_size*1000)
        cfg[7] = "{}\t Beam Radius [mm]\n".format(self.cfg.domain_size*1000)
        cfg[16] = "{}\t Soil Volumetric Water Fraction\n".format(
            self.cfg.soil_moisture/100)
        cfg[17] = "{}\t Air Humidity [g/m3]\n".format(self.cfg.air_humidity)
        cfg[18] = "{}\t Soil Porosity\n".format(self.cfg.get("soil_porosity",
                                                             0.500))
        air_pressure = self.met.calc_atm_depth(self.cfg.get("air_pressure",
                                                            1020))
        atm_depth = self.cfg.get("atm_depth",
                                 self.met.calc_atm_depth(air_pressure))
        cfg[19] = "{}\t Atmospheric Depth [g/cm2]\n".format(atm_depth)
        cfg[20] = "{}\t Cutoff Rigidity [GeV]\n".format(
            self.cfg.cutoff_rigidity)

        # write to new config file
        with open(self.cfg.path_config, 'w+') as out:
            outstring = ""
            for line in cfg:
                outstring += line
            out.write(outstring)

    def generate_uranos_geometry(self):
        """Generates URANOS geometry file"""
        # check initialisation
        if not self.cfg.get("landscape_initialised", False):
            # initalise all landscape elements
            self._initialise_landscape()

        # create geometry file
        with open(self.cfg.path_geometry, 'w+') as out:
            # write layer numbers for source, detector and ground
            out.write('{}\n'.format(int(self.get_layer_nr(
                self.cfg.source_layer))))
            out.write('{}\n'.format(int(self.get_layer_nr(
                self.cfg.detector_layer))))
            out.write('{}\n'.format(int(self.get_layer_nr(
                self.cfg.ground_layer))))

            for layer_name in self.cfg.landscape:
                layer = getattr(self, layer_name)
                layer_pos = layer.cfg.upper_limit
                layer_width = round(float(layer.cfg.width), 1)
                layer_mat = int(layer.cfg.material_code)

                # add layer to geometery definition
                outstring = "{}\t{}\t{}\n".format(layer_pos,
                                                  layer_width,
                                                  layer_mat)
                out.write(outstring)

                # export distributed layers
                if layer.cfg.distribution == "distributed":
                    if layer_name == self.cfg.detector_layer:
                        self.export_layer(layer, layer_type="detector_layer")
                    else:
                        self.export_layer(layer)

        # export python cfg as well
        if self.cfg.get("export_python_cfg", True):
            self.export_cfg()

    def get_layer_nr(self, layer_name):
        """Get layer nr/ID"""
        layer_class = getattr(self, layer_name)
        return layer_class.cfg.id

    def import_cfg(self):
        """import csv file with current configuration"""
        # generate file name
        f = os.path.join(self.cfg.scenario_dir, "python_cfg.csv")
        # import from csv file
        self.cfg.read_csv(f)

    def export_layer(self, layer, layer_type="regular"):
        """export data to URANOS matrices (and optionally as GeoTiffs)"""

        # export layer
        if self.cfg.get("export_to_uranos", True):
            # create URANOS matrices
            f_mat = os.path.join(self.cfg.scenario_dir,
                                 "{}.dat".format(layer.cfg.id))
            f_dens = os.path.join(self.cfg.scenario_dir,
                                  "{}d.dat".format(layer.cfg.id))
            self.create_matrix_from_array(f_mat, layer.material)
            self.create_matrix_from_array(f_dens, layer.density)

        if self.cfg.get("export_geotiffs", True):
            # export layers as GeoTiff
            f_mat_tif = os.path.join(self.cfg.scenario_dir,
                                     "Material_{}.tif".format(layer.cfg.id))
            f_dens_multi_tif = os.path.join(self.cfg.scenario_dir,
                                            "DensityMultiplicator_{}.tif".format(
                                                layer.cfg.id))
            self.domain.export_grid(f_mat_tif, layer.material)
            self.domain.export_grid(f_dens_multi_tif, layer.density)

        if self.cfg.get("export_detector_mask", True):
            if layer_type == "detector_layer":
                # create file name
                f_mask = os.path.join(self.cfg.scenario_dir,
                                      "Air_mask_detector_{}.tif".format(
                                          layer.cfg.id))
                # get mask
                material = layer.material
                mask = np.zeros_like(material, dtype=np.int32)
                mask[material == 1] = 1
                # export air mask for detector layer
                self.domain.export_grid(f_mask, mask)
