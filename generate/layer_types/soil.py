# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 15:10:08 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# yulia imports
from yulia.generate.layer_types import General


class Soil(General):
    def set_layer_material(self):
        """Set uniform material for geometry definition"""
        # use soil code as default
        self.cfg.add("default_material_uniform", 20)
        # set material code
        self.cfg.add("material_code", 
                     self.get_material_code(self.cfg.material, "uniform",
                     warn=False))
        
    def set_domain(self):
        """Set layer characteristics in domain"""
        
        # get main material code
        material_code = self.get_material_code(self.cfg.material,
                                               "distributed")
                
        # set material
        if "geodata" in self.cfg:
            geodata = self.domain.import_geodata(self.cfg.geodata).data
            air_mask = (geodata == 1)
            soil_mask = (geodata <= 100)
            self.material = geodata.copy()
            self.material[soil_mask] = material_code
            # set air if there was an air mask
            self.material[air_mask] = 1
        else:
            self.material = self.domain.domain.copy() * material_code
            
        # set default density
        self.density = self.domain.domain.copy() * 100