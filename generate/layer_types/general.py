# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 13:24:16 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import os

# python package imports
import numpy as np

# yulia imports
from yulia.utils import Gridfile
from yulia.utils import Meteo
from yulia.utils import Utils
from yulia.utils import Logfile

class General(object):
    def __init__(self, 
                 cfg, 
                 cfg_scenario,
                 cfg_full, 
                 log, 
                 domain):
        """
        Initialise class

        Args:
            cfg (object): Config object of layer.
            cfg_full (object): Full Config object.
            log (object): Logfile object.
            domain (object): Domain object.

        Returns:
            None.

        """
        self.cfg = cfg
        self.cfg_scenario = cfg_scenario
        self.cfg_full = cfg_full
        self.log = log
        self.domain = domain
        self.met = Meteo()
        self.utils = Utils()
        self.set_layer_material()
        
        if self.cfg.distribution == "distributed":
            self.set_domain()
        
    def import_geodata(self):
        """Import geodata"""
        # get geodata type
        data_type = getattr(self.cfg_full.generate.geodata, self.cfg.geodata).data_type
        # create method name according to variable
        _method_name = "_import_geodata_{}".format(data_type)
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
        # call specific function
        return getattr(self,
                       _method_name, 
                       lambda: self.log.log(_err_string))()

    def _import_geodata_grid(self):
        """Import grid geodata"""
        f = getattr(self.cfg_full.generate.geodata, self.cfg.geodata).path
        f = os.path.join(self.cfg_full.operate.data_dir, f)
        self.grid = Gridfile(self.log)
        self.grid.import_grid(f)
        return self.grid.data

    def _import_geodata_nest(self):
        """Import geodata stored in NEST"""
        self.log.log("NEST support not yet implemented.")

    def set_layer_material(self):
        """Set uniform material for geometry definition"""
        # use air code as default
        self.cfg.add("default_material", "air")
        # set material code
        self.cfg.add("material_code", 
                     self.get_material_code(self.cfg.material, "uniform", 
                                            warn=False))

    def set_domain(self):
        """Set layer characteristics in domain"""
        
        # set material
        if "geodata" in self.cfg:
            data = self.domain.import_geodata(self.cfg.geodata)
            if hasattr(data, "copy"):
                self.material = data.copy()
            else:
                self.material = data.data.copy()
        else:
            # get main material code
            material_code = self.get_material_code(self.cfg.material,
                                                   "distributed")
            self.material = self.domain.domain.copy() * material_code
        
        # set default density
        self.density = self.domain.domain.copy() * 100
            
    def set_layer_property(self, **args):
        """Set property to layer"""
        # get type of layer
        layer_type = args.get("layer_type", "uniform")
        # create method name according to variable
        _method_name = "_set_layer_property_{}".format(layer_type)
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
        # assign data_set to args
        getattr(self,
                _method_name, 
                lambda **x: self.log.log(_err_string))(**args)
        
    def _set_layer_property_uniform(self):
        pass
    
    def calc_density_fact(self, target_density):
        """Calc layer density in relation to URANOS base density"""
        # get standard density
        base_rho = self.get_material_density(self.cfg.material)
        # get factor
        density_fact = float(target_density)/base_rho
        
        return np.array(density_fact)
    
    def calc_density_multiplicator(self, target_density):
        """Calc URANOS density multiplicator"""
        density_fact = self.calc_density_fact(target_density)
        density_multiplicator = np.ones_like(density_fact)
        
        # 1...200 = Material with material code from GUI and density multiplicator of 0.01...2.00 (= value/100)
        density_multiplicator[density_fact <= 2] = density_fact[density_fact <= 2]*100
        # 201...250 = Material with material code from GUI and density multiplicator of 1...50 (= value - 200)
        density_multiplicator[density_fact > 2] = (200 + density_fact[density_fact > 2])
        
        # control upper limit of density_multiplicator (cannot exceed 250)
        if np.max(density_multiplicator) > 250:
            self.log.log("Error:\tdensity_multiplicator exceeded 250.")
        density_multiplicator[density_multiplicator >250] = 250
    
        # round to zero digits
        return np.round(density_multiplicator, 0)

    def calc_target_density_from_multiplicator(self, density_multiplicator):
        """Inverse multiplicator and calculate original target density"""
        # get standard density
        base_rho = self.get_material_density(self.cfg.material)
        
        if type(density_multiplicator) != np.ndarray:
            density_multiplicator = np.array(density_multiplicator)
        
        # create array for target density
        density = np.ones_like(density_multiplicator)
        
        # create masks for values below and above 200
        mask_below_200 = np.zeros_like(density_multiplicator, dtype=bool)
        mask_above_200 = np.zeros_like(density_multiplicator, dtype=bool)
        mask_below_200 = (density_multiplicator <= 200)
        mask_above_200 = (density_multiplicator > 200)
        
        # convert multiplicator
        density[mask_below_200] = density_multiplicator[mask_below_200]/100
        density[mask_above_200] = density_multiplicator[mask_above_200]-200
              
        return base_rho * density
    
    def get_material_code(self, material_name, layer_type, warn=True):
        """
        Get URANOS material code from material name

        Args:
            material_name (str): Name of material.
            layer_type (str): Type of layer.

        Returns:
            material_code (int): URANOS material code.
        """
        # create method name according to variable
        _method_name = "_get_material_code_{}".format(layer_type)

        # assign data_set to args
        return getattr(self,_method_name)(material_name, warn)
    
    def _get_material_code_uniform(self, material_name, warn=True):
        """Set non-matrix material code"""
        
        # change material name in case of stones
        if "stones" in material_name:
            material_name = "quartz"
        
        # change material name in case of snow
        if "snow" in material_name:
            material_name = "snow"
        
        # change material name in case of vegetation
        if "vegetation" in material_name:
            material_name = "plants"
            
        # change material name in case of wood
        if "wood" in material_name:
            material_name = "plant"
        
        # check if material exists
        if not hasattr(self.cfg_full.material.codes_uniform, material_name):
            # use default material and warn
            default_material = self.cfg.get("default_material", "air")
            if warn:
                err_text = "Warning:\t"
                err_text += "Layer {}: material not found. ".format(self.cfg.name)
                err_text += "Using default material {}.".format(default_material)
                self.log.log(err_text)
            material_name = default_material
        # get config
        _cfg = getattr(self.cfg_full.material.codes_uniform, material_name)
        # return naterial code
        return _cfg.code
    
    def _get_material_code_distributed(self, material_name, warn=True):
        """Set matrix material code"""
        if material_name == "soil":
            code = int(round(self.cfg.get("soil_moisture", 30)*2,0))
    
        elif material_name == "none" or material_name == "None":
            code = -1
    
        else:
            # check if material exists
            if not hasattr(self.cfg_full.material.codes_distributed, material_name):
                # use default material and warn
                default_material = self.cfg.get("default_material", "air")
                if warn:
                    err_text = "Warning:\t"
                    err_text += "Layer {}: material not found. ".format(self.cfg.name)
                    err_text += "Using default material {}.".format(default_material)
                    self.log.log(err_text)
                material_name = default_material
            # get config
            _cfg = getattr(self.cfg_full.material.codes_distributed, material_name)
            # return naterial code
            code = _cfg.code
        return code
    
    def get_material_density(self, material_name):
        """
        Get URANOS material code from material name

        Args:
            material_name (str): Name of material.

        Returns:
            material_density (int): basic density of material.
        """
        _cfg = getattr(self.cfg_full.material.codes_distributed,
                       material_name)
        return _cfg.density