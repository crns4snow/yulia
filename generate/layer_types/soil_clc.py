# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 19:27:03 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# python package imports
import numpy as np

# yulia imports
from yulia.generate.layer_types import Soil


class SoilCLC(Soil):
    def get_material_clc(self, clc_name):
        # get config
        _cfg = getattr(self.cfg_full.material.clc_codes, clc_name)
        # return naterial name
        return _cfg.URANOS_material
    
    def set_domain(self):
        """Set layer characteristics in domain"""
        
        # get main material code
        main_material_code = self.get_material_code(self.cfg.material,
                                                    "distributed")
                
        # set material
        if "geodata" in self.cfg:
            clc = self.domain.import_geodata(self.cfg.geodata).data
            clc_list = np.unique(clc)
            
            self.material = self.domain.domain.copy()
            
            # iterate over land cover classes and convert to material code
            for lc in clc_list:
                # convert to integer
                lc = int(lc)
                # get URANOS material name from land cover class
                material_name = self.get_material_clc(f"clc_{lc}")
                # get URNAOS material code from material name
                material_code = self.get_material_code(material_name,
                                                       "distributed")
                # set material
                self.material[clc == lc] = material_code
            
            
            self.material[self.material <= 100] = main_material_code
        else:
            self.material = self.domain.domain.copy() * main_material_code
            
        # set default density
        self.density = self.domain.domain.copy() * 100