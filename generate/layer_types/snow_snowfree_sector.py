# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 19:19:10 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import os

# yulia imports
from yulia.generate.layer_types import Snow


class SnowfreeSector(Snow):
    def set_snow_free_areas(self):
        """Set snowfree areas"""
        
        # check prerequisites 
        if not hasattr(self.domain, "dem_obj"):
            if not hasattr(self.domain.dem_obj, "sectors"):
                e = "Severe:\tNo sectors defined. Check if dem and shp are loaded."
                self.log.log(e)
        
        # get sector
        sector = self.cfg_scenario.get("sector", 1)
        
        # check if sector exists in distzones
        if sector not in self.domain.dem_obj.sector_list:
            e = f"Error:\tSector {sector} not found."
            self.log.log(e)
            
        # get scf
        scf = self.cfg_scenario.get("scf", 1)/100.
        
        # create mask
        mask = (self.domain.dem_obj.sectors == sector)
        
        # create random snowfree areas
        scf_effective = self.set_random_snowfree(scf, mask)
        
        # add effective scf to scenario cfg
        self.cfg_scenario.add("scf_effective", scf_effective)
        
        # export snow-covered area
        f_out = os.path.join(self.cfg_scenario.scenario_dir, 
                             "snow_cover_fraction.tif")
        self.domain.export_grid(f_out, self.scf_per_zone)