# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 15:07:59 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# python package imports
import numpy as np

# yulia imports
from yulia.generate.layer_types import General


class Snow(General):
    def analyse_snow_free_areas(self):
        """Analyse distribution of snow-free areas"""
        # get main material code
        material_code = self.get_material_code(self.cfg.material,
                                               "distributed")
        
        # create snow mask for entire scene
        mask_snow = np.zeros_like(self.material, dtype=bool)
        mask_snow[self.material == material_code] = True
        
        # calc average scf for entire scene
        n_total = self.material.shape[0] * self.material.shape[1]
        n_snow = np.sum(mask_snow)
        scf = n_snow/n_total
        
        # add to config
        self.cfg_scenario.add("scf_domain", scf)
        
        # iterate over distzones
        for distzone in self.domain.dem_obj.distzone_list:
            # create masks
            mask_distzone = (self.domain.dem_obj.distzones == distzone)
            mask_snow = np.zeros_like(mask_distzone, dtype=bool)
            mask_snow[(self.material == material_code) & mask_distzone] = True
            
            # calculate scf
            n_total = np.sum(mask_distzone)
            n_snow  = np.sum(mask_snow)
            scf = n_snow/n_total
            
            # add to config
            self.cfg_scenario.add("scf_distzone_{:.0f}".format(distzone), scf)
    
    def analyse_swe(self):
        """Analyse snow water equivalent"""
        
        # get main material code
        material_code = self.get_material_code(self.cfg.material,
                                               "distributed")
        
        # create snow mask for entire scene
        mask_snow = np.zeros_like(self.material, dtype=bool)
        mask_snow[self.material == material_code] = True
        
        # calculate swe from material code (complete domain)
        swe = self.calc_target_density_from_multiplicator(self.density)
        swe[~mask_snow] = 0
        swe = np.mean(swe)  
              
        # add to config
        self.cfg_scenario.add("swe_domain", swe)
        
        # calculate swe for entire domain
        if hasattr(self, "swe_grid"):
            # calc mean swe
            swe = np.nanmean(self.swe_grid)        
            
            # add to config
            self.cfg_scenario.add("swe_from_swegrid_domain", swe)

        # iterate over distzones
        for distzone in self.domain.dem_obj.distzone_list:
            
            # create masks
            mask_distzone = (self.domain.dem_obj.distzones == distzone)
            mask_snowfree = np.zeros_like(mask_distzone, dtype=bool)
            mask_snowfree[(self.material != material_code) & mask_distzone] = True         
        
            # calculate swe grid from material code
            swe_grid = self.calc_target_density_from_multiplicator(self.density)
            
            # set snow-free areas to zero
            swe_grid[mask_snowfree] = 0

            # calc mean swe for the current dist zone
            swe = np.mean(swe_grid[mask_distzone])
            
            # add to config
            self.cfg_scenario.add("swe_distzone_{:.0f}".format(distzone), swe)
    
    def calc_density_fact(self, target_swe):
        """Calc layer density in relation to URANOS base density"""
        # get snow density [kg/m3] (based on material density)
        rho = self.get_material_density(self.cfg.material)
        # get height of snow (hs) [m] from layer width and substeps
        hs = self.cfg.width * self.cfg.get("substeps", 1)
        # calc default layer SWE [mm] based on snow density and hs
        default_layer_swe = self.domain.domain.copy() * rho * hs
        # set SWE factor
        swe_factor = target_swe / default_layer_swe
        
        return np.array(swe_factor)
    
    def set_layer_material(self):
        """Set uniform material for geometry definition"""
        # use snow code as default
        self.cfg.add("default_material_uniform", 8)
        # set material code
        self.cfg.add("material_code", 
                     self.get_material_code(self.cfg.material, "uniform"))
        
    def set_domain(self):
        """Set layer characteristics in domain"""
        
        # get main material code
        material_code = self.get_material_code(self.cfg.material,
                                               "distributed")
                
        # set default density
        self.density = self.domain.domain.copy() * 100
        
        # set material
        self.material = self.domain.domain.copy() * material_code
        
        # check for external geodata
        if "geodata" in self.cfg:
            # get external SWE grid
            self.swe_grid = self.domain.import_geodata(self.cfg.geodata).data

            # set mask for snow-covered area
            mask = (self.swe_grid > 0)
            # set material = air for snow-free areas
            self.material[~mask] = self.get_material_code("air",
                                                          "distributed")
            
            # set SWE factor
            self.density[mask] = self.calc_density_multiplicator(
                self.swe_grid)[mask]
        else:            
            # set SWE factor
            if hasattr(self.cfg_scenario, "swe"):
                # set density of snow layer
                self.density = self.calc_density_multiplicator(
                    self.cfg_scenario.swe)
                # set mask for snow-free area
                mask = (self.density == 0)
                # replace snow layer by air where density is zero
                self.density[mask] = 100
                self.material[mask] = self.get_material_code(
                    "air", "distributed")
                
        # set snow-free areas if needed
        self.set_snow_free_areas()
        
        # analyse snow free areas
        if self.cfg.get("calc_scf_from_matrices", True):
            self.analyse_snow_free_areas()
        
        # analyse snow water equvalent
        if self.cfg.get("calc_swe_from_matrices", True):
            self.analyse_swe()
        
    def set_snow_free_areas(self):
        """Set snowfree areas. Default: skip"""
        pass
        
    def set_random_snowfree(self, scf, mask):
        """Set random areas to be snow free"""
        
        # check if a scf arrays exist, otherwise create new ones
        if not hasattr(self, "scf"):
            self.scf = self.domain.domain.copy()
        if not hasattr(self, "scf_per_zone"):
            self.scf_per_zone = self.domain.domain.copy()
        
        # convert scf in snowfree area
        sfa = 1 - scf
        
        # copy material and density for masked area and flatten to 1D
        new_density = self.density[mask].copy().flatten()
        new_material = self.material[mask].copy().flatten()
        
        # calculate nr of pixels to be changed
        len_mask = len(new_density)
        if scf > 0:
            n_change = int(sfa * len_mask)
        else:
            n_change = len_mask
        # don't allow n_change to be larger than len_mask
        if n_change > len_mask:
            n_change = len_mask
        # calculate effective scf (depending on scf and nr of cells)
        scf_of_zone = 1 - (n_change/len_mask)
        self.scf_per_zone[mask] = scf_of_zone
               
        # for fully snow free mask
        if n_change == len_mask:
            self.density[mask] = 100.0
            self.material[mask] = 1
        
        # for fully snow-covered mask
        elif n_change == 0:
            pass
        
        # for equal snow cell loading of partially snow free mask
        else:
            # helper array with mask length
            helper = np.arange(0,len_mask)    
            
            # number of cells to load with additional snow mass
            nr_cells2load = len_mask - n_change 
            
            # indices of cells to load
            cells2load = np.random.choice(
                helper, size=nr_cells2load, replace=False)
            
            # indices of cells to unload (= snowfree)     
            cells2unload = np.array(list(set(helper).difference((cells2load))))
            
            # get snow density of cells to be unloaded
            get_swe = self.calc_target_density_from_multiplicator
            moved_swe = get_swe(new_density[cells2unload])   
            
            # get load to be distributed to each snow cell
            add_load = sum(moved_swe) / nr_cells2load    
            
            # get snow density of cells to be loaded
            current_swe = get_swe(new_density[cells2load])   
            
            # calculate density after loading
            new_swe = np.mean(current_swe + add_load)
            
            # set new density of snow cells (= multiplication factor)
            new_density[cells2load] = self.calc_density_multiplicator(new_swe)[0,0]
        
            # set new material / density of snow free cells (= air)
            new_material[cells2unload] = self.get_material_code(
                "air", "distributed")
            new_density[cells2unload] = 100.0

            self.density[mask] = new_density
            self.material[mask] = new_material
        
        return scf_of_zone