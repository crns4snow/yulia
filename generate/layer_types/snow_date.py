# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 13:20:16 2022

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# yulia imports
from yulia.generate.layer_types import Snow


class SnowDate(Snow):
    def set_domain(self):
        """Set layer characteristics in domain"""
        
        # get main material code
        material_code = self.get_material_code(self.cfg.material,
                                               "distributed")
                
        # set default density
        self.density = self.domain.domain.copy() * 100
        
        # set material
        self.material = self.domain.domain.copy() * material_code
        
        # check for external geodata
        if "geodata" in self.cfg:
            # create args
            args = dict()
            args["geodata"] = self.cfg.geodata
            
            # create filename
            if "date" not in self.cfg_scenario:
                self.log.log("Warning:\tDate undefined.")
            else:
                date = self.cfg_scenario.date
                dateformat = self.cfg_scenario.get("dateformat", 
                                                   "SWE_gap-filled_%Y-%m-%d.tif")
                f = date.strftime(dateformat)
                # add date and filename to args
                args["it"] = date
                args["filename"] = f
            
            # get external SWE grid
            self.swe_grid = self.domain.import_geodata(**args).data

            # set mask for snow-covered area
            mask = (self.swe_grid > 0)
            # set material = air for snow-free areas
            self.material[~mask] = self.get_material_code("air",
                                                          "distributed")
            
            # set SWE factor
            self.density[mask] = self.calc_density_multiplicator(
                self.swe_grid)[mask]
        else:
            # set warning
            self.log.log("Warning:\tNo Geodata defined. Date functionality unused.")
            # set SWE factor
            if hasattr(self.cfg_scenario, "swe"):
                # set density
                self.density = self.calc_density_multiplicator(self.cfg_scenario.swe)
        
        # set snow-free areas if needed
        self.set_snow_free_areas()
        
        # analyse snow free areas
        self.analyse_snow_free_areas()
        
        # analyse snow water equvalent
        self.analyse_swe()
