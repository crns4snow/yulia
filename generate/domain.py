# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 13:05:13 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
import os

# python package imports
import numpy as np

# yulia imports
from yulia.utils import ConfigDict
from yulia.utils import Gridfile
from yulia.utils import PointShape
from yulia.utils import Geotools
from yulia.utils import Utils


class Domain(Geotools):
    """Class defining a URANOS domain"""
    def __init__(self,
                 log,
                 cfg_scenario,
                 cfg_full):
        """
        Initialise domain

        Args:
            log (object): Log object.
            cfg_scenario (object): Config object for specific scenario.
            cfg_full (object): Full config object.

        Returns:
            None.

        """
        
        self.log = log
        self.cfg = cfg_scenario
        self.cfg_full = cfg_full
        
        # general utils class
        self.utils = Utils(self.log)
        
        # import detector shapefile if defined
        if hasattr(self.cfg, "location_shapefile"):
            # import shapefile
            self.shp = self.import_geodata(self.cfg.location_shapefile)
            # set new detector location default
            self.cfg.add("location_definition",
                         self.cfg.get("location_definition", "shp"))
            
        # set domain
        self.set_domain()
        
        # set detector location
        self.set_detector_location()
        
        # get dem
        self.get_dem()
        
        # update elevation if dem and station exist
        if self.cfg.get("has_station", False) and self.cfg.get("has_dem", False):
            # update station elevation
            self._update_station_elev()
                
            # calculate distance zones
            numsectors = self.cfg.get("num_sectors", 16)
            distzonewidth = self.cfg.get("width_distance_zones", 10)
            self.dem_obj.calc_distance_zones(self.station, 
                                             numsectors, 
                                             distzonewidth)
                
    def get_dem(self):
        """Get elevation data for domain"""
        
        # import DEM grid if defined
        if hasattr(self.cfg, "dem_file"):
            self.dem_obj = self.import_geodata(self.cfg.dem_file)
            self.dem = self.dem_obj.data
            self.cfg.add("has_dem", True)
      
    def import_geodata(self, geodata, **args):
        """Import geodata"""
        # get geodata type
        data_type = getattr(self.cfg_full.generate.geodata, geodata).data_type
        # create method name according to variable
        _method_name = "_import_geodata_{}".format(data_type)
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
                
        # create filename
        geo = getattr(self.cfg_full.generate.geodata, geodata)
        f = os.path.join(self.cfg_full.operate.data_dir, geo.path) 
        if "filename" in args:
            f = os.path.join(f, args["filename"])
        
        # add geodata to args
        args["path"] = f
        args["epsg"] = geo.epsg
        
        # call specific function
        return getattr(self,
                        _method_name, 
                        lambda x: self.log.log(_err_string))(**args)

    def _import_geodata_grid(self, path, **args):
        """Import grid geodata"""
        # create grid file object
        grid = Gridfile(self.log)
        # check if domain is defined with min and max
        if self.cfg.get("has_geotrans", False):
            # import dem and reduce it to area of interest
            grid.import_grid(filename=path, 
                             domain=self, 
                             rasterband=args.get("rasterband", 1))
        else:
            # import dem without domain information
            grid.import_grid(filename=path, 
                             rasterband=args.get("rasterband", 1))
        
        return grid

    def _import_geodata_nest(self, path, **args):
        """Import geodata stored in NEST"""
        self.log.log("NEST support not yet implemented.")
       
    def _import_geodata_shp(self, path, epsg, **args):
        """Import point shapefile"""
        # create shapefile object
        shp = PointShape(self.log, self.cfg, epsg)
        shp.import_data(path)
        return shp
       
    def set_detector_location(self):
        """Set location of detector"""
        # skip if detector already defined
        if not self.cfg.get("has_station", False):
            # get type of detector location
            loc_type = self.cfg.get("location_definition", "from_domain")
            
            # create method name according to location type
            _method_name = "_set_detector_location_{}".format(loc_type)
            _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
                _method_name)
            
            # call specific function
            return getattr(self,
                            _method_name, 
                            lambda: self.log.log(_err_string))()
        
    def _set_detector_location_from_coords(self):
        """Set detector location based on coordinates"""
        # get coordinates
        coords = getattr(self.cfg_full.generate.geodata, self.cfg.station).path
        
        # check if coordinates are given directly
        if type(coords) == list and len(coords) == 2:
            # assume that list contains coordinates
            self.station = ConfigDict()
            self.station.add("x", coords[0])
            self.station.add("y", coords[1])
            # add info that station is defined
            self.cfg.add("has_station", True)
        
    def _set_detector_location_from_shp(self):
        """Set detector location based on shapefile"""
        # get detector location
        detector = self.shp.get_point(self.cfg.location)
        if detector is not None:
            self.station = detector
            # add info that station is defined
            self.cfg.add("has_station", True)
            
    def _set_detector_location_from_domain(self):
        """Set detector location based on center of domain"""
        # calculate x and y using bounds of domain
        x = 0.5 * (self.x_min + self.x_max)
        y = 0.5 * (self.y_min + self.y_max)
        
        # get station elevation from config or assume sea-level elevation
        z = self.cfg.get("station_elevation", 0.0)
        
        # create station
        self.station = ConfigDict()
        self.station.add("x", x)
        self.station.add("y", y)
        self.station.add("z", z)
        # add info that station is defined
        self.cfg.add("has_station", True)
    
    def set_domain(self):
        """Set domain according to settings"""
        # get type of detector location
        domain_type = self.cfg.get("domain_definition", "from_domain_size")
        
        # create method name according to domain type
        _method_name = "_set_domain_{}".format(domain_type)
        _err_string = 'ERROR:\tMethod "{}" is not implemented.'.format(
            _method_name)
        
        # call specific function
        return getattr(self,
                        _method_name, 
                        lambda: self.log.log(_err_string))()
    
    def _set_domain_from_dem(self):
        """Set domain from DEM grid"""
        # get dem
        self.get_dem()
        
        if hasattr(self, "dem_obj"):
            # set domain
            self.domain = np.ones_like(self.dem)
            # geotrans
            self.geotrans = self.dem_obj.geotrans
            # grid resolution
            self.res = self.dem_obj.res
            # width and height of the grid
            self.width = self.dem_obj.width
            self.height = self.dem_obj.height
            # coordinates of grid corners
            self.x_min = self.dem_obj.x_min
            self.x_max = self.dem_obj.x_max
            self.y_max = self.dem_obj.y_max
            self.y_min = self.dem_obj.y_min
            # set that min max is defined
            self.cfg.add("has_geotrans", True)
        else:
            # print warning
            self.log.log("Warning:\tNo DEM file defined. Setting domain from size.")
            # set domain from size and resolution parameters
            self._set_domain_from_domain_size()
            
    def _set_domain_from_ncdf(self, domain_props= None):
        """Set domain for generation from ncdf - first called with dummy values, real values are filled later on generation/loading of composite"""
       
        if (domain_props==None): #dummy values
            domain_props={"domain_size":999,
                    # grid resolution
                   "res":999,
                   # width and height of the grid,
                   "width":999,
                   "height":999,
                   # coordinates of grid corners,
                   "x_min":999,
                   "x_max":999,
                   "y_max":999,
                   "y_min":999,
                   "nz":1}
            
        for key,val in domain_props.items():
            if key=="domain_size":
                setattr(self.cfg, key, val)
            else:
                setattr(self, key, val)
            
        self.cfg.add("has_dem", False)
        
        # set domain
        self.domain = np.ones(shape=(self.cfg.domain_size, self.cfg.domain_size))
        # geotrans
        #self.geotrans = self.dem_obj.geotrans
        
        # set that min max is defined
        self.cfg.add("has_geotrans", False)
    def _set_domain_from_domain_size(self):
        """Set domain from domain size and resolution"""
        # get length from domain_size and resolution
        _d = self.cfg.get("domain_size", 1000)
        self.res = self.cfg.get("resolution",2)
        _l = int(round(_d/self.res,0))
        
        # set domain
        self.domain = np.ones((_l, _l))
        
        # set default min/max values
        self.x_min = -0.5*(_d)
        self.x_max = 0.5*(_d)
        self.y_min = -0.5*(_d)
        self.y_max = 0.5*(_d)
        
        # set information
        self.width = _l
        self.height = _l
    
    def _set_domain_from_detector_location(self):
        """Set domain from detector location and radius"""
        # try to set station
        if not self.cfg.get("has_station", False):
            self.set_detector_location()
            
        if self.cfg.get("has_station", False):
            x = self.station.x
            y = self.station.y
            radius = self.cfg.get("radius", 600)
            self.cfg.add("domain_size", radius*2)
            self.x_min = x - radius
            self.x_max = x + radius
            self.y_min = y - radius
            self.y_max = y + radius
            self.res = self.cfg.get("resolution",2)
            # set geotrans
            self.geotrans = (self.x_min,
                             self.res, 
                             0, 
                             self.y_max,
                             0, 
                             -self.res)
            # get length from domain_size and resolution
            _d = self.cfg.get("domain_size", 1000)
            self.res = self.cfg.get("resolution",2)
            _l = int(round(_d/self.res,0))
            # set domain
            self.domain = np.ones((_l, _l))
            # add information that geotrans is defined
            self.cfg.add("has_geotrans", True)
            
        else:
            # set domain according to domain size
            self._set_domain_from_domain_size()
        
    def _update_station_elev(self):
        """Update elevation of station"""
        loc = self.get_row_col_point(self.station.x, self.station.y)
        self.station.add("z", self.dem[loc])