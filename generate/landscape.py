# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 17:13:41 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# python package imports
import numpy as np

# yulia imports
from yulia.utils import Meteo
from yulia.generate import LandscapeElement


class Landscape(object):
    def __init__(self, cfg, log):
        self.cfg = cfg
        self.log = log
        self.met = Meteo()
        self._initialise_landscape_elements()
        
    def _initialise_landscape_elements(self):
        """Initialise all landscape elements"""
        # set prev_le_id to zero
        self.len_layers = 0
        
        # list of landscape elements
        for le_name in self.cfg.generate.landscape:
            # set landscape element
            setattr(self, le_name, LandscapeElement(self.cfg, 
                                                    self.log, 
                                                    le_name,
                                                    self.len_layers))
            self.len_layers = getattr(self, le_name).get_highest_layer_id()
        