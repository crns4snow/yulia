# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 16:02:47 2021

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# python package imports
import numpy as np

# yulia imports
from yulia.generate import layer_types
from yulia.utils import Utils
from yulia.utils import Geotools


class LandscapeElement(object):
    def __init__(self, 
                 cfg_full, 
                 log, 
                 le_name, 
                 le_id):
        # get cfg of landscape element
        self.cfg = getattr(cfg_full.generate.landscape_elements, le_name)
        # full cfg
        self.cfg_full = cfg_full
        # log
        self.log = log
        # general utils class
        self.utils = Utils(self.log)
        # geotools
        self.geo = Geotools(self.log)
        # initialise sublayers
        self._initialise_sublayers(le_id)
        
    def _get_depth_from_grid(self, gridname):
        """get layer depth from grid file"""
        # open grid file
        self.geo.import_grid(gridname)
        # get range of values
        return np.nanmax(self.geo.data) - np.nanmin(self.geo.data)
        
    def _initialise_sublayers(self, le_id):
        """Initialise (sub-)layers of landscape element"""
        # check if sublayers is list
        subl = self.utils.check_if_list(self.cfg.sublayers)
        self.cfg.add("sublayers", subl)
        
        # get thickness of landscape element
        if "landscape_element_thickness" in self.cfg:
            thickness = self.cfg.landscape_element_thickness
        else:
            thickness = "none"
        
        # get lsit of discretisation steps and layer depth
        discretisation_list = list()
        for sub_name in self.cfg.sublayers:
            # get layer cfg
            layer_cfg = getattr(self.cfg_full.generate.layers, sub_name)
            # get discretisation and add to list if defined
            if 'discretisation' in layer_cfg:
                d = layer_cfg.discretisation
                if d != "none" and d != "None" and d != "NONE":
                    discretisation_list.append(float(d))
            # get depth
            if 'layer_thickness' in layer_cfg:
                d = layer_cfg.layer_thickness
                if "ext" in str(d):
                    #TODO: connect to correct grid name
                    f = "TODO"
                    d = self._get_depth_from_grid(f)
                discretisation_list.append(float(d))
                    
        # return lowest discretisation step or "none"
        if len(discretisation_list) > 0:
            discretisation = np.min(discretisation_list)
        else:
            discretisation = "none"
        
        print(self.cfg.name, discretisation)
        
        # create sublayer objects
        for sub_name in self.cfg.sublayers:
            # get layer cfg
            layer_cfg = getattr(self.cfg_full.generate.layers, sub_name)
            
            # set minimal discretisation
            layer_cfg.add("discretisation", discretisation)
            
            # create instance of landscape element class
            layer_class = getattr(layer_types, 
                                  layer_cfg.layer_type, 
                                  lambda x,y: self.log.log(
                                      "Error:\tUnknown Layer Type: '{}'.".format(
                                          layer_cfg.layer_type)))
            setattr(self, sub_name, layer_class(layer_cfg, self.log))
            
    def get_highest_layer_id(self):
        """
        Get highest layer ID.

        Returns:
            max_id (int): Highest layer ID

        """
        # interate over sublayers
        max_id = 1
        return max_id