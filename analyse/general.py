# -*- coding: utf-8 -*-
"""
Created on Mon Jan  3 16:01:09 2022

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python import
from glob import glob
import os
import datetime

# python package imports
import numpy as np
import pandas as pd

# yulia imports
from yulia.generate.landscape_models import Basic
from yulia.utils import Gridfile
from yulia.utils import Utils


class General(Basic):
    """General class for analysing URANOS results"""
    def __init__(self,
                 log,
                 cfg_full,
                 cfg,
                 scenario_list):
        """
        Initialise class

        Args:
            log (object): log object.
            cfg_full (object): Full cfg object.
            cfg (object): analysis cfg object.
            scenario_list (list): list of scenario names.

        Returns:
            None.

        """
        # get full cfg
        self.cfg_full = cfg_full
        
        # get cfg for analysis
        self.cfg = cfg
        
        # log object
        self.log = log
        
        # utils
        self.utils = Utils(self.log)
        
        # check if analyse vars is list
        self.cfg.add("analyse_vars", 
                     self.utils.check_if_list(self.cfg.analyse_vars))
        
        # add empty list of energy levels to cfg
        self.cfg.add("list_energy_levels", list())
        
        # create results dict
        self.density_data = pd.DataFrame(index=scenario_list)
        self.density_std = pd.DataFrame(index=scenario_list)
        
        # add new column for number of neutrons
        self.density_data["n_neutrons"] = 0
        self.density_std["n_neutrons"] = 0
        
        # add columns for requested variables
        for var in self.cfg.analyse_vars:
            self.density_data[var] = np.nan
            self.density_std[var] = np.nan

    def _get_detector_counts(self, 
                             nd_grid, 
                             n_neutrons):
        """
        Get detector neutron counts

        Args:
            nd_grid (obj): neutron density grid object.
            n_neutrons (float): number of neutrons in simulation.

        Returns:
            None.

        """
        # get distance
        _search_dist = self.cfg.get("ncts_search_distance", 50)
        # create distance based mask
        _mask = (self.domain.dem_obj.dist <= _search_dist)
        # calculate sum of neutron counts
        ncts = np.nansum(nd_grid.data[_mask])
        # calculate standard deviation
        std = ncts**(0.5)
        # normalise to neutron density per million simulated neutrons
        _nfactor = n_neutrons / 1000000.
        ncts /= _nfactor
        std /= _nfactor
        
        return ncts, std

    def _import_density_grid(self, p):
        """import density grid"""
        _grid_obj = Gridfile(self.log)
        _grid_obj.import_grid(p)
        return _grid_obj

    def export_results(self):
        """Export results to file"""
        
        # get base name of analysis
        base_name = self.cfg.get("analysis_name", self.cfg.base_name)
                
        # get basedir
        base_dir = self.cfg_full.operate.get("base_dir", "")
        if "/" in base_dir:
            base_dir_list = base_dir.split("/")
        elif os.sep in base_dir:
            base_dir_list = base_dir.split(os.sep)
        else:
            base_dir_list = [base_dir]
        
        # create subfolders
        self.utils.generate_exportfolder(base_dir_list+[base_name]+["analysis"])
        
        # create path
        out_path = os.path.join(base_dir, base_name)
        out_path = os.path.join(out_path, "analysis")
        self.cfg.add("out_path", out_path)
        
        # get current time-step
        now = datetime.datetime.now()
        
        # create export filenames
        f1 = os.path.join(out_path, now.strftime("%Y-%m-%d_ncts.csv"))
        f2 = os.path.join(out_path, now.strftime("%Y-%m-%d_std.csv"))
        f3 = os.path.join(out_path, now.strftime("%Y-%m-%d_results.csv"))
        
        # export dataframes to csv
        self.density_data.to_csv(f1)
        self.density_std.to_csv(f2)
        if hasattr(self, "results_ncts"):
            self.results_ncts.to_csv(f3)

    def import_density_maps(self, sc_obj):
        """
        Import density maps for given scenario

        Args:
            sc_obj (object): Scenario object.

        Returns:
            None.

        """
        # save base name or name of scenario to cfg
        if hasattr(sc_obj.cfg, "base_name"):
            base_name = sc_obj.cfg.base_name
        else:
            base_name = sc_obj.cfg.name
        # save to current config
        self.cfg.add("base_name", base_name)
        
        # save sub_dir_list
        self.cfg.add("sub_dir_list", sc_obj.cfg.get("sub_dir_list", list()))
        
        # get information from sc_obj
        self.domain = sc_obj.domain
        self.cfg.add("scenario_dir", sc_obj.cfg.scenario_dir)
        
        # convert csv to tif if neccessary
        self._convert_uranos_results_to_geotiff()
        
        # get list of density maps
        _path_list_tif = glob(os.path.join(self.cfg.scenario_dir, "densityMap*.tif"))
        
        # generate file name of existing cfg
        f = os.path.join(sc_obj.cfg.scenario_dir, "python_cfg.csv")
        # copy cfg
        cfg = sc_obj.cfg.copy()
        # import from existing cfg csv file
        cfg.read_csv(f)
        
        # add values of requested variables to dataframe
        for var in self.cfg.analyse_vars:
            # save to dataframe
            self.density_data.loc[(sc_obj.cfg.name, var)] = cfg.get(var,
                                                                    np.nan)
            self.density_std.loc[(sc_obj.cfg.name, var)] = cfg.get(var,
                                                                   np.nan)
        
        # iterate over files and extract information
        count = 0
        for p in _path_list_tif:
            count += 1
            # get filename from path
            f = p.split(os.sep)[-1]
            # extract energy window information
            en = f.split("_")[0][10:]
            # extract number of neutrons
            n_neutrons = float(f.split('_N')[-1].split('.')[-2])
            # import data
            data_obj = self._import_density_grid(p)
            # get neutron counts in certain radius around the detector
            ncts, std = self._get_detector_counts(data_obj, 
                                                  n_neutrons)
            # check if data for energy level exists
            if en not in self.density_data.columns:
                # add to energy level list
                self.cfg.list_energy_levels.append(en)                
                # add new column for this energy level
                self.density_data[en] = 0
                self.density_std[en] = 0
                
            if en == self.cfg.list_energy_levels[0]:
                # add number of neutrons
                self.density_data.loc[(sc_obj.cfg.name, 
                                       "n_neutrons")] += n_neutrons
                self.density_std.loc[(sc_obj.cfg.name,
                                      "n_neutrons")] += n_neutrons        
           
            # sum up ncts [per million]
            self.density_data.loc[(sc_obj.cfg.name, en)] += ncts
        
        ## divide ncts / std  by iterations
        # number of energy levels
        en_list = self.cfg.list_energy_levels
        # number of iterations
        if len(en_list) > 0:
            it = count / len(en_list)

            # calculate std and divide by iteration
            self.density_std.loc[(sc_obj.cfg.name, en_list)] = ((self.density_data.loc[(sc_obj.cfg.name, en_list)])**0.5) / it
            # divide ncts by iteration
            self.density_data.loc[(sc_obj.cfg.name, en_list)] /= it
       
    def __call__(self):
        """main call of the class"""
        pass
    
    
    
    