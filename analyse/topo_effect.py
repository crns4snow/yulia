# -*- coding: utf-8 -*-
"""
Created on Tue Jan 11 14:00:13 2022

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Paul Schattan.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python imports
from glob import glob
import os

# python package imports
import numpy as np
import pandas as pd

# yulia imports
from yulia.analyse import General
from yulia.utils import Gridfile


class TopoEffect(General):
    """Class for analysing topographic effects"""
    
    def _import_density_grid(self, p_data):
        """import density grid"""
        # get name of 3D scenario
        _n3d = self.cfg.name_3D_scenario
        # get basedir
        _base_dir = self.cfg_full.operate.get("base_dir", "")
        # get subdir list:
        _sub_dir_list = [_base_dir, _n3d] + self.cfg.get("sub_dir_list", list())
        # generate subdirs
        _sub_dir = ""
        for sd in _sub_dir_list:
            _sub_dir = os.path.join(_sub_dir, sd)
        # # get file name for 3D air mask
        # p_mask = glob(os.path.join(_sub_dir, "Air_mask_detector*.tif"))[0]
        # # import air mask for 3D scenario
        # _mask_obj = Gridfile(self.log)
        # _mask_obj.import_grid(p_mask)
        # mask = _mask_obj.data
        # import grid
        _grid_obj = Gridfile(self.log)
        _grid_obj.import_grid(p_data)
        data = _grid_obj
        # data[mask <= 0] = np.nan
        return data
    
    def __call__(self):
        """main call of the class"""
        # generate list of locations
        location_list = np.unique(self.density_data.dropna().location)
        
        # create empty result dataframes
        self.results_ncts = pd.DataFrame(index=location_list)
        self.results_std = pd.DataFrame(index=location_list)
        
        # create columns for energy levels in result dataframes
        for en in self.cfg.list_energy_levels:
            self.results_ncts[en] = np.nan
            self.results_std[en] = np.nan
        
        # iterate over locations
        for location in location_list:
            # get selection for current location
            df = self.density_data[self.density_data.location == location]
            # iterate over energy levels
            for en in self.cfg.list_energy_levels:
                # calculate 3D/2D ratio for current location and energy level
                if "Topo3Dfulldomain" in df.landscape_model.to_list() and "Topo2Dfulldomain" in df.landscape_model.to_list():
                    ratio = df[df.landscape_model == "Topo3Dfulldomain"][en].values[0] / df[df.landscape_model == "Topo2Dfulldomain"][en].values[0]
                elif "Topo3D" in df.landscape_model.to_list() and "Topo2D" in df.landscape_model.to_list():
                    ratio = df[df.landscape_model == "Topo3D"][en].values[0] / df[df.landscape_model == "Topo2D"][en].values[0]
                else:
                    ratio = np.nan
                # save to results
                self.results_ncts.loc[(location, en)] = ratio