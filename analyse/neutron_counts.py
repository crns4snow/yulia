# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 12:35:56 2022

This code is part of YULIA, Your Uranos Layer Integration Assistant,
issued under the MIT License.

Copyright: Jan Schmieder.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# native python import
from glob import glob
import os

# yulia imports
from yulia.analyse import General

class NeutronCounts(General):
    '''Class to calculate the sum of simulated neutrons'''
    
    def get_sim_nt(self,sc_obj):
        '''
        Get the number of simulated neutrons per (sub-)scenario
        '''
    
        # get information from sc_obj
        self.cfg.add("scenario_dir", sc_obj.cfg.scenario_dir)
  
        # get path list of density maps for 'Selected' energy level
        path_list_map = glob(os.path.join(self.cfg.scenario_dir,
                                          "densityMapSelected*.tif"))
        
        # get simulated neutron number per list item
        n_neutrons_list = []
        for p in path_list_map:
            # get filename from path
            f = p.split(os.sep)[-1]
            # get number of neutron
            n_neutrons = float(f.split('_N')[-1].split('.')[-2])
            n_neutrons_list.append(n_neutrons)
    
        # calculate sum of simulated neutrons
        n_neutrons_total = sum(n_neutrons_list)
    
        # scenario string
        sc = self.cfg.scenario_dir
        
        # console output
        print(f'{sc}\n'
              f'Number of simulated neutrons:\t{n_neutrons_total}\n')


    def __call__(self):
        pass